package com.baraciptalaksana.icckedirikota.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.BuildConfig;
import com.baraciptalaksana.icckedirikota.activity.ActivityChangePassword;
import com.baraciptalaksana.icckedirikota.activity.ActivityEditProfile;
import com.baraciptalaksana.icckedirikota.activity.ActivityForgotPassword;
import com.baraciptalaksana.icckedirikota.activity.ActivityLogin;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    TextView profil_name, tv_nrp, tv_telp, tv_fungsi, tv_jabatan, tv_kode_satwil, tv_satwil, reset_password, tv_versi;
    Button profil_logout;

    CircularImageView profil_photo;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        profil_name = (TextView)getView().findViewById(R.id.profil_name);
        profil_logout = getView().findViewById(R.id.profil_logout);
        reset_password = getView().findViewById(R.id.reset_password);
        profil_photo = getView().findViewById(R.id.profil_photo);

        profil_name = (TextView)getView().findViewById(R.id.profil_name);
        tv_nrp = (TextView)getView().findViewById(R.id.tv_nrp);
        tv_telp = (TextView)getView().findViewById(R.id.tv_telp);
        tv_fungsi = (TextView)getView().findViewById(R.id.tv_fungsi);
        tv_jabatan = (TextView)getView().findViewById(R.id.tv_jabatan);
        tv_kode_satwil = (TextView)getView().findViewById(R.id.tv_kode_satwil);
        tv_satwil = (TextView)getView().findViewById(R.id.tv_satwil);
        tv_versi = (TextView)getView().findViewById(R.id.tv_versi);

        profil_name.setText(SharedPreferences.getNama(getContext()));
        tv_nrp.setText(SharedPreferences.getNrp(getContext()));
        tv_telp.setText(SharedPreferences.getTlp(getContext()));
        tv_fungsi.setText(SharedPreferences.getFungsi(getContext()));
        tv_jabatan.setText(SharedPreferences.getJabatan(getContext()));
        tv_kode_satwil.setText(SharedPreferences.getSatwilTextKode(getContext()));
        tv_satwil.setText(SharedPreferences.getSatwilText(getContext()));
        tv_versi.setText(BuildConfig.VERSION_NAME);

        profil_logout.setVisibility(
            Helper.isEmptyString(SharedPreferences.getToken(getContext()))
                ? View.GONE : View.VISIBLE);

        String foto = SharedPreferences.getFoto(getContext());

        if (!Helper.isEmptyString(foto))
            Picasso.with(getContext()).load(foto)
                    .placeholder(R.drawable.ic_icon_police).into(profil_photo);


        profil_name.setOnClickListener(this);
        reset_password.setOnClickListener(this);
        profil_logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profil_name:
                Intent intent = new Intent(getActivity(), ActivityEditProfile.class);
                Helper.gotoActivity(getActivity(), intent, Helper.Transition.FADE);
            break;
            case R.id.reset_password:
                changePassword();
                break;
            case R.id.profil_logout:
                confirmDialog();
            break;
        }
    }

    private void stopTrackerService() {
        getActivity().stopService(new Intent(getActivity(), TrackerService.class));
    }

    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
            .setMessage("Apakah anda ingin logout?")
            .setPositiveButton("Ya",  new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    stopTrackerService();
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            })
            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int id) {
                    dialog.cancel();
                }
            })
            .show();
    }

    private void changePassword(){
        Intent logInIntent = new Intent(getActivity(), ActivityChangePassword.class);
        startActivity(logInIntent);
        getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
