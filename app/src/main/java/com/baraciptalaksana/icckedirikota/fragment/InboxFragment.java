package com.baraciptalaksana.icckedirikota.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baraciptalaksana.icckedirikota.adapter.CommandAdapter;
import com.baraciptalaksana.icckedirikota.adapter.InboxAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Inbox;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;

import java.util.ArrayList;
import java.util.List;

public class InboxFragment extends Fragment {

    List<Inbox> rowListItem = new ArrayList<Inbox>();
    InboxAdapter rcAdapter;
    private GridLayoutManager lLayout;
    RecyclerView rView;

    public static InboxFragment newInstance() {

        return new InboxFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inbox, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<Inbox> rowListItem = getAllItemList();
        rView = (RecyclerView)getView().findViewById(R.id.recyclerview);
        lLayout = new GridLayoutManager(getActivity(), 1);
        rView.setLayoutManager(lLayout);
        rView.setHasFixedSize(true);
        rcAdapter = new InboxAdapter(getActivity(), rowListItem);
        rView.setItemAnimator(new DefaultItemAnimator());
        rView.setAdapter(rcAdapter);
    }

    private ArrayList<Inbox> getAllItemList() {
        ArrayList<Inbox> allItems = new ArrayList<Inbox>();
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Pengumuman", R.drawable._inbox_pengumaman));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Peringatan", R.drawable._inbox_peringatan));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Private", R.drawable.photo_female_1));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Private", R.drawable.photo_female_1));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Peringatan", R.drawable._inbox_peringatan));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Pengumuman", R.drawable._inbox_pengumaman));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Peringatan", R.drawable._inbox_peringatan));
        allItems.add(new Inbox("2018-06-27","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur ipsum in placerat molestie.  Fusce quis mauris a enim sollicitudin", "Pengumuman", R.drawable._inbox_pengumaman));
        return allItems;
    }

//R.drawable.laporan_informasi,
}
