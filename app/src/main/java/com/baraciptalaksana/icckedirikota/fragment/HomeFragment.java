package com.baraciptalaksana.icckedirikota.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.activity.ActivityCheckinBarcode;
import com.baraciptalaksana.icckedirikota.activity.ActivityCheckinBarcodeResult;
import com.baraciptalaksana.icckedirikota.activity.ActivityCheckinPosgatur;
import com.baraciptalaksana.icckedirikota.activity.ActivityCheckoutBarcode;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailHumas;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailLaka;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailLapinfo;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailProblemSolve;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailSambang;
import com.baraciptalaksana.icckedirikota.activity.ActivityHistoryHumas;
import com.baraciptalaksana.icckedirikota.activity.ActivityHistoryLapinfo;
import com.baraciptalaksana.icckedirikota.activity.ActivityJadwalKegiatan;
import com.baraciptalaksana.icckedirikota.activity.ActivityLogin;
import com.baraciptalaksana.icckedirikota.activity.ActivityLokasiPetugasTps;
import com.baraciptalaksana.icckedirikota.activity.ActivityLokasiPlottingPam;
import com.baraciptalaksana.icckedirikota.activity.ActivityPerformanceAnggota;
import com.baraciptalaksana.icckedirikota.activity.ActivityPerhitunganSuara;
import com.baraciptalaksana.icckedirikota.activity.ActivityPlottingPerhitunganSuara;
import com.baraciptalaksana.icckedirikota.activity.ActivityPolicePosition;
import com.baraciptalaksana.icckedirikota.activity.ActivityPosGaturCheckinResult;
import com.baraciptalaksana.icckedirikota.activity.ActivityPosgaturCheckinBarcode;
import com.baraciptalaksana.icckedirikota.activity.ActivityProblemSolving;
import com.baraciptalaksana.icckedirikota.activity.ActivityReportInformation;
import com.baraciptalaksana.icckedirikota.activity.ActivityReportLaka;
import com.baraciptalaksana.icckedirikota.activity.ActivityReportSambang;
import com.baraciptalaksana.icckedirikota.activity.ActivityRutePatroli;
import com.baraciptalaksana.icckedirikota.adapter.HomeHumasAdapter;
import com.baraciptalaksana.icckedirikota.adapter.HomeLakaAdapter;
import com.baraciptalaksana.icckedirikota.adapter.HomeLapinfoAdapter;
import com.baraciptalaksana.icckedirikota.adapter.HomeProblemSolveAdapter;
import com.baraciptalaksana.icckedirikota.adapter.HomeSambangAdapter;
import com.baraciptalaksana.icckedirikota.adapter.MainmenuAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Berita;
import com.baraciptalaksana.icckedirikota.models.Laka;
import com.baraciptalaksana.icckedirikota.models.Lapinfo;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;
import com.baraciptalaksana.icckedirikota.models.ProblemSolve;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Sambang;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.utils.StartSnapHelper;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class HomeFragment extends Fragment {
    //LOGINDATA
    TextView userRank;
    TextView userName;
    ImageView userPicture;

    //LAPINFO
    private ArrayList<Lapinfo> rowListItemLapinfo = new ArrayList<Lapinfo>();
    HomeLapinfoAdapter RecyclerViewHorizontalAdapterInformasi;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerInformasi;
    RotateLoading loading_lapinfo;
    LinearLayoutManager HorizontalLayoutInformasi;

    //SAMBANG
    private ArrayList<Sambang> rowListItemSambang = new ArrayList<Sambang>();
    HomeSambangAdapter RecyclerViewHorizontalAdapterSambang;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerSambang;
    RotateLoading loading_sambang;
    LinearLayoutManager HorizontalLayoutSambang;

    //PROBLEMSOLVE
    private ArrayList<ProblemSolve> rowListItemProblemSolve = new ArrayList<ProblemSolve>();
    HomeProblemSolveAdapter RecyclerViewHorizontalAdapterProblemSolve;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerProblemSolve;
    RotateLoading loading_problemsolve;
    LinearLayoutManager HorizontalLayoutProblemSolve;

    //LAKANTAS
    private ArrayList<Laka> rowListItemLaka = new ArrayList<Laka>();
    HomeLakaAdapter RecyclerViewHorizontalAdapterLaka;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerLaka;
    RotateLoading loading_laka;
    LinearLayoutManager HorizontalLayoutLaka;

    //MENU
    ArrayList<Mainmenu> rowListItemMenu = new ArrayList<Mainmenu>();
    MainmenuAdapter RecyclerViewHorizontalAdapterMenu;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerMenu;
    RotateLoading loading_menu;
    LinearLayoutManager HorizontalLayoutMenu;

    //HUMAS
    ArrayList<Berita> rowListItemBerita = new ArrayList<Berita>();
    HomeHumasAdapter RecyclerViewHorizontalAdapterHumas;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerHumas;
    RotateLoading loading_humas;
    LinearLayoutManager HorizontalLayoutHumas;

    //PEMILU
    ArrayList<Mainmenu> rowListItemMenuPemilu = new ArrayList<Mainmenu>();
    MainmenuAdapter RecyclerViewHorizontalAdapterPemilu;
    RecyclerView.LayoutManager RecyclerViewLayoutManagerPemilu;
    RotateLoading loading_menu_pemilu;
    LinearLayoutManager HorizontalLayoutPemilu;

    RecyclerView recyclerview_menu, recyclerview_lap_pemilu, recyclerview_lap_informasi, recyclerview_lap_sambang, recyclerview_lap_problemsolve, recyclerview_lap_laka, recyclerview_berita_humas;


    View ChildView;
    int RecyclerViewItemPosition;

    public HomeFragment() {

    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //USER LOGIN DATA
        userRank = getView().findViewById(R.id.tv_user_rank);
        userName = getView().findViewById(R.id.tv_user_name);
        userPicture = getView().findViewById(R.id.iv_user_picture);

        userRank.setText(SharedPreferences.getPangkat(getContext()));
        userName.setText(SharedPreferences.getNama(getContext()));
        String foto = SharedPreferences.getFoto(getContext());

        if (!Helper.isEmptyString(foto))
            Picasso.with(getContext()).load(foto)
                .placeholder(R.drawable.ic_icon_police).into(userPicture);

        recyclerview_menu = (RecyclerView)getView().findViewById(R.id.recyclerview_menu);
        SnapHelper snapHelperMenu = new StartSnapHelper();
        snapHelperMenu.attachToRecyclerView(recyclerview_menu);

        loading_lapinfo = (RotateLoading)getView().findViewById(R.id.loading_lapinfo);
        loading_sambang = (RotateLoading)getView().findViewById(R.id.loading_sambang);
        loading_problemsolve = (RotateLoading)getView().findViewById(R.id.loading_problemsolve);
        loading_laka = (RotateLoading)getView().findViewById(R.id.loading_laka);
        loading_menu = (RotateLoading)getView().findViewById(R.id.loading_menu);
        loading_menu_pemilu = (RotateLoading)getView().findViewById(R.id.loading_menu_pemilu);
        loading_humas = (RotateLoading)getView().findViewById(R.id.loading_humas);

        //PEMILU
        recyclerview_lap_pemilu = (RecyclerView)getView().findViewById(R.id.recyclerview_lap_pemilu);
        SnapHelper snapHelperPemilu = new StartSnapHelper();
        snapHelperPemilu.attachToRecyclerView(recyclerview_lap_pemilu);

        //LAPINFO
        recyclerview_lap_informasi = (RecyclerView)getView().findViewById(R.id.recyclerview_lap_informasi);
        SnapHelper snapHelperInformasi = new StartSnapHelper();
        snapHelperInformasi.attachToRecyclerView(recyclerview_lap_informasi);

        //SAMBANG
        recyclerview_lap_sambang = (RecyclerView)getView().findViewById(R.id.recyclerview_lap_sambang);
        SnapHelper snapHelperSambang = new StartSnapHelper();
        snapHelperSambang.attachToRecyclerView(recyclerview_lap_sambang);

        //PROBLEMSOLVE
        recyclerview_lap_problemsolve = (RecyclerView)getView().findViewById(R.id.recyclerview_lap_problemsolve);
        SnapHelper snapHelperProblemSolve = new StartSnapHelper();
        snapHelperProblemSolve.attachToRecyclerView(recyclerview_lap_problemsolve);

        //LAKANTAS
        recyclerview_lap_laka = (RecyclerView)getView().findViewById(R.id.recyclerview_lap_laka);
        SnapHelper snapHelperLaka = new StartSnapHelper();
        snapHelperLaka.attachToRecyclerView(recyclerview_lap_laka);

        //HUMAS
        recyclerview_berita_humas = (RecyclerView)getView().findViewById(R.id.recyclerview_berita_humas);
        SnapHelper snapHelperHumas = new StartSnapHelper();
        snapHelperHumas.attachToRecyclerView(recyclerview_berita_humas);

        RecyclerViewLayoutManagerMenu = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerPemilu = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerInformasi = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerSambang = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerProblemSolve = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerLaka = new LinearLayoutManager(getActivity());
        RecyclerViewLayoutManagerHumas = new LinearLayoutManager(getActivity());

        recyclerview_menu.setLayoutManager(RecyclerViewLayoutManagerMenu);
        recyclerview_lap_pemilu.setLayoutManager(RecyclerViewLayoutManagerPemilu);
        recyclerview_lap_informasi.setLayoutManager(RecyclerViewLayoutManagerInformasi);
        recyclerview_lap_sambang.setLayoutManager(RecyclerViewLayoutManagerSambang);
        recyclerview_lap_problemsolve.setLayoutManager(RecyclerViewLayoutManagerProblemSolve);
        recyclerview_lap_laka.setLayoutManager(RecyclerViewLayoutManagerLaka);
        recyclerview_berita_humas.setLayoutManager(RecyclerViewLayoutManagerHumas);

        //rowListItemMenu = getAllItemList();
        //Pemilu = getAllItemListPemilu();
        getListMainMenu(SharedPreferences.getToken(getActivity()), SharedPreferences.getFungsi(getActivity()));
        getListMainMenuPemilu(SharedPreferences.getToken(getActivity()), SharedPreferences.getFungsi(getActivity()));
        getListHomeLapinfo(SharedPreferences.getToken(getActivity()), 1);
        getListHomeSambang(SharedPreferences.getToken(getActivity()), 1);
        getListHomeProblemSolve(SharedPreferences.getToken(getActivity()), 1);
        getListHomeLaka(SharedPreferences.getToken(getActivity()), 1);
        getListHomeHumas(SharedPreferences.getToken(getActivity()));

        RecyclerViewHorizontalAdapterInformasi = new HomeLapinfoAdapter(getActivity(), rowListItemLapinfo);
        RecyclerViewHorizontalAdapterSambang = new HomeSambangAdapter(getActivity(), rowListItemSambang);
        RecyclerViewHorizontalAdapterProblemSolve = new HomeProblemSolveAdapter(getActivity(), rowListItemProblemSolve);
        RecyclerViewHorizontalAdapterLaka = new HomeLakaAdapter(getActivity(), rowListItemLaka);
        RecyclerViewHorizontalAdapterMenu = new MainmenuAdapter(getActivity(), rowListItemMenu);
        RecyclerViewHorizontalAdapterHumas = new HomeHumasAdapter(getActivity(), rowListItemBerita);
        RecyclerViewHorizontalAdapterPemilu = new MainmenuAdapter(getActivity(), rowListItemMenuPemilu);

        HorizontalLayoutMenu = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutPemilu = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutInformasi = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutSambang = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutProblemSolve = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutLaka = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        HorizontalLayoutHumas = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerview_menu.setLayoutManager(HorizontalLayoutMenu);
        recyclerview_menu.setAdapter(RecyclerViewHorizontalAdapterMenu);

        recyclerview_lap_pemilu.setLayoutManager(HorizontalLayoutPemilu);
        recyclerview_lap_pemilu.setAdapter(RecyclerViewHorizontalAdapterPemilu);

        recyclerview_lap_informasi.setLayoutManager(HorizontalLayoutInformasi);
        recyclerview_lap_informasi.setAdapter(RecyclerViewHorizontalAdapterInformasi);

        recyclerview_lap_sambang.setLayoutManager(HorizontalLayoutSambang);
        recyclerview_lap_sambang.setAdapter(RecyclerViewHorizontalAdapterSambang);

        recyclerview_lap_problemsolve.setLayoutManager(HorizontalLayoutProblemSolve);
        recyclerview_lap_problemsolve.setAdapter(RecyclerViewHorizontalAdapterProblemSolve);

        recyclerview_lap_laka.setLayoutManager(HorizontalLayoutLaka);
        recyclerview_lap_laka.setAdapter(RecyclerViewHorizontalAdapterLaka);

        recyclerview_berita_humas.setLayoutManager(HorizontalLayoutHumas);
        recyclerview_berita_humas.setAdapter(RecyclerViewHorizontalAdapterHumas);

        //ACTION
        recyclerview_menu.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);

                    switch (rowListItemMenu.get(RecyclerViewItemPosition).getNama().toString()){
                        case"Patroli Barcode":
                            if(SharedPreferences.getPosPatroliStatus(getActivity()) !=null && SharedPreferences.getPosPatroliId(getActivity()) !=null && SharedPreferences.getPosPatroliStatus(getActivity()).equalsIgnoreCase("true") && !SharedPreferences.getPosPatroliId(getActivity()).equalsIgnoreCase("0")){
                                Intent logInIntent = new Intent(getActivity(), ActivityCheckinBarcodeResult.class);
                                startActivity(logInIntent);
                                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }else{
                                Intent logInIntent = new Intent(getActivity(), ActivityCheckinBarcode.class);
                                startActivity(logInIntent);
                                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        break;
                        case "Laporan Informasi":
                            Intent logInIntentInformasi = new Intent(getActivity(), ActivityReportInformation.class);
                            startActivity(logInIntentInformasi);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Laporan Sambang":
                            Intent logInIntentSambang = new Intent(getActivity(), ActivityReportSambang.class);
                            startActivity(logInIntentSambang);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Problem Solving":
                            Intent logInIntentProblemSolving = new Intent(getActivity(), ActivityProblemSolving.class);
                            startActivity(logInIntentProblemSolving);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Rute Patroli":
                            Intent logInIntentRutePatroli = new Intent(getActivity(), ActivityRutePatroli.class);
                            startActivity(logInIntentRutePatroli);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Checkin Pos Gatur":
                            if(SharedPreferences.getPosGaturStatus(getActivity()) !=null && SharedPreferences.getPosGaturId(getActivity()) !=null && SharedPreferences.getPosGaturStatus(getActivity()).equalsIgnoreCase("true") && !SharedPreferences.getPosGaturId(getActivity()).equalsIgnoreCase("0")){
                                Intent logInIntentCheckinPosGatur = new Intent(getActivity(), ActivityPosGaturCheckinResult.class);
                                logInIntentCheckinPosGatur.putExtra("Id",Integer.parseInt(SharedPreferences.getPosGaturId(getActivity())));
                                startActivity(logInIntentCheckinPosGatur);
                                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }else{
                                Intent logInIntentCheckinPosGatur = new Intent(getActivity(), ActivityCheckinPosgatur.class);
                                startActivity(logInIntentCheckinPosGatur);
                                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                            }
                        break;
                        case "Laporan Lakalantas":
                            Intent logInIntentLaka = new Intent(getActivity(), ActivityReportLaka.class);
                            startActivity(logInIntentLaka);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Performance":
                            Intent logInIntentPerformance = new Intent(getActivity(), ActivityPerformanceAnggota.class);
                            startActivity(logInIntentPerformance);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Posisi Anggota":
                            Intent logInIntentPosAnggota = new Intent(getActivity(), ActivityPolicePosition.class);
                            startActivity(logInIntentPosAnggota);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        case "Jadwal Kegiatan":
                            Intent logInIntentJadwalKegiatan = new Intent(getActivity(), ActivityJadwalKegiatan.class);
                            startActivity(logInIntentJadwalKegiatan);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        break;
                        default:
                            Intent logInIntentDefault = new Intent(getActivity(), ActivityReportInformation.class);
                            startActivity(logInIntentDefault);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        recyclerview_lap_pemilu.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    //Getting clicked value.
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    if(rowListItemMenuPemilu.get(RecyclerViewItemPosition).getNama().toString().equalsIgnoreCase("Plotting PAM Pemilu")){
                        Intent logInIntent = new Intent(getActivity(), ActivityLokasiPlottingPam.class);
                        startActivity(logInIntent);
                        getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    }else if(rowListItemMenuPemilu.get(RecyclerViewItemPosition).getNama().toString().equalsIgnoreCase("Hasil Perhitungan Suara")){
                        if(SharedPreferences.getJabatanLevel(getActivity()).equalsIgnoreCase("Kapolres")){
                            Intent logInIntent = new Intent(getActivity(), ActivityPlottingPerhitunganSuara.class);
                            startActivity(logInIntent);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        }else{
                            Intent logInIntent = new Intent(getActivity(), ActivityPerhitunganSuara.class);
                            startActivity(logInIntent);
                            getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        }
                    }else if(rowListItemMenuPemilu.get(RecyclerViewItemPosition).getNama().toString().equalsIgnoreCase("Lokasi TPS")){
                        Intent logInIntentPilkada = new Intent(getActivity(), ActivityLokasiPetugasTps.class);
                        startActivity(logInIntentPilkada);
                        getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        //LAPINFO
        recyclerview_lap_informasi.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    Lapinfo lapinfo = rowListItemLapinfo.get(RecyclerViewItemPosition);

                    Intent intent;
                    if (lapinfo.isMore()) {
                        intent = new Intent(getActivity(), ActivityHistoryLapinfo.class);
                    } else {
                        intent = new Intent(getActivity(), ActivityDetailLapinfo.class);
                    }

                    intent.putExtra("Id", lapinfo.getId());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        //SAMBANG
        recyclerview_lap_sambang.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    Intent intent = new Intent(getActivity(), ActivityDetailSambang.class);
                    intent.putExtra("Id", rowListItemSambang.get(RecyclerViewItemPosition).getId());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        //PROBLEMSOLVE
        recyclerview_lap_problemsolve.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    Intent intent = new Intent(getActivity(), ActivityDetailProblemSolve.class);
                    intent.putExtra("Id", rowListItemProblemSolve.get(RecyclerViewItemPosition).getId());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        //LAKANTAS
        recyclerview_lap_laka.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    Intent intent = new Intent(getActivity(), ActivityDetailLaka.class);
                    intent.putExtra("Id", rowListItemLaka.get(RecyclerViewItemPosition).getId());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        recyclerview_berita_humas.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {
                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {

                    ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                    if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {
                        RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);

                        Berita berita = rowListItemBerita.get(RecyclerViewItemPosition);

                        Intent intent;
                        if (berita.isMore()) {
                            intent = new Intent(getActivity(), ActivityHistoryHumas.class);
                        } else {
                            intent = new Intent(getActivity(), ActivityDetailHumas.class);
                        }

                        intent.putExtra("Id", berita.getId());
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

                    }
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private ArrayList<Mainmenu> getAllItemList() {
        ArrayList<Mainmenu> allItems = new ArrayList<Mainmenu>();
        allItems.add(new Mainmenu(R.drawable.laporan_informasi, "Laporan Informasi"));
        allItems.add(new Mainmenu(R.drawable.laporan_sambang, "Laporan Sambang"));
        allItems.add(new Mainmenu(R.drawable.problem_solve, "Problem Solving"));
        allItems.add(new Mainmenu(R.drawable.patroli_barcode, "Patroli Barcode"));
        allItems.add(new Mainmenu(R.drawable.rute_patroli, "Rute Patroli"));
        allItems.add(new Mainmenu(R.drawable.check_in_postgatur, "Checkin Pos Gatur"));
        allItems.add(new Mainmenu(R.drawable.laporan_laka, "Laporan Lakalantas"));
        allItems.add(new Mainmenu(R.drawable.performance, "Performance"));
        allItems.add(new Mainmenu(R.drawable.laporan_anggota, "Posisi Anggota"));
        allItems.add(new Mainmenu(R.drawable.jadwal_kegiatan, "Jadwal & Kegiatan"));
        return allItems;
    }

    private ArrayList<Mainmenu> getAllItemListPemilu() {
        ArrayList<Mainmenu> allItems = new ArrayList<Mainmenu>();
        allItems.add(new Mainmenu(R.drawable.lapsus_pilkada, "Lokasi TPS"));
        allItems.add(new Mainmenu(R.drawable.ploating_pam, "Plotting PAM Pemilu"));
        allItems.add(new Mainmenu(R.drawable.perhitungan_suara, "Hasil Perhitungan Suara"));
        return allItems;
    }

    private void getListMainMenu(String token, String fungsi){
        loading_menu.start();
        ApiInterface.Factory.getInstance().listmenu("Bearer " + token, fungsi).enqueue(new Callback<Response.ResponseListMenu>() {
            @Override
            public void onResponse(Call<Response.ResponseListMenu> call, retrofit2.Response<Response.ResponseListMenu> response) {
                loading_menu.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemMenu.clear();
                        rowListItemMenu.addAll(response.body().getData());
                        RecyclerViewHorizontalAdapterMenu.notifyDataSetChanged();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                        SharedPreferences.logout(getContext());
                        Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                        Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListMenu> call, Throwable t) {
                loading_menu.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListMainMenuPemilu(String token, String fungsi){
        loading_menu_pemilu.start();
        ApiInterface.Factory.getInstance().listmenupemilu("Bearer " + token, fungsi).enqueue(new Callback<Response.ResponseListMenu>() {
            @Override
            public void onResponse(Call<Response.ResponseListMenu> call, retrofit2.Response<Response.ResponseListMenu> response) {
                loading_menu_pemilu.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemMenuPemilu.clear();
                        rowListItemMenuPemilu.addAll(response.body().getData());
                        RecyclerViewHorizontalAdapterPemilu.notifyDataSetChanged();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        if (getActivity() != null)
                            Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();

                        SharedPreferences.logout(getContext());
                        Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                        Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListMenu> call, Throwable t) {
                loading_menu_pemilu.stop();
                t.printStackTrace();
            }
        });
    }


    private void getListHomeLapinfo(String token, int offsets){
        loading_lapinfo.start();
        ApiInterface.Factory.getInstance().homelapinfo("Bearer " + token, offsets).enqueue(new Callback<Response.ResponseListLapinfo>() {
            @Override
            public void onResponse(Call<Response.ResponseListLapinfo> call, retrofit2.Response<Response.ResponseListLapinfo> response) {
                loading_lapinfo.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemLapinfo.clear();
                        rowListItemLapinfo.addAll(response.body().getData());

                        if (response.body().getData().size() == 5)
                            rowListItemLapinfo.add(Lapinfo.genMore());

                        RecyclerViewHorizontalAdapterInformasi.notifyDataSetChanged();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                        SharedPreferences.logout(getContext());
                        Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                        Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListLapinfo> call, Throwable t) {
                loading_lapinfo.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListHomeSambang(String token, int offsets){
        loading_sambang.start();
        ApiInterface.Factory.getInstance().homesambang("Bearer " + token, offsets).enqueue(new Callback<Response.ResponseListSambang>() {
            @Override
            public void onResponse(Call<Response.ResponseListSambang> call, retrofit2.Response<Response.ResponseListSambang> response) {
                loading_sambang.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemSambang.clear();
                        rowListItemSambang.addAll(response.body().getData());
                        RecyclerViewHorizontalAdapterSambang.notifyDataSetChanged();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListSambang> call, Throwable t) {
                loading_sambang.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListHomeProblemSolve(String token, int offsets){
        loading_problemsolve.start();
        ApiInterface.Factory.getInstance().homeproblemsolve("Bearer " + token, offsets).enqueue(new Callback<Response.ResponseListProblemSolve>() {
            @Override
            public void onResponse(Call<Response.ResponseListProblemSolve> call, retrofit2.Response<Response.ResponseListProblemSolve> response) {
                loading_problemsolve.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemProblemSolve.clear();
                        rowListItemProblemSolve.addAll(response.body().getData());
                        RecyclerViewHorizontalAdapterProblemSolve.notifyDataSetChanged();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListProblemSolve> call, Throwable t) {
                loading_problemsolve.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListHomeLaka(String token, int offsets){
        loading_laka.start();
        ApiInterface.Factory.getInstance().homelaka("Bearer " + token, offsets).enqueue(new Callback<Response.ResponseListLaka>() {
            @Override
            public void onResponse(Call<Response.ResponseListLaka> call, retrofit2.Response<Response.ResponseListLaka> response) {
                loading_laka.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemLaka.clear();
                        rowListItemLaka.addAll(response.body().getData());
                        RecyclerViewHorizontalAdapterLaka.notifyDataSetChanged();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListLaka> call, Throwable t) {
                loading_laka.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListHomeHumas(String token){
        loading_humas.start();
        ApiInterface.Factory.getInstance().listhomeberita("Bearer " + token, 1).enqueue(new Callback<Response.ResponseListBerita>() {
            @Override
            public void onResponse(Call<Response.ResponseListBerita> call, retrofit2.Response<Response.ResponseListBerita> response) {
                loading_humas.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItemBerita.clear();
                        rowListItemBerita.addAll(response.body().getData());

                        if (response.body().getData().size() == 5)
                            rowListItemBerita.add(Berita.genMore());

                        RecyclerViewHorizontalAdapterHumas.notifyDataSetChanged();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                        SharedPreferences.logout(getContext());
                        Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                        Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListBerita> call, Throwable t) {
                loading_humas.stop();
                t.printStackTrace();
            }
        });
    }
}
