package com.baraciptalaksana.icckedirikota.fragment;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.activity.ActivityLogin;
import com.baraciptalaksana.icckedirikota.activity.ActivityPenugasanCreate;
import com.baraciptalaksana.icckedirikota.adapter.PenugasanAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Penugasan;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Tugas;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class CommandFragment extends Fragment implements Paginate.Callbacks{

    private ArrayList<Tugas> rowListItem = new ArrayList<Tugas>();
    private PenugasanAdapter rcAdapter;
    private GridLayoutManager lLayout;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.swipeRefreshLayout)
    PullRefreshLayout swipeRefreshLayout;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private Paginate paginate;
    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;

    public static CommandFragment newInstance() {
        return new CommandFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_command, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lLayout = new GridLayoutManager(getActivity(), 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new PenugasanAdapter(getActivity(), rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListPenugasan(SharedPreferences.getToken(getActivity()), SharedPreferences.getNrp(getActivity()), 1);
            }
        });

        swipeRefreshLayout.setRefreshing(true);
        getListPenugasan(SharedPreferences.getToken(getActivity()), SharedPreferences.getNrp(getActivity()), offset);

        paginate = Paginate.with(recyclerview, this)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .build();

        paginate.setHasMoreDataToLoad(false);

        if (SharedPreferences.getFungsi(getContext()).equalsIgnoreCase("ADMINISTRATOR")){
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent logInIntent = new Intent(getActivity(), ActivityPenugasanCreate.class);
                startActivity(logInIntent);
                getActivity().overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });
    }

    private void getListPenugasan(String token, String nrp, final int offsets){
        ApiInterface.Factory.getInstance().listtugas("Bearer " + token, nrp, offsets).enqueue(new Callback<Response.ResponseListTugas>() {
            @Override
            public void onResponse(Call<Response.ResponseListTugas> call, retrofit2.Response<Response.ResponseListTugas> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                        paginate.setHasMoreDataToLoad(false);
                    }
                } else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(getActivity(), "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(getContext());
                    Intent logInIntent = new Intent(getActivity(), ActivityLogin.class);
                    Helper.gotoActivityWithFinish(getActivity(), logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response.ResponseListTugas> call, Throwable t) {
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListPenugasan(SharedPreferences.getToken(getActivity()), SharedPreferences.getNrp(getActivity()), offset);
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_sambang));
                loading.start();
            }
        }
    }
}
