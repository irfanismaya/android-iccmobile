package com.baraciptalaksana.icckedirikota.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baraciptalaksana.icckedirikota.activity.ActivityLokasiPetugasTps;
import com.baraciptalaksana.icckedirikota.adapter.PenugasanAdapter;
import com.baraciptalaksana.icckedirikota.adapter.PetugasTpsAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Penugasan;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Tps;
import com.baraciptalaksana.icckedirikota.models.Tugas;
import com.paginate.Paginate;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class PenugasanFragment extends Fragment {

    private ArrayList<Tugas> rowListItem = new ArrayList<Tugas>();
    private PenugasanAdapter rcAdapter;
    private GridLayoutManager lLayout;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.loading)
    RotateLoading loading;
    @BindView(R.id.swipeRefreshLayout)
    PullRefreshLayout swipeRefreshLayout;

    public PenugasanFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_penugasan, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lLayout = new GridLayoutManager(getActivity(), 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new PenugasanAdapter(getActivity(), rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                rowListItem.clear();
                getListPenugasan(SharedPreferences.getToken(getActivity()), SharedPreferences.getNrp(getActivity()), 1);
            }
        });

        getListPenugasan(SharedPreferences.getToken(getActivity()), SharedPreferences.getNrp(getActivity()), 1);
    }

    private void getListPenugasan(String token, String nrp, int offset){
        loading.start();
        ApiInterface.Factory.getInstance().listtugas("Bearer " + token, nrp, offset).enqueue(new Callback<Response.ResponseListTugas>() {
            @Override
            public void onResponse(Call<Response.ResponseListTugas> call, retrofit2.Response<Response.ResponseListTugas> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListTugas> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }
}

