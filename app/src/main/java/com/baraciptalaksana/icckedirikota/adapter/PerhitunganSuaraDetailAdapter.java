package com.baraciptalaksana.icckedirikota.adapter;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.DetailSuara;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PerhitunganSuaraDetailAdapter extends RecyclerView.Adapter<PerhitunganSuaraDetailAdapter.ViewHolder> implements Filterable {
    private ArrayList<DetailSuara> mArrayList;
    public ArrayList<DetailSuara> mFilteredList;
    private Activity mActivity;

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_suara, tv_paslon, tv_peserta;
        ImageView image;
        CardView cvPaslon;
        public ViewHolder(View view) {
            super(view);
            tv_suara = view.findViewById(R.id.tv_suara);
            tv_paslon = view.findViewById(R.id.tv_paslon);
            tv_peserta = view.findViewById(R.id.tv_peserta);
            image = view.findViewById(R.id.image);
            cvPaslon = view.findViewById(R.id.row_paslon);
        }
    }

    public PerhitunganSuaraDetailAdapter(Activity activity, ArrayList<DetailSuara> arrayList) {
        this.mActivity = activity;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public PerhitunganSuaraDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_detail_perhitungansuara, viewGroup, false);
        return new PerhitunganSuaraDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PerhitunganSuaraDetailAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof PerhitunganSuaraDetailAdapter.ViewHolder) {
            viewHolder.tv_suara.setText(""+mFilteredList.get(position).getSuara());
            viewHolder.tv_paslon.setText("No. " + mFilteredList.get(position).getNomorUrut());
            viewHolder.tv_peserta.setText(""+mFilteredList.get(position).getPeserta().replace(" - ", "\n"));

            if (mFilteredList.get(position).getNomorUrut() == null) {
                Picasso.with(mActivity).load(R.drawable.ic_vote_pemilu).into(viewHolder.image);
            } else if (mFilteredList.get(position).getNomorUrut() == 0) {
                Picasso.with(mActivity).load(R.drawable.ic_circle_pemilu).into(viewHolder.image);
                viewHolder.tv_paslon.setText("");
                CharSequence text = viewHolder.tv_peserta.getText();
                viewHolder.tv_peserta.setText(text + "\n");
                viewHolder.image.setColorFilter(mActivity.getResources().getColor(R.color.color_problem_solving));
            } else {
                Picasso.with(mActivity).load(R.drawable.ic_circle_pemilu).into(viewHolder.image);
                int i = mFilteredList.get(position).getNomorUrut() % Constants.PEMILU_COLORS.length;
                viewHolder.image.setColorFilter(mActivity.getResources().getColor(Constants.PEMILU_COLORS[i]));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<DetailSuara> filteredList = new ArrayList<>();
                    for (DetailSuara detailSuara : mArrayList) {
                        if (detailSuara.getPersentase().toLowerCase().contains(charString) || detailSuara.getPeserta().toLowerCase().contains(charString)) {
                            filteredList.add(detailSuara);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<DetailSuara>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
