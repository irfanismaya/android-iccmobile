package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPetugasTps;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPlottingTps;
import com.baraciptalaksana.icckedirikota.models.Tps;

import java.util.ArrayList;

public class PlottingTpsAdapter extends RecyclerView.Adapter<PlottingTpsAdapter.ViewHolder> implements Filterable {
    private ArrayList<Tps> mArrayList;
    private ArrayList<Tps> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cd_detail;
        TextView tv_distance, tv_name, tv_address;
        ImageButton ivb_direction;
        LinearLayout ll_detail;
        public ViewHolder(View view) {
            super(view);
            tv_distance = view.findViewById(R.id.tv_distance);
            tv_name = view.findViewById(R.id.tv_name);
            tv_address = view.findViewById(R.id.tv_address);
            cd_detail = view.findViewById(R.id.cd_detail);
            ivb_direction = view.findViewById(R.id.ivb_direction);
            ll_detail = view.findViewById(R.id.ll_detail);
        }
    }

    public PlottingTpsAdapter(Context context, ArrayList<Tps> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public PlottingTpsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_plotting_tps, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlottingTpsAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {

            if (position % 2 == 1) {
                viewHolder.ll_detail.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                viewHolder.ll_detail.setBackgroundColor(Color.parseColor("#e6e6e6"));
            }

            viewHolder.tv_distance.setText(mFilteredList.get(position).getJarak());
            viewHolder.tv_name.setText(mFilteredList.get(position).getTps());
            viewHolder.tv_address.setText("Kec. "+mFilteredList.get(position).getKecamatan()+", "+"Kel. "+mFilteredList.get(position).getKelurahan());

            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPlottingTps.class);
                    intent.putExtra("id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ivb_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("geo:"+mFilteredList.get(position).getLat()+","+mFilteredList.get(position).getLon()));
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Tps> filteredList = new ArrayList<>();
                    for (Tps tps : mArrayList) {
                        if (tps.getTps().toLowerCase().contains(charString) || tps.getKelurahan().toLowerCase().contains(charString) || tps.getKecamatan().toLowerCase().contains(charString)) {
                            filteredList.add(tps);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Tps>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
