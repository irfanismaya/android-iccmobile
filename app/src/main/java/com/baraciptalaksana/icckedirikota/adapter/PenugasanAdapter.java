package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailTugas;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Penugasan;
import com.baraciptalaksana.icckedirikota.models.Tugas;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PenugasanAdapter extends RecyclerView.Adapter<PenugasanAdapter.ViewHolder> implements Filterable {
    private ArrayList<Tugas> mArrayList;
    private ArrayList<Tugas> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_personil, tv_uraian, tv_time;
        private CardView cd_detail;
        private LinearLayout ll_color;
        CircleImageView image;
        RotateLoading progressbar;
        LinearLayout ll_detail;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tv_time = view.findViewById(R.id.tv_time);
            tv_personil = view.findViewById(R.id.tv_personil);
            ll_detail = view.findViewById(R.id.ll_detail);
            tv_uraian = view.findViewById(R.id.tv_uraian);
            cd_detail = view.findViewById(R.id.cd_detail);
            progressbar = view.findViewById(R.id.progressbar);
        }
    }

    public PenugasanAdapter(Context context, ArrayList<Tugas> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public PenugasanAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_penugasan, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PenugasanAdapter.ViewHolder viewHolder, final int i) {
        if (viewHolder instanceof ViewHolder) {

            if (i % 2 == 1) {
                viewHolder.ll_detail.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                viewHolder.ll_detail.setBackgroundColor(Color.parseColor("#e6e6e6"));
            }

            viewHolder.tv_time.setText(mFilteredList.get(i).getCreatedAt());
            viewHolder.tv_personil.setText(mFilteredList.get(i).getJabatan()+":");
            viewHolder.tv_uraian.setText(mFilteredList.get(i).getUraian());

            String url = Helper.imgUrl(mFilteredList
                    .get(i)
                    .getThumbnail(), Constants.IMG_DIR_ANGGOTA);

            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailTugas.class);
                    intent.putExtra("id", mFilteredList.get(i).getId());
                    intent.putExtra("jabatan", mFilteredList.get(i).getJabatan());
                    intent.putExtra("waktu", mFilteredList.get(i).getCreatedAt());
                    intent.putExtra("uraian", mFilteredList.get(i).getUraian());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });


            viewHolder.ll_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailTugas.class);
                    intent.putExtra("id", mFilteredList.get(i).getId());
                    intent.putExtra("jabatan", mFilteredList.get(i).getJabatan());
                    intent.putExtra("waktu", mFilteredList.get(i).getCreatedAt());
                    intent.putExtra("uraian", mFilteredList.get(i).getUraian());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Tugas> filteredList = new ArrayList<>();
                    for (Tugas tugas : mArrayList) {
                        if (tugas.getMenugasi().toLowerCase().contains(charString) || tugas.getUraian().toLowerCase().contains(charString)) {
                            filteredList.add(tugas);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Tugas>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
