package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;
import java.util.List;

public class MainmenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Mainmenu> itemList;
    private Context context;

    public class ItemHolder extends RecyclerView.ViewHolder {
        public TextView tv_adapter_mainmenu_title;
        public ImageView iv_adapter_mainmenu_image;

        public ItemHolder(View view){
            super(view);
            tv_adapter_mainmenu_title = view.findViewById(R.id.tv_adapter_mainmenu_title);
            iv_adapter_mainmenu_image = view.findViewById(R.id.iv_adapter_mainmenu_image);
        }
    }

    public MainmenuAdapter(Context context, List<Mainmenu> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_menu, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            final Mainmenu itemObject = itemList.get(position);
            ((ItemHolder) holder).tv_adapter_mainmenu_title.setText(itemObject.getNama());
            switch (itemObject.getNama()) {
                case "Laporan Informasi":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.laporan_informasi);
                    break;
                case "Laporan Sambang":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.laporan_sambang);
                    break;
                case "Problem Solving":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.problem_solve);
                    break;
                case "Patroli Barcode":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.patroli_barcode);
                    break;
                case "Rute Patroli":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.rute_patroli);
                    break;
                case "Checkin Pos Gatur":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.check_in_postgatur);
                    break;
                case "Laporan Lakalantas":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.laporan_laka);
                    break;
                case "Lokasi TPS":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.lapsus_pilkada);
                    break;
                case "Performance":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.performance);
                    break;
                case "Posisi Anggota":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.laporan_anggota);
                    break;
                case "Jadwal Kegiatan":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.jadwal_kegiatan);
                    break;
                case "Plotting PAM Pemilu":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.ploating_pam);
                    break;
                case "Hasil Perhitungan Suara":
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.perhitungan_suara);
                    break;
                default:
                    ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.no_image);
            }

        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void swapData(List<Mainmenu> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
