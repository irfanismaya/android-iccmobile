package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailLapinfo;
import com.baraciptalaksana.icckedirikota.activity.ActivityListCommentLapinfo;
import com.balysv.materialripple.MaterialRippleLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Lapinfo;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;

public class LapinfoAdapter extends RecyclerView.Adapter<LapinfoAdapter.ViewHolder> implements Filterable {
    private ArrayList<Lapinfo> mArrayList;
    private ArrayList<Lapinfo> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_image;
        TextView tv_category, tv_tgl_masuk, tv_seen, tv_comment;
        ImageButton ivb_seen, ivb_love, ivb_comment;
        MaterialRippleLayout mr_detail;
        RotateLoading progressbar;
        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_category = view.findViewById(R.id.tv_category);
            tv_tgl_masuk = view.findViewById(R.id.tv_tgl_massuk);
            tv_comment = view.findViewById(R.id.tv_comment);
            tv_seen =  view.findViewById(R.id.tv_seen);
            ivb_comment = view.findViewById(R.id.ivb_comment);
            ivb_seen = view.findViewById(R.id.ivb_seen);
            mr_detail = view.findViewById(R.id.mr_detail);
            progressbar = view.findViewById(R.id.progressbar);
        }
    }

    public LapinfoAdapter(Context context, ArrayList<Lapinfo> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public LapinfoAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_laporan, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LapinfoAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {
            String date = Helper.fullDateFormat(Helper.stringToDate(mFilteredList.get(position).getTglMasuk()));
            viewHolder.tv_category.setText(mFilteredList.get(position).getKategori());
            viewHolder.tv_tgl_masuk.setText(date);
            viewHolder.tv_comment.setText(""+mFilteredList.get(position).getComment());
            viewHolder.tv_seen.setText(""+mFilteredList.get(position).getViewer());
            Picasso.with(mContext)
                    .load(Helper.imgUrl(mFilteredList
                            .get(position)
                            .getThumbnail(), Constants.IMG_DIR_LAP_INFO))
                    .placeholder(R.drawable.no_image)
                    .resize(300,300)
                    .centerCrop()
                    .into(viewHolder.iv_image, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    viewHolder.progressbar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
            viewHolder.mr_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailLapinfo.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ivb_seen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityListCommentLapinfo.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ivb_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityListCommentLapinfo.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Lapinfo> filteredList = new ArrayList<>();
                    for (Lapinfo androidVersion : mArrayList) {
                        if (androidVersion.getKategori().toLowerCase().contains(charString) || androidVersion.getTglMasuk().toLowerCase().contains(charString) || androidVersion.getWaktuMasuk().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Lapinfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
