package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Berita;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.victor.loading.rotate.RotateLoading;

import java.util.List;

public class HomeHumasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Berita> itemList;
    private Context mContext;

    public class ItemHolder extends RecyclerView.ViewHolder {
        public TextView tv_title;
        public ImageView iv_image;
        public RotateLoading progressbar;
        public ItemHolder(View view){
            super(view);
            tv_title = view.findViewById(R.id.tv_title);
            iv_image = view.findViewById(R.id.iv_image);
            progressbar = view.findViewById(R.id.progressbar);
        }
    }

    public HomeHumasAdapter(Context context, List<Berita> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_all_report, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {

            final Berita itemObject = itemList.get(position);
            if (!itemObject.isMore()) {
                ((ItemHolder) holder).tv_title.setText(itemObject.getJudul());

                Picasso.with(mContext)
                        .load(Helper.imgUrl(itemObject.getThumbnail(), Constants.IMG_DIR_BERITA)).placeholder(R.drawable.no_image)
                        .resize(300, 300)
                        .centerCrop()
                        .into(((ItemHolder) holder).iv_image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                ((ItemHolder) holder).progressbar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } else {

            }
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void swapData(List<Berita> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
