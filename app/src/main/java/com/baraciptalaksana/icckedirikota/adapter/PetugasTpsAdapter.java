package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Filter;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPetugasTps;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Tps;
import java.util.ArrayList;

public class PetugasTpsAdapter extends RecyclerView.Adapter<PetugasTpsAdapter.ViewHolder> implements Filterable {
    private ArrayList<Tps> mArrayList;
    private ArrayList<Tps> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cd_detail;
        TextView tv_distance, tv_name, tv_address;
        ImageButton ivb_direction;
        public ViewHolder(View view) {
            super(view);
            tv_distance = view.findViewById(R.id.tv_distance);
            tv_name = view.findViewById(R.id.tv_name);
            tv_address = view.findViewById(R.id.tv_address);
            cd_detail = view.findViewById(R.id.cd_detail);
            ivb_direction = view.findViewById(R.id.ivb_direction);
        }
    }

    public PetugasTpsAdapter(Context context, ArrayList<Tps> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public PetugasTpsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_petugas_tps, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PetugasTpsAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {
            viewHolder.tv_distance.setText(mFilteredList.get(position).getJarakM());
            viewHolder.tv_name.setText(mFilteredList.get(position).getTps());
            viewHolder.tv_address.setText("Kec. "+mFilteredList.get(position).getKecamatan()+", "+"Kel. "+mFilteredList.get(position).getKelurahan());

            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPetugasTps.class);
                    intent.putExtra("id", mFilteredList.get(position).getId());
                    intent.putExtra("tps", mFilteredList.get(position).getTps());
                    intent.putExtra("lat", mFilteredList.get(position).getLat());
                    intent.putExtra("lon", mFilteredList.get(position).getLon());
                    intent.putExtra("hasil", mFilteredList.get(position).getHasil());
                    intent.putExtra("nama", mFilteredList.get(position).getNama());
                    intent.putExtra("nrp", mFilteredList.get(position).getNrp());
                    intent.putExtra("foto", mFilteredList.get(position).getFoto());
                    intent.putExtra("lat_anggota", mFilteredList.get(position).getLatAnggota());
                    intent.putExtra("lon_anggota", mFilteredList.get(position).getLonAnggota());
                    intent.putExtra("alamat", mFilteredList.get(position).getAlamat());
                    intent.putExtra("kecamatan", mFilteredList.get(position).getKecamatan());
                    intent.putExtra("kelurahan", mFilteredList.get(position).getKelurahan());
                    intent.putExtra("penanggung_jawab", mFilteredList.get(position).getPenanggungJawab());
                    intent.putExtra("tlp_penanggung_jawab", mFilteredList.get(position).getTlpPenanggungJawab());
                    intent.putExtra("jarak_m", mFilteredList.get(position).getJarakM());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ivb_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("geo:"+mFilteredList.get(position).getLat()+","+mFilteredList.get(position).getLon()));
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Tps> filteredList = new ArrayList<>();
                    for (Tps tps : mArrayList) {
                        if (tps.getTps().toLowerCase().contains(charString) || tps.getAlamat().toLowerCase().contains(charString)) {
                            filteredList.add(tps);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Tps>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
