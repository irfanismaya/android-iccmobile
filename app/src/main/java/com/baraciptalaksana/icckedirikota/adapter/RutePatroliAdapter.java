package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPosGatur;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailRutePatroli;
import com.baraciptalaksana.icckedirikota.activity.ActivityLaporanPatroliDialogis;
import com.alexzh.circleimageview.CircleImageView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.RutePatroli;
import java.util.ArrayList;

public class RutePatroliAdapter extends RecyclerView.Adapter<RutePatroliAdapter.ViewHolder> implements Filterable {
    private ArrayList<RutePatroli> mArrayList;
    private ArrayList<RutePatroli> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cd_detail;
        ImageButton ivb_direction, ivb_report;
        CircleImageView iv_icon;
        TextView tv_name, tv_category, tv_address, tv_officer;
        public ViewHolder(View view) {
            super(view);
            cd_detail = view.findViewById(R.id.cd_detail);
            tv_name = view.findViewById(R.id.tv_name);
            tv_address = view.findViewById(R.id.tv_address);
            tv_category = view.findViewById(R.id.tv_category);
            tv_category = view.findViewById(R.id.tv_category);
            tv_officer = view.findViewById(R.id.tv_officer);
            ivb_direction = view.findViewById(R.id.ivb_direction);
            ivb_report = view.findViewById(R.id.ivb_report);
            iv_icon = view.findViewById(R.id.iv_icon);
        }
    }

    public RutePatroliAdapter(Context context, ArrayList<RutePatroli> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public RutePatroliAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_rute_patroli, viewGroup, false);
        return new RutePatroliAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RutePatroliAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof RutePatroliAdapter.ViewHolder) {
            viewHolder.tv_name.setText(mFilteredList.get(position).getNama());
            viewHolder.tv_category.setText(mFilteredList.get(position).getKategory());
            viewHolder.tv_address.setText(""+mFilteredList.get(position).getAlamat());
            viewHolder.tv_officer.setText(mFilteredList.get(position).getNamaPenanggungjawab());
            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailRutePatroli.class);
                    intent.putExtra("Officer", mFilteredList.get(position).getNamaPenanggungjawab());
                    intent.putExtra("Telp", mFilteredList.get(position).getTlpPenanggungjawab());
                    intent.putExtra("Name", mFilteredList.get(position).getNama());
                    intent.putExtra("Address", mFilteredList.get(position).getAlamat());
                    intent.putExtra("Category", mFilteredList.get(position).getKategory());
                    intent.putExtra("Lat", mFilteredList.get(position).getLat());
                    intent.putExtra("Lon", mFilteredList.get(position).getLon());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<RutePatroli> filteredList = new ArrayList<>();
                    for (RutePatroli rutepatroli : mArrayList) {
                        if (rutepatroli.getNama().toLowerCase().contains(charString) || rutepatroli.getAlamat().toLowerCase().contains(charString) || rutepatroli.getKategory().toLowerCase().contains(charString)) {
                            filteredList.add(rutepatroli);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<RutePatroli>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
