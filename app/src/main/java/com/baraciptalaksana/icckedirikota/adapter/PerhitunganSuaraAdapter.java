package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPerhitunganSuara;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Suara;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PerhitunganSuaraAdapter extends RecyclerView.Adapter<PerhitunganSuaraAdapter.ViewHolder> implements Filterable {
    private ArrayList<Suara> mArrayList;
    private ArrayList<Suara> mFilteredList;
    private Context mContext;
    private String pemiluId;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cd_detail;
        LinearLayout ll_linear;
        ImageView image;
        TextView tv_tps, tv_date, tv_persentase, tv_address;
        public ViewHolder(View view) {
            super(view);
            cd_detail = view.findViewById(R.id.cd_detail);
            ll_linear = view.findViewById(R.id.ll_linear);
            image = view.findViewById(R.id.image);
            tv_tps = view.findViewById(R.id.tv_tps);
            tv_address = view.findViewById(R.id.tv_address);
            tv_date = view.findViewById(R.id.tv_date);
            tv_persentase = view.findViewById(R.id.tv_persentase);
        }
    }

    public PerhitunganSuaraAdapter(Context context, ArrayList<Suara> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    public String getPemiluId() {
        return pemiluId;
    }

    public void setPemiluId(String pemiluId) {
        this.pemiluId = pemiluId;
    }

    @Override
    public PerhitunganSuaraAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_perhitungan_suara, viewGroup, false);
        return new PerhitunganSuaraAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PerhitunganSuaraAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof PerhitunganSuaraAdapter.ViewHolder) {
            viewHolder.tv_tps.setText(mFilteredList.get(position).getTps());
            viewHolder.tv_address.setText(mFilteredList.get(position).getKecamatan()+", "+mFilteredList.get(position).getKelurahan());

            if (!Helper.isEmptyString(mFilteredList.get(position).getCreatedAt())) {
                viewHolder.tv_date.setText(""+mFilteredList.get(position).getCreatedAt());
            }

            if (mFilteredList.get(position).getNomorUrut() == null) {
                Picasso.with(mContext).load(R.drawable.ic_vote_pemilu).into(viewHolder.image);
            } else if (mFilteredList.get(position).getNomorUrut() == 0) {
                Picasso.with(mContext).load(R.drawable.ic_circle_pemilu).into(viewHolder.image);
                viewHolder.tv_persentase.setText(mFilteredList.get(position).getMenang());
                viewHolder.image.setColorFilter(mContext.getResources().getColor(R.color.color_problem_solving));
            } else {
                Picasso.with(mContext).load(R.drawable.ic_circle_pemilu).into(viewHolder.image);
                int i = mFilteredList.get(position).getNomorUrut() % Constants.PEMILU_COLORS.length;
                viewHolder.tv_persentase.setText(mFilteredList.get(position).getMenang());
                viewHolder.image.setColorFilter(mContext.getResources().getColor(Constants.PEMILU_COLORS[i]));
            }

            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPerhitunganSuara.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    intent.putExtra("Tps", mFilteredList.get(position).getTps());
                    intent.putExtra("alamat1", mFilteredList.get(position).getAlamat());
                    intent.putExtra("alamat2", "Kec. " + mFilteredList.get(position).getKecamatan()
                        + ", Kel. " + mFilteredList.get(position).getKelurahan() );
                    intent.putExtra("tps_id", ""+mFilteredList.get(position).getId());
                    intent.putExtra("pemilu", pemiluId);
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ll_linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPerhitunganSuara.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    intent.putExtra("Tps", mFilteredList.get(position).getTps());
                    intent.putExtra("alamat1", mFilteredList.get(position).getAlamat());
                    intent.putExtra("alamat2", "Kec. " + mFilteredList.get(position).getKecamatan()
                            + ", Kel. " + mFilteredList.get(position).getKelurahan() );
                    intent.putExtra("tps_id", ""+mFilteredList.get(position).getId());
                    intent.putExtra("pemilu", pemiluId);
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Suara> filteredList = new ArrayList<>();
                    for (Suara androidVersion : mArrayList) {
                        if (androidVersion.getTps().toLowerCase().contains(charString) || androidVersion.getAlamat().toLowerCase().contains(charString) || androidVersion.getMenang().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Suara>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
