package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailSchedule;
import com.balysv.materialripple.MaterialRippleLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Schedule;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> implements Filterable {
    private ArrayList<Schedule> mArrayList;
    private ArrayList<Schedule> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_description,tv_time;
        private CardView cd_detail;
        private MaterialRippleLayout mr_detail;
        private CircularImageView imageView;
        public ViewHolder(View view) {
            super(view);
            imageView = (CircularImageView) view.findViewById(R.id.image);
            tv_time = (TextView)view.findViewById(R.id.tv_time);
            tv_description = (TextView)view.findViewById(R.id.tv_description);
            cd_detail = (CardView)view.findViewById(R.id.cd_detail);
            mr_detail = (MaterialRippleLayout)view.findViewById(R.id.mr_detail);
        }
    }

    public ScheduleAdapter(Context context, ArrayList<Schedule> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public ScheduleAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_schedule, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleAdapter.ViewHolder viewHolder, final int i) {
        if (viewHolder instanceof ViewHolder) {
            int color = R.color.color_jadwal_kegiatan_back3;
            int icon = R.drawable.ic_jadwal_1;

            if (mFilteredList.get(i).getJenis().equalsIgnoreCase("pertemuan")) {
                color = R.color.color_jadwal_kegiatan_back1;
                icon = R.drawable.ic_jadwal_1;
            }

            if (mFilteredList.get(i).getJenis().equalsIgnoreCase("kegiatan")) {
                color = R.color.color_jadwal_kegiatan_back2;
                icon = R.drawable.ic_jadwal_2;
            }

            viewHolder.cd_detail.setCardBackgroundColor(mContext.getResources().getColor(color));

            Picasso.with(mContext).load(icon).placeholder(icon).into(viewHolder.imageView);

            viewHolder.tv_time.setText(mFilteredList.get(i).getWaktu());
            viewHolder.tv_description.setText(mFilteredList.get(i).getKegiatan());

            ((ViewHolder) viewHolder).cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailSchedule.class);
                    intent.putExtra("id", ""+mFilteredList.get(i).getId());
                    ((AppCompatActivity) mContext).startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
            ((ViewHolder) viewHolder).mr_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailSchedule.class);
                    intent.putExtra("id", ""+mFilteredList.get(i).getId());
                    ((AppCompatActivity) mContext).startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Schedule> filteredList = new ArrayList<>();
                    for (Schedule schedule : mArrayList) {
                        if (schedule.getWaktu().toLowerCase().contains(charString) || schedule.getKegiatan().toLowerCase().contains(charString)) {
                            filteredList.add(schedule);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Schedule>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
