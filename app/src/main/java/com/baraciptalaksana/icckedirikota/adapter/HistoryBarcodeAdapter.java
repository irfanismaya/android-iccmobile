package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailBarcode;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;
import java.util.ArrayList;

public class HistoryBarcodeAdapter extends RecyclerView.Adapter<HistoryBarcodeAdapter.ViewHolder> implements Filterable {
    private ArrayList<Mainmenu> mArrayList;
    private ArrayList<Mainmenu> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_date;
        private CardView cd_detail;
        public ViewHolder(View view) {
            super(view);
            tv_date = (TextView)view.findViewById(R.id.tv_date);
            cd_detail = (CardView)view.findViewById(R.id.cd_detail);
        }
    }

    public HistoryBarcodeAdapter(Context context, ArrayList<Mainmenu> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public HistoryBarcodeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_historycheckinbarcode, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryBarcodeAdapter.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            viewHolder.tv_date.setText(mFilteredList.get(i).getNama());
            ((ViewHolder) viewHolder).cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailBarcode.class);
                    ((AppCompatActivity) mContext).startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Mainmenu> filteredList = new ArrayList<>();
                    for (Mainmenu androidVersion : mArrayList) {
                        if (androidVersion.getNama().toLowerCase().contains(charString) || androidVersion.getNama().toLowerCase().contains(charString) || androidVersion.getNama().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Mainmenu>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
