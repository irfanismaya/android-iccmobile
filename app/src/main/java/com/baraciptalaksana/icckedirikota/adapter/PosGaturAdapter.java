package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPosGatur;
import com.baraciptalaksana.icckedirikota.activity.ActivityPosgaturCheckinBarcode;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.PostGatur;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;

import java.util.ArrayList;

public class PosGaturAdapter extends RecyclerView.Adapter<PosGaturAdapter.ViewHolder> implements Filterable {
    private ArrayList<PostGatur> mArrayList;
    private ArrayList<PostGatur> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cd_detail;
        ImageButton ivb_barcode, ivb_direction, ivb_info;
        TextView tv_distance, tv_name, tv_address;
        LinearLayout ll_background;
        public ViewHolder(View view) {
            super(view);
            cd_detail = view.findViewById(R.id.cd_detail);
            tv_distance = view.findViewById(R.id.tv_distance);
            tv_name = view.findViewById(R.id.tv_name);
            tv_address = view.findViewById(R.id.tv_address);
            ivb_barcode = view.findViewById(R.id.ivb_barcode);
            ivb_direction = view.findViewById(R.id.ivb_direction);
            ivb_info = view.findViewById(R.id.ivb_info);
            ll_background = view.findViewById(R.id.ll_background);
        }
    }

    public PosGaturAdapter(Context context, ArrayList<PostGatur> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public PosGaturAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_pos_gatur, viewGroup, false);
        return new PosGaturAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PosGaturAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof PosGaturAdapter.ViewHolder) {

            if (position % 2 == 1) {
                viewHolder.ll_background.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                viewHolder.ll_background.setBackgroundColor(Color.parseColor("#e6e6e6"));
            }

            viewHolder.tv_distance.setText(mFilteredList.get(position).getJarakM());
            viewHolder.tv_name.setText(mFilteredList.get(position).getNama());
            viewHolder.tv_address.setText(""+mFilteredList.get(position).getAlamat());

            viewHolder.cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPosGatur.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    intent.putExtra("lat", mFilteredList.get(position).getLat());
                    intent.putExtra("lon", mFilteredList.get(position).getLon());
                    intent.putExtra("alamat", mFilteredList.get(position).getAlamat());
                    intent.putExtra("pimpinan", mFilteredList.get(position).getPimpinan());
                    intent.putExtra("tlp", mFilteredList.get(position).getTlp());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });

            viewHolder.ivb_barcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            viewHolder.ivb_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("geo:"+mFilteredList.get(position).getLat()+","+mFilteredList.get(position).getLon()));
                    mContext.startActivity(intent);
                }
            });

            viewHolder.ivb_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityPosgaturCheckinBarcode.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<PostGatur> filteredList = new ArrayList<>();
                    for (PostGatur postGatur : mArrayList) {
                        if (postGatur.getJarakM().toLowerCase().contains(charString) || postGatur.getNama().toLowerCase().contains(charString) || postGatur.getAlamat().toLowerCase().contains(charString)) {
                            filteredList.add(postGatur);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<PostGatur>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
