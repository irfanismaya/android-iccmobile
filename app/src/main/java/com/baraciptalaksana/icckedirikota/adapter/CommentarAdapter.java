package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Comment;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentarAdapter extends RecyclerView.Adapter<CommentarAdapter.ViewHolder> implements Filterable {
    private List<Comment> mArrayList;
    private List<Comment> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_date, tv_comment, tv_name, tv_jabatan;
        CircleImageView image;
        RotateLoading progressbar;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tv_date = view.findViewById(R.id.tv_date);
            tv_name = view.findViewById(R.id.tv_name);
            tv_jabatan = view.findViewById(R.id.tv_jabatan);
            tv_comment = view.findViewById(R.id.tv_comment);
            progressbar = view.findViewById(R.id.progressbar);
        }
    }

    public CommentarAdapter(Context context, List<Comment> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public CommentarAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_commentar, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CommentarAdapter.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            viewHolder.tv_name.setText(mFilteredList.get(i).getNama());
            viewHolder.tv_jabatan.setText(mFilteredList.get(i).getJabatan());
            viewHolder.tv_comment.setText(mFilteredList.get(i).getKomentar());
            viewHolder.tv_date.setText(mFilteredList.get(i).getCreatedAt());

            String url = Helper.imgUrl(mFilteredList
                    .get(i)
                    .getThumbnail(), Constants.IMG_DIR_ANGGOTA);

            Picasso.with(mContext)
                    .load(url)
                    .placeholder(R.drawable.no_image)
                    .resize(300,300)
                    .centerCrop()
                    .into(viewHolder.image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            viewHolder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Comment> filteredList = new ArrayList<>();
                    for (Comment androidVersion : mArrayList) {
                        if (androidVersion.getKomentar().toLowerCase().contains(charString) || androidVersion.getCreatedAt().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Comment>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
