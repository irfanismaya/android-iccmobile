package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Inbox;

import java.util.List;

public class InboxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Inbox> itemList;
    private Context mContext;

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView iv_image;
        TextView tv_time, tv_message;
        LinearLayout ll_background;
        public ItemHolder(View view){
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_time = view.findViewById(R.id.tv_time);
            tv_message = view.findViewById(R.id.tv_message);
            ll_background = view.findViewById(R.id.ll_background);
        }
    }

    public InboxAdapter(Context context, List<Inbox> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_inbox, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            final Inbox itemObject = itemList.get(position);
            if (position % 2 == 1) {
                ((ItemHolder) holder).ll_background.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                ((ItemHolder) holder).ll_background.setBackgroundColor(Color.parseColor("#e6e6e6"));
            }
            ((ItemHolder) holder).tv_time.setText(itemObject.getTime());
            ((ItemHolder) holder).tv_message.setText(itemObject.getMessage());
            switch (itemObject.getType()) {
                case "Peringatan":
                    ((ItemHolder) holder).iv_image.setImageResource(R.drawable._inbox_peringatan);
                    break;
                case "Pengumuman":
                    ((ItemHolder) holder).iv_image.setImageResource(R.drawable._inbox_pengumaman);
                    break;
                case "Private":
                    ((ItemHolder) holder).iv_image.setImageResource(R.drawable.photo_female_1);
                    break;
                default:
                    ((ItemHolder) holder).iv_image.setImageResource(R.drawable.no_image);
            }

        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void swapData(List<Inbox> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
