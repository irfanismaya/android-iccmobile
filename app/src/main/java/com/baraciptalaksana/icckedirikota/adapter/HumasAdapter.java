package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.activity.ActivityDetailHumas;
import com.baraciptalaksana.icckedirikota.models.Berita;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;
import java.util.ArrayList;

public class HumasAdapter extends RecyclerView.Adapter<HumasAdapter.ViewHolder> implements Filterable {
    private ArrayList<Berita> mArrayList;
    private ArrayList<Berita> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_image;
        TextView tv_category, tv_tgl_masuk;
        MaterialRippleLayout mr_detail;
        RotateLoading progressbar;
        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.iv_image);
            tv_category = view.findViewById(R.id.tv_category);
            tv_tgl_masuk = view.findViewById(R.id.tv_tgl_massuk);
            mr_detail = view.findViewById(R.id.mr_detail);
            progressbar = view.findViewById(R.id.progressbar);
        }
    }

    public HumasAdapter(Context context, ArrayList<Berita> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public HumasAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_humas, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HumasAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {
            //String date = Helper.fullDateFormat(Helper.stringToDate(mFilteredList.get(position).getCreatedAt()));
            viewHolder.tv_category.setText(mFilteredList.get(position).getJudul());
            viewHolder.tv_tgl_masuk.setText(mFilteredList.get(position).getCreatedAt());
            Picasso.with(mContext)
                    .load(Helper.imgUrl(mFilteredList
                            .get(position)
                            .getThumbnail(), Constants.IMG_DIR_BERITA))
                    .placeholder(R.drawable.no_image)
                    .resize(300,300)
                    .centerCrop()
                    .into(viewHolder.iv_image, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    viewHolder.progressbar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
            viewHolder.mr_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailHumas.class);
                    intent.putExtra("Id", mFilteredList.get(position).getId());
                    mContext.startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Berita> filteredList = new ArrayList<>();
                    for (Berita berita : mArrayList) {
                        if (berita.getJudul().toLowerCase().contains(charString) || berita.getCreatedAt().toLowerCase().contains(charString) ) {
                            filteredList.add(berita);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Berita>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
