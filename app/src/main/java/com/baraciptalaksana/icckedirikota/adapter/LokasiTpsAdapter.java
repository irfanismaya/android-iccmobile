package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailPetugasTps;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;

import java.util.List;

public class LokasiTpsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Mainmenu> itemList;
    private Context mContext;

    public class ItemHolder extends RecyclerView.ViewHolder {
        CardView cd_detail;
        public ItemHolder(View view){
            super(view);
            cd_detail = (CardView) view.findViewById(R.id.cd_detail);
        }
    }

    public LokasiTpsAdapter(Context context, List<Mainmenu> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_lokasi_tps, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            final Mainmenu itemObject = itemList.get(position);
            ((ItemHolder) holder).cd_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityDetailPetugasTps.class);
                    ((AppCompatActivity) mContext).startActivity(intent);
                    ((AppCompatActivity) mContext).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void swapData(List<Mainmenu> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
