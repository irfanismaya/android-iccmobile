package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.activity.ActivityDetailBarcode;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.SatuanWilayah;
import com.baraciptalaksana.icckedirikota.utils.Helper;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class SatuanWilayahAdapter extends RecyclerView.Adapter<SatuanWilayahAdapter.ViewHolder> implements Filterable {
    private ArrayList<SatuanWilayah> mArrayList;
    private ArrayList<SatuanWilayah> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name,tv_pimpinan, tv_address, tv_distance, tv_telp;
        private ImageButton ib_telp, ib_address;
        private LinearLayout ll_color;
        CardView cd_detail;
        public ViewHolder(View view) {
            super(view);
            cd_detail = view.findViewById(R.id.cd_detail);
            tv_name = view.findViewById(R.id.tv_name);
            tv_telp = view.findViewById(R.id.tv_telp);
            tv_distance = view.findViewById(R.id.tv_distance);
            tv_pimpinan = view.findViewById(R.id.tv_pimpinan);
            tv_address = view.findViewById(R.id.tv_address);
            ib_telp = view.findViewById(R.id.ib_telp);
            ll_color = view.findViewById(R.id.ll_color);
            ib_address = view.findViewById(R.id.ib_address);
        }
    }

    public SatuanWilayahAdapter(Context context, ArrayList<SatuanWilayah> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public SatuanWilayahAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_satuan_wilayah, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SatuanWilayahAdapter.ViewHolder viewHolder, final int i) {
        if (viewHolder instanceof ViewHolder) {

            if (i % 2 == 1) {
                viewHolder.cd_detail.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                viewHolder.cd_detail.setBackgroundColor(Color.parseColor("#e6e6e6"));
            }

            if(Helper.isEmptyString(mFilteredList.get(i).getNama()) || mFilteredList.get(i).getNama()==null){
                viewHolder.tv_name.setText("-");
            }else{
                viewHolder.tv_name.setText(mFilteredList.get(i).getNama());
            }

            if(Helper.isEmptyString(mFilteredList.get(i).getJarak()) || mFilteredList.get(i).getJarak()==null){
                viewHolder.tv_distance.setText("-");
            }else{
                viewHolder.tv_distance.setText(mFilteredList.get(i).getJarak());
            }

            if(Helper.isEmptyString(mFilteredList.get(i).getPimpinan()) || mFilteredList.get(i).getPimpinan()==null){
                viewHolder.tv_pimpinan.setText("-");
            }else{
                viewHolder.tv_pimpinan.setText(mFilteredList.get(i).getPimpinan());
            }

            if(Helper.isEmptyString(mFilteredList.get(i).getAlamat()) || mFilteredList.get(i).getAlamat()==null){
                viewHolder.tv_address.setText("-");
            }else{
                viewHolder.tv_address.setText(mFilteredList.get(i).getAlamat());
            }

            if(Helper.isEmptyString(mFilteredList.get(i).getTlp()) || mFilteredList.get(i).getTlp()==null){
                viewHolder.tv_telp.setText("-");
            }else{
                viewHolder.tv_telp.setText(mFilteredList.get(i).getTlp());
            }

            viewHolder.ib_telp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!Helper.isEmptyString(mFilteredList.get(i).getTlp().toString()) || mFilteredList.get(i).getTlp() != null){
                        showConfirmDialogCall(mFilteredList.get(i).getTlp().toString(), mFilteredList.get(i).getNama()+"?");
                    }else{
                        Toasty.warning(mContext, "No telp belum tersedia", Toast.LENGTH_LONG, true).show();
                    }
                }
            });

            viewHolder.ib_address.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Helper.isEmptyString(mFilteredList.get(i).getLat()) || !Helper.isEmptyString(mFilteredList.get(i).getLon()) || mFilteredList.get(i).getLat()!=null ||mFilteredList.get(i).getLon()!=null) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("geo:"+mFilteredList.get(i).getLat().toString()+","+mFilteredList.get(i).getLon().toString()));
                        mContext.startActivity(intent);
                    }else{
                        Toasty.warning(mContext, "Alamat belum tersedia", Toast.LENGTH_LONG, true).show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<SatuanWilayah> filteredList = new ArrayList<>();
                    for (SatuanWilayah satuanWilayah : mArrayList) {
                        if (satuanWilayah.getNama().toLowerCase().contains(charString) || satuanWilayah.getTlp().toLowerCase().contains(charString) || satuanWilayah.getPimpinan().toLowerCase().contains(charString)) {
                            filteredList.add(satuanWilayah);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<SatuanWilayah>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private void showConfirmDialogCall(final String telp, String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("iCC Mobile");
        builder.setMessage("Apakah anda akan menelpon "+name);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intentDial = new Intent(Intent.ACTION_DIAL,
                        Uri.parse("tel:"+telp));
                mContext.startActivity(intentDial);
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }
}
