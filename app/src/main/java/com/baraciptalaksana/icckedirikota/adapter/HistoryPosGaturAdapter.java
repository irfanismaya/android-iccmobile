package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.HistoryPosGatur;
import java.util.ArrayList;

public class HistoryPosGaturAdapter extends RecyclerView.Adapter<HistoryPosGaturAdapter.ViewHolder> implements Filterable {
    private ArrayList<HistoryPosGatur> mArrayList;
    private ArrayList<HistoryPosGatur> mFilteredList;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_address, tv_durasi, tv_date;
        public ViewHolder(View view) {
            super(view);
            tv_address = view.findViewById(R.id.tv_address);
            tv_durasi = view.findViewById(R.id.tv_durasi);
            tv_date = view.findViewById(R.id.tv_date);
        }
    }

    public HistoryPosGaturAdapter(Context context, ArrayList<HistoryPosGatur> arrayList) {
        this.mContext = context;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public HistoryPosGaturAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_history_pos_gatur, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryPosGaturAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ViewHolder) {
            viewHolder.tv_date.setText(mFilteredList.get(position).getCreatedAt());
            viewHolder.tv_address.setText(mFilteredList.get(position).getAlamat());
            viewHolder.tv_durasi.setText(mFilteredList.get(position).getDurasi());
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<HistoryPosGatur> filteredList = new ArrayList<>();
                    for (HistoryPosGatur historyPosGatur : mArrayList) {
                        if (historyPosGatur.getCreatedAt().toLowerCase().contains(charString) || historyPosGatur.getAlamat().toLowerCase().contains(charString) || historyPosGatur.getDurasi().toLowerCase().contains(charString)) {
                            filteredList.add(historyPosGatur);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<HistoryPosGatur>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
