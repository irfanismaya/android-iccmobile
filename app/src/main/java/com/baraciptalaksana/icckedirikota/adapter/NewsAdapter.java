package com.baraciptalaksana.icckedirikota.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Lapinfo;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.victor.loading.rotate.RotateLoading;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Lapinfo> itemList;
    private Context mContext;

    public class ItemHolder extends RecyclerView.ViewHolder {
        public TextView tv_adapter_mainmenu_title;
        public ImageView iv_adapter_mainmenu_image;
        public RotateLoading progressbar;
        public ItemHolder(View view){
            super(view);
            tv_adapter_mainmenu_title = (TextView)view.findViewById(R.id.tv_adapter_mainmenu_title);
            iv_adapter_mainmenu_image = (ImageView) view.findViewById(R.id.iv_adapter_mainmenu_image);
            progressbar = (RotateLoading) view.findViewById(R.id.progressbar);
        }
    }

    public NewsAdapter(Context context, List<Lapinfo> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_news, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            final Lapinfo itemObject = itemList.get(position);
            if (itemObject != null) {
                ((ItemHolder) holder).tv_adapter_mainmenu_title.setText(itemObject.getKategori());
                ((ItemHolder) holder).iv_adapter_mainmenu_image.setImageResource(R.drawable.no_image);

                Picasso.with(mContext)
                        .load(Constants.URL_IMAGE + itemObject
                                .getFoto()).placeholder(R.drawable.no_image)
                        .resize(300, 300)
                        .centerCrop()
                        .into(((ItemHolder) holder).iv_adapter_mainmenu_image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                ((ItemHolder) holder).progressbar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void swapData(List<Lapinfo> itemList) {
        this.itemList.clear();
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
