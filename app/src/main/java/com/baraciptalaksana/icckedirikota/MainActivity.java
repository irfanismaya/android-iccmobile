package com.baraciptalaksana.icckedirikota;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BaselineLayout;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.fragment.CommandFragment;
import com.baraciptalaksana.icckedirikota.fragment.HomeFragment;
import com.baraciptalaksana.icckedirikota.fragment.InboxFragment;
import com.baraciptalaksana.icckedirikota.fragment.PolresFragment;
import com.baraciptalaksana.icckedirikota.fragment.ProfileFragment;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import java.lang.reflect.Field;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST = 1;

    private BottomNavigationView mBottomNavigationView;
    private Context mContext = this;

    int versioncode = BuildConfig.VERSION_CODE;
    String versionname = BuildConfig.VERSION_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupBottomNavigation();
        if (savedInstanceState == null) {
            loadHomeFragment();
        }

        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            startTrackerService();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST);
        }

        getCheckVersion(SharedPreferences.getToken(mContext));
    }

    private void startTrackerService() {
        startService(new Intent(this, TrackerService.class));
    }

    private void setupBottomNavigation() {
        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        disableShiftMode(mBottomNavigationView);
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_home:
                    loadHomeFragment();
                    return true;
                case R.id.action_polres:
                    loadPolressFragment();
                    return true;
                case R.id.action_command:
                    loadCommandFragment();
                    return true;
                case R.id.action_inbox:
                    loadInboxFragment();
                    return true;
                case R.id.action_profile:
                    loadProfileFragment();
                    return true;
            }
            return false;
            }
        });
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                item.setChecked(item.getItemData().isChecked());
                View itemTitle = item.getChildAt(1);
                TextView smallTitle = (TextView) ((BaselineLayout) itemTitle).getChildAt(0);
                TextView largeTitle = (TextView) ((BaselineLayout) itemTitle).getChildAt(1);
                smallTitle.setTextSize(10f);
                largeTitle.setTextSize(11f);
            }
        }
        catch (NoSuchFieldException e){}
        catch (IllegalAccessException e){}
    }

    private void loadHomeFragment() {
        HomeFragment fragment = HomeFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadPolressFragment() {
        PolresFragment fragment = PolresFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadCommandFragment() {
        CommandFragment fragment = CommandFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadInboxFragment() {
        InboxFragment fragment = InboxFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadProfileFragment() {
        ProfileFragment fragment = ProfileFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startTrackerService();
        }
    }


    private void getCheckVersion(String token){
        ApiInterface.Factory.getInstance().checkversion("Bearer " + token).enqueue(new Callback<Response.ResponseCheckVersion>() {
            @Override
            public void onResponse(Call<Response.ResponseCheckVersion> call, retrofit2.Response<Response.ResponseCheckVersion> response) {
                if (response.isSuccessful()) {
                   if(response.body() != null && response.body().getData() != null && response.body().getData().getCode() > versioncode && !response.body().getData().getName().equalsIgnoreCase(versionname)){
                       confirmDialog();
                   }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseCheckVersion> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder
            .setTitle(R.string.dialog_update_title)
            .setMessage(R.string.dialog_update_message)
            .setPositiveButton("Perbaharui",  new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            }).show();
    }
}
