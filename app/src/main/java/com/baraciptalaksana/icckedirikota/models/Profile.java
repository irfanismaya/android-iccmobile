package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("nrp")
    @Expose
    private String nrp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("fungsi")
    @Expose
    private String fungsi;
    @SerializedName("pangkat")
    @Expose
    private String pangkat;
    @SerializedName("jabatan_id")
    @Expose
    private Integer jabatanId;
    @SerializedName("satuan_wilayah_kode")
    @Expose
    private String satuanWilayahKode;
    @SerializedName("tlp")
    @Expose
    private Object tlp;
    @SerializedName("no_wa")
    @Expose
    private Object noWa;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("perintah")
    @Expose
    private Integer perintah;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("lat")
    @Expose
    private Object lat;
    @SerializedName("lon")
    @Expose
    private Object lon;
    @SerializedName("acc")
    @Expose
    private Object acc;
    @SerializedName("speed")
    @Expose
    private Object speed;
    @SerializedName("bat_lvl")
    @Expose
    private Object batLvl;
    @SerializedName("ket")
    @Expose
    private Object ket;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public Integer getJabatanId() {
        return jabatanId;
    }

    public void setJabatanId(Integer jabatanId) {
        this.jabatanId = jabatanId;
    }

    public String getSatuanWilayahKode() {
        return satuanWilayahKode;
    }

    public void setSatuanWilayahKode(String satuanWilayahKode) {
        this.satuanWilayahKode = satuanWilayahKode;
    }

    public Object getTlp() {
        return tlp;
    }

    public void setTlp(Object tlp) {
        this.tlp = tlp;
    }

    public Object getNoWa() {
        return noWa;
    }

    public void setNoWa(Object noWa) {
        this.noWa = noWa;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Integer getPerintah() {
        return perintah;
    }

    public void setPerintah(Integer perintah) {
        this.perintah = perintah;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLon() {
        return lon;
    }

    public void setLon(Object lon) {
        this.lon = lon;
    }

    public Object getAcc() {
        return acc;
    }

    public void setAcc(Object acc) {
        this.acc = acc;
    }

    public Object getSpeed() {
        return speed;
    }

    public void setSpeed(Object speed) {
        this.speed = speed;
    }

    public Object getBatLvl() {
        return batLvl;
    }

    public void setBatLvl(Object batLvl) {
        this.batLvl = batLvl;
    }

    public Object getKet() {
        return ket;
    }

    public void setKet(Object ket) {
        this.ket = ket;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
