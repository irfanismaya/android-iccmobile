package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fungsi {

    @SerializedName("fungsi")
    @Expose
    private String fungsi;

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

}