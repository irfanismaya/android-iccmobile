package com.baraciptalaksana.icckedirikota.models;

public class Inbox {
    String time, message, type;
    int image;

    public Inbox(String time, String message, String type, int image) {
        this.time = time;
        this.message = message;
        this.type = type;
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
