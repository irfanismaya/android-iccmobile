package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request {
    public class UserRequestLogin {
        @SerializedName("nrp")
        @Expose
        private String nrp;
        @SerializedName("password")
        @Expose
        private String password;

        public String getNrp() {
            return nrp;
        }

        public void setNrp(String nrp) {
            this.nrp = nrp;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public class PatroliBarcodeRequestCheckin {

        @SerializedName("nrp")
        @Expose
        private String nrp;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lon")
        @Expose
        private String lon;
        @SerializedName("kode")
        @Expose
        private String kode;

        public String getNrp() {
            return nrp;
        }

        public void setNrp(String nrp) {
            this.nrp = nrp;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

    }

    public class PatroliBarcodeRequestCheckout {

        @SerializedName("nrp")
        @Expose
        private String nrp;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lon")
        @Expose
        private String lon;
        @SerializedName("kode")
        @Expose
        private String kode;

        public String getNrp() {
            return nrp;
        }

        public void setNrp(String nrp) {
            this.nrp = nrp;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

    }

    public class UserRequestForgotPassword {
        @SerializedName("email")
        @Expose
        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public class CommentRequestLaporan{
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("komentar")
        @Expose
        private String komentar;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getKomentar() {
            return komentar;
        }

        public void setKomentar(String komentar) {
            this.komentar = komentar;
        }

    }

    public class PenugasanRequest {

        @SerializedName("nrp")
        @Expose
        private String nrp;
        @SerializedName("nrp_personil")
        @Expose
        private String nrpPersonil;
        @SerializedName("satuan")
        @Expose
        private String satuan;
        @SerializedName("uraian")
        @Expose
        private String uraian;

        @SerializedName("keterangan")
        @Expose
        private String keterangan;

        public String getNrp() {
            return nrp;
        }

        public void setNrp(String nrp) {
            this.nrp = nrp;
        }

        public String getNrpPersonil() {
            return nrpPersonil;
        }

        public void setNrpPersonil(String nrpPersonil) {
            this.nrpPersonil = nrpPersonil;
        }

        public String getSatuan() {
            return satuan;
        }

        public void setSatuan(String satuan) {
            this.satuan = satuan;
        }

        public String getUraian() {
            return uraian;
        }

        public void setUraian(String uraian) {
            this.uraian = uraian;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

    }

    public class JadwalRequest {

        @SerializedName("nrp")
        @Expose
        private String nrp;
        @SerializedName("kegiatan")
        @Expose
        private String kegiatan;
        @SerializedName("waktu_start")
        @Expose
        private String waktuStart;
        @SerializedName("waktu_end")
        @Expose
        private String waktuEnd;
        @SerializedName("lokasi")
        @Expose
        private String lokasi;
        @SerializedName("peserta")
        @Expose
        private String peserta;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("jenis")
        @Expose
        private String jenis;

        public String getNrp() {
            return nrp;
        }

        public void setNrp(String nrp) {
            this.nrp = nrp;
        }

        public String getKegiatan() {
            return kegiatan;
        }

        public void setKegiatan(String kegiatan) {
            this.kegiatan = kegiatan;
        }

        public String getWaktuStart() {
            return waktuStart;
        }

        public void setWaktuStart(String waktuStart) {
            this.waktuStart = waktuStart;
        }

        public String getWaktuEnd() {
            return waktuEnd;
        }

        public void setWaktuEnd(String waktuEnd) {
            this.waktuEnd = waktuEnd;
        }

        public String getLokasi() {
            return lokasi;
        }

        public void setLokasi(String lokasi) {
            this.lokasi = lokasi;
        }

        public String getPeserta() {
            return peserta;
        }

        public void setPeserta(String peserta) {
            this.peserta = peserta;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getJenis() {
            return jenis;
        }

        public void setJenis(String jenis) {
            this.jenis = jenis;
        }

    }

    public class EditPetugasTpsRequest{
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("penanggung_jawab")
        @Expose
        private String penanggungJawab;
        @SerializedName("tlp_penanggung_jawab")
        @Expose
        private String tlpPenanggungJawab;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lon")
        @Expose
        private String lon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getPenanggungJawab() {
            return penanggungJawab;
        }

        public void setPenanggungJawab(String penanggungJawab) {
            this.penanggungJawab = penanggungJawab;
        }

        public String getTlpPenanggungJawab() {
            return tlpPenanggungJawab;
        }

        public void setTlpPenanggungJawab(String tlpPenanggungJawab) {
            this.tlpPenanggungJawab = tlpPenanggungJawab;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

    }

    public class HasilPemiluRequest {

        @SerializedName("tps")
        @Expose
        private String tps;
        @SerializedName("peserta")
        @Expose
        private String peserta;
        @SerializedName("jumlah")
        @Expose
        private String jumlah;

        public String getTps() {
            return tps;
        }

        public void setTps(String tps) {
            this.tps = tps;
        }

        public String getPeserta() {
            return peserta;
        }

        public void setPeserta(String peserta) {
            this.peserta = peserta;
        }

        public String getJumlah() {
            return jumlah;
        }

        public void setJumlah(String jumlah) {
            this.jumlah = jumlah;
        }
    }

    public class JadwalStatusRequest {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("disposisi")
        @Expose
        private Integer disposisi;

        public JadwalStatusRequest() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getDisposisi() {
            return disposisi;
        }

        public void setDisposisi(Integer disposisi) {
            this.disposisi = disposisi;
        }
    }
}
