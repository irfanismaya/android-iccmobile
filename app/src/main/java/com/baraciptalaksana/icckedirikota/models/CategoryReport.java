package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryReport {
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("uraian")
    @Expose
    private String uraian;

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }
}
