package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Personil {

    @SerializedName("nrp")
    @Expose
    private String nrp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("fungsi")
    @Expose
    private String fungsi;
    @SerializedName("pangkat")
    @Expose
    private String pangkat;
    @SerializedName("wilayah")
    @Expose
    private String wilayah;

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String toString(){
        return nama;
    }

}
