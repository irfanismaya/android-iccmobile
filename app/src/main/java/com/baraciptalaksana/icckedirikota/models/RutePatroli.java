package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RutePatroli {

    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("nama_penanggungjawab")
    @Expose
    private String namaPenanggungjawab;
    @SerializedName("tlp_penanggungjawab")
    @Expose
    private String tlpPenanggungjawab;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("kategory")
    @Expose
    private String kategory;
    @SerializedName("icon")
    @Expose
    private String icon;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNamaPenanggungjawab() {
        return namaPenanggungjawab;
    }

    public void setNamaPenanggungjawab(String namaPenanggungjawab) {
        this.namaPenanggungjawab = namaPenanggungjawab;
    }

    public String getTlpPenanggungjawab() {
        return tlpPenanggungjawab;
    }

    public void setTlpPenanggungjawab(String tlpPenanggungjawab) {
        this.tlpPenanggungjawab = tlpPenanggungjawab;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getKategory() {
        return kategory;
    }

    public void setKategory(String kategory) {
        this.kategory = kategory;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}