package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pemilu {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pemilu")
    @Expose
    private String pemilu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPemilu() {
        return pemilu;
    }

    public void setPemilu(String pemilu) {
        this.pemilu = pemilu;
    }

}
