package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailSuara {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tps")
    @Expose
    private String tps;
    @SerializedName("peserta")
    @Expose
    private String peserta;
    @SerializedName("peserta_id")
    @Expose
    private String pesertaId;
    @SerializedName("suara")
    @Expose
    private Integer suara;
    @SerializedName("persentase")
    @Expose
    private String persentase;
    @SerializedName("nomor_urut")
    @Expose
    private Integer nomorUrut;
    @SerializedName("pemilu")
    @Expose
    private String pemilu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTps() {
        return tps;
    }

    public void setTps(String tps) {
        this.tps = tps;
    }

    public String getPeserta() {
        return peserta;
    }

    public void setPeserta(String peserta) {
        this.peserta = peserta;
    }

    public Integer getSuara() {
        return suara;
    }

    public void setSuara(Integer suara) {
        this.suara = suara;
    }

    public String getPersentase() {
        return persentase;
    }

    public void setPersentase(String persentase) {
        this.persentase = persentase;
    }

    public Integer getNomorUrut() {
        return nomorUrut;
    }

    public void setNomorUrut(Integer nomorUrut) {
        this.nomorUrut = nomorUrut;
    }

    public String getPesertaId() {
        return pesertaId;
    }

    public void setPesertaId(String pesertaId) {
        this.pesertaId = pesertaId;
    }

    public String getPemilu() {
        return pemilu;
    }

    public void setPemilu(String pemilu) {
        this.pemilu = pemilu;
    }
}
