package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummarySuaraKapolsek {
    @SerializedName("peserta")
    @Expose
    private String peserta;
    @SerializedName("peserta_id")
    @Expose
    private Integer pesertaId;
    @SerializedName("nomor_urut")
    @Expose
    private Integer nomorUrut;
    @SerializedName("suara")
    @Expose
    private Integer suara;
    @SerializedName("persentase_suara")
    @Expose
    private String persentaseSuara;
    @SerializedName("tps")
    @Expose
    private Integer tps;
    @SerializedName("persentase_tps")
    @Expose
    private String persentaseTps;

    public String getPeserta() {
        return peserta;
    }

    public void setPeserta(String peserta) {
        this.peserta = peserta;
    }

    public Integer getPesertaId() {
        return pesertaId;
    }

    public void setPesertaId(Integer pesertaId) {
        this.pesertaId = pesertaId;
    }

    public Integer getNomorUrut() {
        return nomorUrut;
    }

    public void setNomorUrut(Integer nomorUrut) {
        this.nomorUrut = nomorUrut;
    }

    public Integer getSuara() {
        return suara;
    }

    public void setSuara(Integer suara) {
        this.suara = suara;
    }

    public String getPersentaseSuara() {
        return persentaseSuara;
    }

    public void setPersentaseSuara(String persentaseSuara) {
        this.persentaseSuara = persentaseSuara;
    }

    public Integer getTps() {
        return tps;
    }

    public void setTps(Integer tps) {
        this.tps = tps;
    }

    public String getPersentaseTps() {
        return persentaseTps;
    }

    public void setPersentaseTps(String persentaseTps) {
        this.persentaseTps = persentaseTps;
    }
}

