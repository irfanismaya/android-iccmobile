package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    public class LoginResponse {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("access_token")
        @Expose
        private String accessToken;
        @SerializedName("data")
        @Expose
        private Data data;
        @SerializedName("menu")
        @Expose
        private List<Menu> menu = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public List<Menu> getMenu() {
            return menu;
        }

        public void setMenu(List<Menu> menu) {
            this.menu = menu;
        }

        public class Data {

            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("nama")
            @Expose
            private String nama;
            @SerializedName("foto")
            @Expose
            private String foto;
            @SerializedName("thumbnail")
            @Expose
            private String thumbnail;
            @SerializedName("fungsi")
            @Expose
            private String fungsi;
            @SerializedName("pangkat")
            @Expose
            private String pangkat;
            @SerializedName("jabatan_id")
            @Expose
            private Integer jabatanId;
            @SerializedName("satuan_wilayah_kode")
            @Expose
            private String satuanWilayahKode;
            @SerializedName("tlp")
            @Expose
            private Object tlp;
            @SerializedName("no_wa")
            @Expose
            private Object noWa;
            @SerializedName("email")
            @Expose
            private Object email;
            @SerializedName("perintah")
            @Expose
            private Integer perintah;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("lat")
            @Expose
            private Object lat;
            @SerializedName("lon")
            @Expose
            private Object lon;
            @SerializedName("acc")
            @Expose
            private Object acc;
            @SerializedName("speed")
            @Expose
            private Object speed;
            @SerializedName("bat_lvl")
            @Expose
            private Object batLvl;
            @SerializedName("ket")
            @Expose
            private Object ket;
            @SerializedName("created_at")
            @Expose
            private Object createdAt;
            @SerializedName("updated_at")
            @Expose
            private Object updatedAt;

            @SerializedName("jabatan")
            @Expose
            private String jabatan;

            @SerializedName("jabatan_level")
            @Expose
            private String jabatanLevel;

            @SerializedName("satuan_wilayah")
            @Expose
            private String satuanWilayah;

            @SerializedName("marker_icon")
            @Expose
            private String markerIcon;

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getFungsi() {
                return fungsi;
            }

            public void setFungsi(String fungsi) {
                this.fungsi = fungsi;
            }

            public String getPangkat() {
                return pangkat;
            }

            public void setPangkat(String pangkat) {
                this.pangkat = pangkat;
            }

            public Integer getJabatanId() {
                return jabatanId;
            }

            public void setJabatanId(Integer jabatanId) {
                this.jabatanId = jabatanId;
            }

            public String getSatuanWilayahKode() {
                return satuanWilayahKode;
            }

            public void setSatuanWilayahKode(String satuanWilayahKode) {
                this.satuanWilayahKode = satuanWilayahKode;
            }

            public String getMarkerIcon() {
                return markerIcon;
            }

            public void setMarkerIcon(String markerIcon) {
                this.markerIcon = markerIcon;
            }

            public Object getTlp() {
                return tlp;
            }

            public void setTlp(Object tlp) {
                this.tlp = tlp;
            }

            public Object getNoWa() {
                return noWa;
            }

            public void setNoWa(Object noWa) {
                this.noWa = noWa;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public Integer getPerintah() {
                return perintah;
            }

            public void setPerintah(Integer perintah) {
                this.perintah = perintah;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public Object getLat() {
                return lat;
            }

            public void setLat(Object lat) {
                this.lat = lat;
            }

            public Object getLon() {
                return lon;
            }

            public void setLon(Object lon) {
                this.lon = lon;
            }

            public Object getAcc() {
                return acc;
            }

            public void setAcc(Object acc) {
                this.acc = acc;
            }

            public Object getSpeed() {
                return speed;
            }

            public void setSpeed(Object speed) {
                this.speed = speed;
            }

            public Object getBatLvl() {
                return batLvl;
            }

            public void setBatLvl(Object batLvl) {
                this.batLvl = batLvl;
            }

            public Object getKet() {
                return ket;
            }

            public void setKet(Object ket) {
                this.ket = ket;
            }

            public Object getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(Object createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(Object updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getJabatan() {
                return jabatan;
            }

            public void setJabatan(String jabatan) {
                this.jabatan = jabatan;
            }

            public String getSatuanWilayah() {
                return satuanWilayah;
            }

            public void setSatuanWilayah(String satuanWilayah) {
                this.satuanWilayah = satuanWilayah;
            }

            public String getJabatanLevel() {
                return jabatanLevel;
            }

            public void setJabatanLevel(String jabatanLevel) {
                this.jabatanLevel = jabatanLevel;
            }
        }

        public class Menu {

            @SerializedName("nama")
            @Expose
            private String nama;
            @SerializedName("img")
            @Expose
            private String img;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }
        }
    }

    public class ResponseDetailPlottingTps{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("tps")
            @Expose
            private String tps;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("alamat")
            @Expose
            private String alamat;
            @SerializedName("kecamatan")
            @Expose
            private String kecamatan;
            @SerializedName("kelurahan")
            @Expose
            private String kelurahan;
            @SerializedName("penanggung_jawab")
            @Expose
            private String penanggungJawab;
            @SerializedName("tlp_penanggung_jawab")
            @Expose
            private String tlpPenanggungJawab;
            @SerializedName("id_pemilu")
            @Expose
            private String idPemilu;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTps() {
                return tps;
            }

            public void setTps(String tps) {
                this.tps = tps;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getKecamatan() {
                return kecamatan;
            }

            public void setKecamatan(String kecamatan) {
                this.kecamatan = kecamatan;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }

            public String getPenanggungJawab() {
                return penanggungJawab;
            }

            public void setPenanggungJawab(String penanggungJawab) {
                this.penanggungJawab = penanggungJawab;
            }

            public String getTlpPenanggungJawab() {
                return tlpPenanggungJawab;
            }

            public void setTlpPenanggungJawab(String tlpPenanggungJawab) {
                this.tlpPenanggungJawab = tlpPenanggungJawab;
            }

            public String getIdPemilu() {
                return idPemilu;
            }

            public void setIdPemilu(String idPemilu) {
                this.idPemilu = idPemilu;
            }
        }

    }

    public class ResponseDetailLapinfo{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("kategori")
            @Expose
            private String kategori;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("tgl_masuk")
            @Expose
            private String tglMasuk;
            @SerializedName("waktu_masuk")
            @Expose
            private String waktuMasuk;

            @SerializedName("foto")
            @Expose
            private List<Foto> foto = null;

            @SerializedName("thumbnail")
            @Expose
            private List<Thumbnail> thumbnail = null;

            @SerializedName("viewer")
            @Expose
            private Integer viewer;
            @SerializedName("comment")
            @Expose
            private Integer comment;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getKategori() {
                return kategori;
            }

            public void setKategori(String kategori) {
                this.kategori = kategori;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getTglMasuk() {
                return tglMasuk;
            }

            public void setTglMasuk(String tglMasuk) {
                this.tglMasuk = tglMasuk;
            }

            public String getWaktuMasuk() {
                return waktuMasuk;
            }

            public void setWaktuMasuk(String waktuMasuk) {
                this.waktuMasuk = waktuMasuk;
            }

            public List<Foto> getFoto() {
                return foto;
            }

            public void setFoto(List<Foto> foto) {
                this.foto = foto;
            }

            public List<Thumbnail> getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(List<Thumbnail> thumbnail) {
                this.thumbnail = thumbnail;
            }

            public Integer getViewer() {
                return viewer;
            }

            public void setViewer(Integer viewer) {
                this.viewer = viewer;
            }

            public Integer getComment() {
                return comment;
            }

            public void setComment(Integer comment) {
                this.comment = comment;
            }

        }

        public class Foto {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

        public class Thumbnail {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }
    }

    public class ResponseForgotPassword{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class ResponseSubmitData{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    public class ResponseDetailSambang{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("kategori")
            @Expose
            private String kategori;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("foto")
            @Expose
            private List<Foto> foto = null;

            @SerializedName("sasaran")
            @Expose
            private String sasaran;
            @SerializedName("pencapaian")
            @Expose
            private String pencapaian;
            @SerializedName("tgl_masuk")
            @Expose
            private String tglMasuk;
            @SerializedName("waktu_masuk")
            @Expose
            private String waktuMasuk;
            @SerializedName("keterangan")
            @Expose
            private String keterangan;

            @SerializedName("thumbnail")
            @Expose
            private List<Thumbnail> thumbnail = null;

            @SerializedName("viewer")
            @Expose
            private Integer viewer;
            @SerializedName("comment")
            @Expose
            private Integer comment;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getKategori() {
                return kategori;
            }

            public void setKategori(String kategori) {
                this.kategori = kategori;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public List<Foto> getFoto() {
                return foto;
            }

            public void setFoto(List<Foto> foto) {
                this.foto = foto;
            }

            public String getSasaran() {
                return sasaran;
            }

            public void setSasaran(String sasaran) {
                this.sasaran = sasaran;
            }

            public String getPencapaian() {
                return pencapaian;
            }

            public void setPencapaian(String pencapaian) {
                this.pencapaian = pencapaian;
            }

            public String getTglMasuk() {
                return tglMasuk;
            }

            public void setTglMasuk(String tglMasuk) {
                this.tglMasuk = tglMasuk;
            }

            public String getWaktuMasuk() {
                return waktuMasuk;
            }

            public void setWaktuMasuk(String waktuMasuk) {
                this.waktuMasuk = waktuMasuk;
            }

            public String getKeterangan() {
                return keterangan;
            }

            public void setKeterangan(String keterangan) {
                this.keterangan = keterangan;
            }

            public List<Thumbnail> getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(List<Thumbnail> thumbnail) {
                this.thumbnail = thumbnail;
            }

            public Integer getViewer() {
                return viewer;
            }

            public void setViewer(Integer viewer) {
                this.viewer = viewer;
            }

            public Integer getComment() {
                return comment;
            }

            public void setComment(Integer comment) {
                this.comment = comment;
            }

        }

        public class Foto {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

        public class Thumbnail {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

    }

    public class ResponseCheckVersion{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("app")
            @Expose
            private String app;
            @SerializedName("code")
            @Expose
            private Integer code;
            @SerializedName("name")
            @Expose
            private String name;

            public String getApp() {
                return app;
            }

            public void setApp(String app) {
                this.app = app;
            }

            public Integer getCode() {
                return code;
            }

            public void setCode(Integer code) {
                this.code = code;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

        }
    }

    public class ResponseDetailProblemSolve{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("kategori")
            @Expose
            private String kategori;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("foto")
            @Expose
            private List<Foto> foto = null;
            @SerializedName("waktu_kejadian")
            @Expose
            private String waktuKejadian;
            @SerializedName("tempat_kejadian")
            @Expose
            private String tempatKejadian;
            @SerializedName("pelapor")
            @Expose
            private String pelapor;
            @SerializedName("pekerjaan_pelapor")
            @Expose
            private String pekerjaanPelapor;
            @SerializedName("alamat_pelapor")
            @Expose
            private String alamatPelapor;
            @SerializedName("terlapor")
            @Expose
            private String terlapor;
            @SerializedName("pekerjaan_terlapor")
            @Expose
            private String pekerjaanTerlapor;
            @SerializedName("alamat_terlapor")
            @Expose
            private String alamatTerlapor;
            @SerializedName("hasil_penanganan")
            @Expose
            private String hasilPenanganan;
            @SerializedName("tgl_masuk")
            @Expose
            private String tglMasuk;
            @SerializedName("waktu_masuk")
            @Expose
            private String waktuMasuk;
            @SerializedName("keterangan")
            @Expose
            private String keterangan;

            @SerializedName("thumbnail")
            @Expose
            private List<Thumbnail> thumbnail = null;

            @SerializedName("viewer")
            @Expose
            private Integer viewer;
            @SerializedName("comment")
            @Expose
            private Integer comment;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getKategori() {
                return kategori;
            }

            public void setKategori(String kategori) {
                this.kategori = kategori;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public List<Foto> getFoto() {
                return foto;
            }

            public void setFoto(List<Foto> foto) {
                this.foto = foto;
            }

            public String getWaktuKejadian() {
                return waktuKejadian;
            }

            public void setWaktuKejadian(String waktuKejadian) {
                this.waktuKejadian = waktuKejadian;
            }

            public String getTempatKejadian() {
                return tempatKejadian;
            }

            public void setTempatKejadian(String tempatKejadian) {
                this.tempatKejadian = tempatKejadian;
            }

            public String getPelapor() {
                return pelapor;
            }

            public void setPelapor(String pelapor) {
                this.pelapor = pelapor;
            }

            public String getPekerjaanPelapor() {
                return pekerjaanPelapor;
            }

            public void setPekerjaanPelapor(String pekerjaanPelapor) {
                this.pekerjaanPelapor = pekerjaanPelapor;
            }

            public String getAlamatPelapor() {
                return alamatPelapor;
            }

            public void setAlamatPelapor(String alamatPelapor) {
                this.alamatPelapor = alamatPelapor;
            }

            public String getTerlapor() {
                return terlapor;
            }

            public void setTerlapor(String terlapor) {
                this.terlapor = terlapor;
            }

            public String getPekerjaanTerlapor() {
                return pekerjaanTerlapor;
            }

            public void setPekerjaanTerlapor(String pekerjaanTerlapor) {
                this.pekerjaanTerlapor = pekerjaanTerlapor;
            }

            public String getAlamatTerlapor() {
                return alamatTerlapor;
            }

            public void setAlamatTerlapor(String alamatTerlapor) {
                this.alamatTerlapor = alamatTerlapor;
            }

            public String getHasilPenanganan() {
                return hasilPenanganan;
            }

            public void setHasilPenanganan(String hasilPenanganan) {
                this.hasilPenanganan = hasilPenanganan;
            }

            public String getTglMasuk() {
                return tglMasuk;
            }

            public void setTglMasuk(String tglMasuk) {
                this.tglMasuk = tglMasuk;
            }

            public String getWaktuMasuk() {
                return waktuMasuk;
            }

            public void setWaktuMasuk(String waktuMasuk) {
                this.waktuMasuk = waktuMasuk;
            }

            public String getKeterangan() {
                return keterangan;
            }

            public void setKeterangan(String keterangan) {
                this.keterangan = keterangan;
            }

            public List<Thumbnail> getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(List<Thumbnail> thumbnail) {
                this.thumbnail = thumbnail;
            }

            public Integer getViewer() {
                return viewer;
            }

            public void setViewer(Integer viewer) {
                this.viewer = viewer;
            }

            public Integer getComment() {
                return comment;
            }

            public void setComment(Integer comment) {
                this.comment = comment;
            }

        }

        public class Foto {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

        public class Thumbnail {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

    }

    public class ResponseDetailLaka{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("kategori")
            @Expose
            private String kategori;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("tgl_masuk")
            @Expose
            private String tglMasuk;
            @SerializedName("waktu_masuk")
            @Expose
            private String waktuMasuk;
            @SerializedName("foto")
            @Expose
            private List<Foto> foto = null;

            @SerializedName("thumbnail")
            @Expose
            private List<Thumbnail> thumbnail = null;

            @SerializedName("viewer")
            @Expose
            private Integer viewer;
            @SerializedName("comment")
            @Expose
            private Integer comment;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getKategori() {
                return kategori;
            }

            public void setKategori(String kategori) {
                this.kategori = kategori;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getTglMasuk() {
                return tglMasuk;
            }

            public void setTglMasuk(String tglMasuk) {
                this.tglMasuk = tglMasuk;
            }

            public String getWaktuMasuk() {
                return waktuMasuk;
            }

            public void setWaktuMasuk(String waktuMasuk) {
                this.waktuMasuk = waktuMasuk;
            }

            public List<Foto> getFoto() {
                return foto;
            }

            public void setFoto(List<Foto> foto) {
                this.foto = foto;
            }

            public List<Thumbnail> getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(List<Thumbnail> thumbnail) {
                this.thumbnail = thumbnail;
            }

            public Integer getViewer() {
                return viewer;
            }

            public void setViewer(Integer viewer) {
                this.viewer = viewer;
            }

            public Integer getComment() {
                return comment;
            }

            public void setComment(Integer comment) {
                this.comment = comment;
            }

        }

        public class Foto {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

        public class Thumbnail {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

    }

    public class ResponseDetailListBarcode{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("nrp")
            @Expose
            private String nrp;
            @SerializedName("kategori")
            @Expose
            private String kategori;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("tgl_masuk")
            @Expose
            private String tglMasuk;
            @SerializedName("waktu_masuk")
            @Expose
            private String waktuMasuk;
            @SerializedName("foto")
            @Expose
            private List<Foto> foto = null;
            @SerializedName("thumbnail")
            @Expose
            private List<Thumbnail> thumbnail = null;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNrp() {
                return nrp;
            }

            public void setNrp(String nrp) {
                this.nrp = nrp;
            }

            public String getKategori() {
                return kategori;
            }

            public void setKategori(String kategori) {
                this.kategori = kategori;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getTglMasuk() {
                return tglMasuk;
            }

            public void setTglMasuk(String tglMasuk) {
                this.tglMasuk = tglMasuk;
            }

            public String getWaktuMasuk() {
                return waktuMasuk;
            }

            public void setWaktuMasuk(String waktuMasuk) {
                this.waktuMasuk = waktuMasuk;
            }

            public List<Foto> getFoto() {
                return foto;
            }

            public void setFoto(List<Foto> foto) {
                this.foto = foto;
            }

            public List<Thumbnail> getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(List<Thumbnail> thumbnail) {
                this.thumbnail = thumbnail;
            }

        }

        public class Foto {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }

        public class Thumbnail {

            @SerializedName("nama")
            @Expose
            private String nama;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

        }
    }

    public class ResponseSubmitComment{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class ResponseBase{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class ResponseCheckoutcode {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("durasi")
        @Expose
        private String durasi;
        @SerializedName("alamat")
        @Expose
        private String alamat;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDurasi() {
            return durasi;
        }

        public void setDurasi(String durasi) {
            this.durasi = durasi;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

    }

    public class ResponsePostLaporan{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    public class ResponseCategoryAllReport{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<CategoryReport> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<CategoryReport> getData() {
            return data;
        }

        public void setData(List<CategoryReport> data) {
            this.data = data;
        }
    }

    public class ResponseHistoryPosGatur{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<HistoryPosGatur> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<HistoryPosGatur> getData() {
            return data;
        }

        public void setData(List<HistoryPosGatur> data) {
            this.data = data;
        }
    }

    public class ResponseListSambang{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Sambang> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Sambang> getData() {
            return data;
        }

        public void setData(List<Sambang> data) {
            this.data = data;
        }
    }

    public class ResponseListPostGatur{

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<PostGatur> data = null;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<PostGatur> getData() {
            return data;
        }

        public void setData(List<PostGatur> data) {
            this.data = data;
        }
    }

    public class ResponseDetailPosGatur{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("alamat")
            @Expose
            private String alamat;
            @SerializedName("pimpinan")
            @Expose
            private String pimpinan;
            @SerializedName("tlp")
            @Expose
            private String tlp;

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getPimpinan() {
                return pimpinan;
            }

            public void setPimpinan(String pimpinan) {
                this.pimpinan = pimpinan;
            }

            public String getTlp() {
                return tlp;
            }

            public void setTlp(String tlp) {
                this.tlp = tlp;
            }

        }
    }

    public class ResponseCheckinGatur{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("alamat")
            @Expose
            private String alamat;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("durasi")
            @Expose
            private String durasi;

            @SerializedName("check_in")
            @Expose
            private String checkIn;

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getDurasi() {
                return durasi;
            }

            public void setDurasi(String durasi) {
                this.durasi = durasi;
            }

            public String getCheckIn() {
                return checkIn;
            }

            public void setCheckIn(String checkIn) {
                this.checkIn = checkIn;
            }

        }

    }

    public class ResponseDetailObvit{
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("nama")
            @Expose
            private String nama;
            @SerializedName("alamat")
            @Expose
            private String alamat;
            @SerializedName("lat")
            @Expose
            private String lat;
            @SerializedName("lon")
            @Expose
            private String lon;
            @SerializedName("penanggung_jwb")
            @Expose
            private String penanggungJwb;
            @SerializedName("tlp")
            @Expose
            private String tlp;

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getPenanggungJwb() {
                return penanggungJwb;
            }

            public void setPenanggungJwb(String penanggungJwb) {
                this.penanggungJwb = penanggungJwb;
            }

            public String getTlp() {
                return tlp;
            }

            public void setTlp(String tlp) {
                this.tlp = tlp;
            }

        }

    }

    public class ResponseDetailHumas{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("judul")
            @Expose
            private String judul;
            @SerializedName("konten")
            @Expose
            private String konten;
            @SerializedName("foto")
            @Expose
            private String foto;
            @SerializedName("thumbnail")
            @Expose
            private String thumbnail;
            @SerializedName("created_at")
            @Expose
            private String createdAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getJudul() {
                return judul;
            }

            public void setJudul(String judul) {
                this.judul = judul;
            }

            public String getKonten() {
                return konten;
            }

            public void setKonten(String konten) {
                this.konten = konten;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }
        }
    }

    public class ResponseListLapinfo{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Lapinfo> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Lapinfo> getData() {
            return data;
        }

        public void setData(List<Lapinfo> data) {
            this.data = data;
        }
    }

    public class ResponseDetailSuaraTps{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("foto")
        @Expose
        private String foto;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("data")
        @Expose
        private List<DetailSuara> data = null;

        public List<DetailSuara> getData() {
            return data;
        }

        public void setData(List<DetailSuara> data) {
            this.data = data;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getFoto() {
            return foto;
        }

        public void setFoto(String foto) {
            this.foto = foto;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }
    }

    public class ResponseListKelurahan{

        @SerializedName("data")
        @Expose
        private List<Kelurahan> data = null;

        public List<Kelurahan> getData() {
            return data;
        }

        public void setData(List<Kelurahan> data) {
            this.data = data;
        }
    }

    public class ResponseListKecamatan{

        @SerializedName("data")
        @Expose
        private List<Kecamatan> data = null;

        public List<Kecamatan> getData() {
            return data;
        }

        public void setData(List<Kecamatan> data) {
            this.data = data;
        }
    }

    public class ResponseListMenu{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Mainmenu> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Mainmenu> getData() {
            return data;
        }
        public void setData(List<Mainmenu> data) {
            this.data = data;
        }
    }

    public class ResponseReportCommment{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Comment> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Comment> getData() {
            return data;
        }

        public void setData(List<Comment> data) {
            this.data = data;
        }
    }

    public class ResponseListProblemSolve{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<ProblemSolve> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<ProblemSolve> getData() {
            return data;
        }

        public void setData(List<ProblemSolve> data) {
            this.data = data;
        }
    }

    public class ResponseListPetugasTps{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Tps> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Tps> getData() {
            return data;
        }

        public void setData(List<Tps> data) {
            this.data = data;
        }
    }

    public class ResponseListPenugasan{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Penugasan> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Penugasan> getData() {
            return data;
        }

        public void setData(List<Penugasan> data) {
            this.data = data;
        }
    }

    public class ResponseListTugas{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Tugas> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Tugas> getData() {
            return data;
        }

        public void setData(List<Tugas> data) {
            this.data = data;
        }
    }

    public class ResponseListPemilu{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Pemilu> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Pemilu> getData() {
            return data;
        }

        public void setData(List<Pemilu> data) {
            this.data = data;
        }

    }

    public class ResponseListPeserta{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Peserta> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Peserta> getData() {
            return data;
        }

        public void setData(List<Peserta> data) {
            this.data = data;
        }

    }

    public class ResponseListSuara{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Suara> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Suara> getData() {
            return data;
        }

        public void setData(List<Suara> data) {
            this.data = data;
        }

    }

    public class ResponseListSuaraKapolsek {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<SuaraKapolsek> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<SuaraKapolsek> getData() {
            return data;
        }

        public void setData(List<SuaraKapolsek> data) {
            this.data = data;
        }

    }

    public class ResponseSummarySuaraKapolsek {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<SummarySuaraKapolsek> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<SummarySuaraKapolsek> getData() {
            return data;
        }

        public void setData(List<SummarySuaraKapolsek> data) {
            this.data = data;
        }

    }

    public class ResponseListBerita{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Berita> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Berita> getData() {
            return data;
        }

        public void setData(List<Berita> data) {
            this.data = data;
        }
    }

    public class ResponseListKesatuan{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<Kesatuan> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Kesatuan> getData() {
            return data;
        }

        public void setData(List<Kesatuan> data) {
            this.data = data;
        }
    }

    public class ResponseListPersonil{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<Personil> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Personil> getData() {
            return data;
        }

        public void setData(List<Personil> data) {
            this.data = data;
        }
    }

    public class ResponseListSatuanWilayah{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<SatuanWilayah> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<SatuanWilayah> getData() {
            return data;
        }

        public void setData(List<SatuanWilayah> data) {
            this.data = data;
        }
    }

    public class ResponseListLaka{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Laka> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Laka> getData() {
            return data;
        }

        public void setData(List<Laka> data) {
            this.data = data;
        }
    }

    public class ResponseListBarcode {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<PatroliBarcode> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<PatroliBarcode> getData() {
            return data;
        }

        public void setData(List<PatroliBarcode> data) {
            this.data = data;
        }

    }

    public class ResponseListJadwal{

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<Schedule> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Schedule> getData() {
            return data;
        }

        public void setData(List<Schedule> data) {
            this.data = data;
        }
    }

    public class ResponseListJadwalBulan {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<JadwalBulan> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<JadwalBulan> getData() {
            return data;
        }

        public void setData(List<JadwalBulan> data) {
            this.data = data;
        }
    }

    public class ResponseListDisposisi {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<Disposisi> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Disposisi> getData() {
            return data;
        }

        public void setData(List<Disposisi> data) {
            this.data = data;
        }
    }

    public class ResponseDetailJadwal {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Jadwal data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Jadwal getData() {
            return data;
        }

        public void setData(Jadwal data) {
            this.data = data;
        }

        public class Jadwal {
            @SerializedName("waktu")
            @Expose
            private String waktu;
            @SerializedName("kegiatan")
            @Expose
            private String kegiatan;
            @SerializedName("lokasi")
            @Expose
            private String lokasi;
            @SerializedName("peserta")
            @Expose
            private String peserta;
            @SerializedName("note")
            @Expose
            private String notes;
            @SerializedName("jenis")
            @Expose
            private String jenis;
            @SerializedName("status")
            @Expose
            private String status;
            @SerializedName("disposisi")
            @Expose
            private String disposisi;

            public String getWaktu() {
                return waktu;
            }

            public void setWaktu(String waktu) {
                this.waktu = waktu;
            }

            public String getKegiatan() {
                return kegiatan;
            }

            public void setKegiatan(String kegiatan) {
                this.kegiatan = kegiatan;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getPeserta() {
                return peserta;
            }

            public void setPeserta(String peserta) {
                this.peserta = peserta;
            }

            public String getNotes() {
                return notes;
            }

            public void setNotes(String notes) {
                this.notes = notes;
            }

            public String getJenis() {
                return jenis;
            }

            public void setJenis(String jenis) {
                this.jenis = jenis;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDisposisi() {
                return disposisi;
            }

            public void setDisposisi(String disposisi) {
                this.disposisi = disposisi;
            }
        }
    }

    public class ResponseListRutePatroli{

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private List<RutePatroli> data = null;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<RutePatroli> getData() {
            return data;
        }

        public void setData(List<RutePatroli> data) {
            this.data = data;
        }
    }

    public class ResponseDetailTugas {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("menugasi")
            @Expose
            private String menugasi;
            @SerializedName("foto")
            @Expose
            private String foto;
            @SerializedName("uraian")
            @Expose
            private String uraian;
            @SerializedName("created_at")
            @Expose
            private String createdAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getMenugasi() {
                return menugasi;
            }

            public void setMenugasi(String menugasi) {
                this.menugasi = menugasi;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }

            public String getUraian() {
                return uraian;
            }

            public void setUraian(String uraian) {
                this.uraian = uraian;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

        }
    }

    public class ResponseListFungsi{
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<Fungsi> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Fungsi> getData() {
            return data;
        }

        public void setData(List<Fungsi> data) {
            this.data = data;
        }
    }

    public class ResponsePerformanceLapInfo {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<PerformanceLapInfo> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<PerformanceLapInfo> getData() {
            return data;
        }

        public void setData(List<PerformanceLapInfo> data) {
            this.data = data;
        }
    }

    public class ResponsePerformanceLakaLantas {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<PerformanceLakaLantas> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<PerformanceLakaLantas> getData() {
            return data;
        }

        public void setData(List<PerformanceLakaLantas> data) {
            this.data = data;
        }
    }

    public class ResponsePerformancePatroli {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<PerformancePatroli> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<PerformancePatroli> getData() {
            return data;
        }

        public void setData(List<PerformancePatroli> data) {
            this.data = data;
        }
    }
}
