package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Penugasan {

    @SerializedName("personil")
    @Expose
    private String personil;
    @SerializedName("pangkat")
    @Expose
    private String pangkat;
    @SerializedName("wilayah")
    @Expose
    private String wilayah;
    @SerializedName("satuan")
    @Expose
    private String satuan;
    @SerializedName("uraian")
    @Expose
    private String uraian;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("dilihat")
    @Expose
    private Boolean dilihat;
    @SerializedName("response")
    @Expose
    private Object response;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPersonil() {
        return personil;
    }

    public void setPersonil(String personil) {
        this.personil = personil;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public Boolean getDilihat() {
        return dilihat;
    }

    public void setDilihat(Boolean dilihat) {
        this.dilihat = dilihat;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

}