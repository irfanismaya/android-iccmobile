package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatroliBarcode {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nrp")
    @Expose
    private String nrp;
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("uraian")
    @Expose
    private String uraian;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("tgl_masuk")
    @Expose
    private String tglMasuk;
    @SerializedName("waktu_masuk")
    @Expose
    private String waktuMasuk;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("viewer")
    @Expose
    private Integer viewer;
    @SerializedName("comment")
    @Expose
    private Integer comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTglMasuk() {
        return tglMasuk;
    }

    public void setTglMasuk(String tglMasuk) {
        this.tglMasuk = tglMasuk;
    }

    public String getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(String waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getViewer() {
        return viewer;
    }

    public void setViewer(Integer viewer) {
        this.viewer = viewer;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

}
