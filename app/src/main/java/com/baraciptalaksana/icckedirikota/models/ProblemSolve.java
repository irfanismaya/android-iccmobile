package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProblemSolve {
    @Expose
    private Integer id;
    @SerializedName("nrp")
    @Expose
    private String nrp;
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("uraian")
    @Expose
    private String uraian;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("waktu_kejadian")
    @Expose
    private String waktuKejadian;
    @SerializedName("tempat_kejadian")
    @Expose
    private String tempatKejadian;
    @SerializedName("pelapor")
    @Expose
    private String pelapor;
    @SerializedName("pekerjaan_pelapor")
    @Expose
    private String pekerjaanPelapor;
    @SerializedName("alamat_pelapor")
    @Expose
    private String alamatPelapor;
    @SerializedName("terlapor")
    @Expose
    private String terlapor;
    @SerializedName("pekerjaan_terlapor")
    @Expose
    private String pekerjaanTerlapor;
    @SerializedName("alamat_terlapor")
    @Expose
    private String alamatTerlapor;
    @SerializedName("hasil_penanganan")
    @Expose
    private String hasilPenanganan;
    @SerializedName("tgl_masuk")
    @Expose
    private String tglMasuk;
    @SerializedName("waktu_masuk")
    @Expose
    private String waktuMasuk;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("viewer")
    @Expose
    private Integer viewer;
    @SerializedName("comment")
    @Expose
    private Integer comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getWaktuKejadian() {
        return waktuKejadian;
    }

    public void setWaktuKejadian(String waktuKejadian) {
        this.waktuKejadian = waktuKejadian;
    }

    public String getTempatKejadian() {
        return tempatKejadian;
    }

    public void setTempatKejadian(String tempatKejadian) {
        this.tempatKejadian = tempatKejadian;
    }

    public String getPelapor() {
        return pelapor;
    }

    public void setPelapor(String pelapor) {
        this.pelapor = pelapor;
    }

    public String getPekerjaanPelapor() {
        return pekerjaanPelapor;
    }

    public void setPekerjaanPelapor(String pekerjaanPelapor) {
        this.pekerjaanPelapor = pekerjaanPelapor;
    }

    public String getAlamatPelapor() {
        return alamatPelapor;
    }

    public void setAlamatPelapor(String alamatPelapor) {
        this.alamatPelapor = alamatPelapor;
    }

    public String getTerlapor() {
        return terlapor;
    }

    public void setTerlapor(String terlapor) {
        this.terlapor = terlapor;
    }

    public String getPekerjaanTerlapor() {
        return pekerjaanTerlapor;
    }

    public void setPekerjaanTerlapor(String pekerjaanTerlapor) {
        this.pekerjaanTerlapor = pekerjaanTerlapor;
    }

    public String getAlamatTerlapor() {
        return alamatTerlapor;
    }

    public void setAlamatTerlapor(String alamatTerlapor) {
        this.alamatTerlapor = alamatTerlapor;
    }

    public String getHasilPenanganan() {
        return hasilPenanganan;
    }

    public void setHasilPenanganan(String hasilPenanganan) {
        this.hasilPenanganan = hasilPenanganan;
    }

    public String getTglMasuk() {
        return tglMasuk;
    }

    public void setTglMasuk(String tglMasuk) {
        this.tglMasuk = tglMasuk;
    }

    public String getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(String waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getViewer() {
        return viewer;
    }

    public void setViewer(Integer viewer) {
        this.viewer = viewer;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
