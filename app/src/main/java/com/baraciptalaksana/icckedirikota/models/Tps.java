package com.baraciptalaksana.icckedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tps {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tps")
    @Expose
    private String tps;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("hasil")
    @Expose
    private String hasil;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nrp")
    @Expose
    private String nrp;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("lat_anggota")
    @Expose
    private String latAnggota;
    @SerializedName("lon_anggota")
    @Expose
    private String lonAnggota;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("kelurahan")
    @Expose
    private String kelurahan;
    @SerializedName("penanggung_jawab")
    @Expose
    private String penanggungJawab;
    @SerializedName("tlp_penanggung_jawab")
    @Expose
    private String tlpPenanggungJawab;
    @SerializedName("jarak_m")
    @Expose
    private String jarakM;

    @SerializedName("jarak")
    @Expose
    private String jarak;

    @SerializedName("pangkat")
    @Expose
    private String pangkat;
    @SerializedName("fungsi")
    @Expose
    private String fungsi;
    @SerializedName("jabatan")
    @Expose
    private String jabatan;
    @SerializedName("jabatan_level")
    @Expose
    private String jabatanLevel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTps() {
        return tps;
    }

    public void setTps(String tps) {
        this.tps = tps;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getHasil() {
        return hasil;
    }

    public void setHasil(String hasil) {
        this.hasil = hasil;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getLatAnggota() {
        return latAnggota;
    }

    public void setLatAnggota(String latAnggota) {
        this.latAnggota = latAnggota;
    }

    public String getLonAnggota() {
        return lonAnggota;
    }

    public void setLonAnggota(String lonAnggota) {
        this.lonAnggota = lonAnggota;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getPenanggungJawab() {
        return penanggungJawab;
    }

    public void setPenanggungJawab(String penanggungJawab) {
        this.penanggungJawab = penanggungJawab;
    }

    public String getTlpPenanggungJawab() {
        return tlpPenanggungJawab;
    }

    public void setTlpPenanggungJawab(String tlpPenanggungJawab) {
        this.tlpPenanggungJawab = tlpPenanggungJawab;
    }

    public String getJarakM() {
        return jarakM;
    }

    public void setJarakM(String jarakM) {
        this.jarakM = jarakM;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getJabatanLevel() {
        return jabatanLevel;
    }

    public void setJabatanLevel(String jabatanLevel) {
        this.jabatanLevel = jabatanLevel;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

}