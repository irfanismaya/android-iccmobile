package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.ReportInformationAdapter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.CategoryReport;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.utils.Tools;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.LinePagerIndicatorDecoration;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityCreateJadwal extends AppCompatActivity {

    @BindView(R.id.et_kegiatan) AppCompatEditText et_kegiatan;
    @BindView(R.id.et_waktu_mulai) AppCompatEditText et_waktu_mulai;
    @BindView(R.id.et_waktu_berakhir) AppCompatEditText et_waktu_berakhir;
    @BindView(R.id.et_lokasi) AppCompatEditText et_lokasi;
    @BindView(R.id.et_peserta) AppCompatEditText et_peserta;
    @BindView(R.id.et_catatan) AppCompatEditText et_catatan;
    @BindView(R.id.et_status) AppCompatEditText et_status;
    @BindView(R.id.spnr_jenis_kegiatan) AppCompatSpinner spnr_jenis_kegiatan;
    @BindView(R.id.btn_submit) Button btn_submit;
    @BindView(R.id.loading) RotateLoading loading;

    DatePickerDialog pickerjadwal;
    private Context mContext = this;

    private int PLACE_PICKER_REQUEST_LOKASI = 1;
    private static final int REQUEST_LOCATION = 2;
    LocationManager locationManager;
    String lattitude,longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_jadwal);
        initToolbar();
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityCreateJadwal.this, R.color.color_laporan_sambang);

        String[] penanganan = getResources().getStringArray(R.array.status_problemsolve);
        ArrayAdapter<String> arraypenanganan = new ArrayAdapter<>(getApplicationContext(), R.layout.simple_spinner_item, penanganan);
        arraypenanganan.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spnr_jenis_kegiatan.setAdapter(arraypenanganan);
        spnr_jenis_kegiatan.setSelection(0);

        spnr_jenis_kegiatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        checkCurrentLocation();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buat Jadwal");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.et_waktu_mulai)
    public void onWaktuMulai() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        pickerjadwal = new DatePickerDialog(ActivityCreateJadwal.this,
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {

                    Calendar cur_calender = Calendar.getInstance();
                    TimePickerDialog datePicker = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            et_waktu_mulai.setText(year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth +" "+hourOfDay + ":" +minute+ ":" +second);
                        }
                    }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
                    datePicker.setThemeDark(false);
                    datePicker.setAccentColor(getResources().getColor(R.color.color_laporan_sambang));
                    datePicker.show(getFragmentManager(), "Timepickerdialog");

                }
            }, year, month, day);
        pickerjadwal.show();
    }

    @OnClick(R.id.et_waktu_berakhir)
    public void onWaktuBerakhir() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        pickerjadwal = new DatePickerDialog(ActivityCreateJadwal.this,
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {

                    Calendar cur_calender = Calendar.getInstance();
                    TimePickerDialog datePicker = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            et_waktu_berakhir.setText(year+ "-" + (monthOfYear + 1) + "-" + dayOfMonth +" "+hourOfDay + ":" + minute+ ":" + second);
                        }
                    }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
                    datePicker.setThemeDark(false);
                    datePicker.setAccentColor(getResources().getColor(R.color.color_laporan_sambang));
                    datePicker.show(getFragmentManager(), "Timepickerdialog");

                }
            }, year, month, day);
        pickerjadwal.show();
    }


    @OnClick(R.id.et_lokasi)
    public void onChangeLocationPerkara() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(ActivityCreateJadwal.this), PLACE_PICKER_REQUEST_LOKASI);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitData() {
        if(checkForm()){
            if(Helper.isNetworkAvailable(mContext)){
                createjadwal(SharedPreferences.getToken(mContext));
            }else {
                Toasty.warning(mContext, "Silahkan periksa kembali jaringan ada", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    private boolean checkForm() {
        if(et_kegiatan.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Nama kegiatan tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_waktu_mulai.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Waktu mulai tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_waktu_berakhir.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Waktu berakhir tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_lokasi.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Lokasi tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_peserta.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Nama peserta tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_catatan.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Catatan tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_status.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Status tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else {
            return true;
        }
    }

    private void clearForm(){
        et_kegiatan.setText("");
        et_waktu_mulai.setText("");
        et_waktu_berakhir.setText("");
        et_peserta.setText("");
        et_catatan.setText("");
        et_status.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST_LOKASI) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format(
                        "Place: %s \n" +
                                "Alamat: %s \n" +
                                "Latlng %s \n", place.getName(), place.getAddress(), place.getLatLng().latitude+" "+place.getLatLng().longitude);
                et_lokasi.setText(place.getLatLng().latitude+","+place.getLatLng().longitude);
            }
        }
    }

    private void createjadwal(String token) {
        Request.JadwalRequest request = new Request().new JadwalRequest();
        request.setNrp(SharedPreferences.getNrp(mContext));
        request.setKegiatan(et_kegiatan.getText().toString());
        request.setWaktuStart(et_waktu_mulai.getText().toString());
        request.setWaktuEnd(et_waktu_berakhir.getText().toString());
        request.setLokasi(et_lokasi.getText().toString());
        request.setPeserta(et_peserta.getText().toString());
        request.setNote(et_catatan.getText().toString());
        request.setStatus(et_status.getText().toString());
        request.setJenis(spnr_jenis_kegiatan.getSelectedItem().toString());
        callsubmitjadwal(token, request);
    }

    private void callsubmitjadwal(String token, final Request.JadwalRequest request) {
        loading.start();
        ApiInterface.Factory.getInstance().submitjadwal("Bearer " + token, request).enqueue(new Callback<Response.ResponseSubmitData>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitData> call, retrofit2.Response<Response.ResponseSubmitData> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        clearForm();
                        finish();
                    } else {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }else{
                    Toasty.success(mContext, "Email tidak ditemukan", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitData> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });

    }

    private void checkCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ActivityCreateJadwal.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (ActivityCreateJadwal.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ActivityCreateJadwal.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            }else{
                Toasty.warning(mContext, "Unble to Trace your location", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

}
