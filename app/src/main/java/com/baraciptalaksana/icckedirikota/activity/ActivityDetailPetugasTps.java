package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.R;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityDetailPetugasTps extends AppCompatActivity {
    @BindView(R.id.btn_checkin)
    Button btn_checkin;

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.ib_edit)
    ImageButton ib_edit;

    @BindView(R.id.tv_edit)
    TextView tv_edit;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_address_full)
    TextView tv_address_full;

    @BindView(R.id.tv_name_officer)
    TextView tv_name_officer;

    @BindView(R.id.tv_telp)
    TextView tv_telp;

    @BindView(R.id.tv_koordinat)
    TextView tv_koordinat;

    Bundle mBundle;
    int id = 0;
    String tps, lat, lon, hasil, nama, alamat, penanggung_jawab, tlp_penanggung_jawab, kecamatan, kelurahan ="";

    private Context mContext = this;
    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_petugastps);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailPetugasTps.this, R.color.color_laporan_sambang);
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            id = mBundle.getInt("id");
            tps = mBundle.getString("tps");
            lat = mBundle.getString("lat");
            lon = mBundle.getString("lon");
            hasil = mBundle.getString("hasil");
            nama = mBundle.getString("nama");
            alamat = mBundle.getString("alamat");
            kecamatan = mBundle.getString("kecamatan");
            kelurahan = mBundle.getString("kelurahan");
            penanggung_jawab = mBundle.getString("penanggung_jawab");
            tlp_penanggung_jawab = mBundle.getString("tlp_penanggung_jawab");
            tv_edit.setText(tps);
            tv_address.setText("Kec. "+kecamatan+", "+"Kel. "+kelurahan);
            tv_address_full.setText(alamat);
            tv_name_officer.setText(penanggung_jawab);
            tv_telp.setText(tlp_penanggung_jawab);

            tv_koordinat.setText(lat+","+lon);
            showMap(Double.parseDouble(lat) ,Double.parseDouble(lon));
        }
    }

    @OnClick(R.id.btn_checkin)
    public void onCheckin() {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:"+lat+","+lon));
        startActivity(intent);
    }

    @OnClick(R.id.tv_edit)
    public void onEditData() {
        Intent logInIntent = new Intent(ActivityDetailPetugasTps.this, ActivityEditPetugasTps.class);
        logInIntent.putExtra("id", id);
        logInIntent.putExtra("tps", tps);
        logInIntent.putExtra("lat", lat);
        logInIntent.putExtra("lon", lon);
        logInIntent.putExtra("nama", nama);
        logInIntent.putExtra("alamat", alamat);
        logInIntent.putExtra("kecamatan", kecamatan);
        logInIntent.putExtra("kelurahan", kelurahan);
        logInIntent.putExtra("penanggung_jawab", penanggung_jawab);
        logInIntent.putExtra("tlp_penanggung_jawab", tlp_penanggung_jawab);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @OnClick(R.id.ib_edit)
    public void onEditDataPetugas() {
        Intent logInIntent = new Intent(ActivityDetailPetugasTps.this, ActivityEditPetugasTps.class);
        logInIntent.putExtra("id", id);
        logInIntent.putExtra("tps", tps);
        logInIntent.putExtra("lat", lat);
        logInIntent.putExtra("lon", lon);
        logInIntent.putExtra("nama", nama);
        logInIntent.putExtra("alamat", alamat);
        logInIntent.putExtra("kecamatan", kecamatan);
        logInIntent.putExtra("kelurahan", kelurahan);
        logInIntent.putExtra("penanggung_jawab", penanggung_jawab);
        logInIntent.putExtra("tlp_penanggung_jawab", tlp_penanggung_jawab);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail TPS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

}
