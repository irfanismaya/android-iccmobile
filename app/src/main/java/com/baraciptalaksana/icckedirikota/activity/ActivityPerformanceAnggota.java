package com.baraciptalaksana.icckedirikota.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Fungsi;
import com.baraciptalaksana.icckedirikota.models.PerformanceLakaLantas;
import com.baraciptalaksana.icckedirikota.models.PerformanceLapInfo;
import com.baraciptalaksana.icckedirikota.models.PerformancePatroli;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.victor.loading.rotate.RotateLoading;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPerformanceAnggota extends AppCompatActivity {
    private static final String TAG = ActivityPolicePosition.class.getSimpleName();

    private PieChart mChartLogin, mChartLapInfo;
    private BarChart mChartLakaLantas, mChartPatroli;

    private HashMap<Integer, Fungsi> listFungsi = new HashMap<>();
    private HashMap<Integer, Float> listLogin = new HashMap<>();
    HashMap<Integer, DatabaseReference> listReff = new HashMap<>();

    List<PerformanceLapInfo> listLapInfo = new ArrayList<>();
    List<PerformanceLakaLantas> listLakaLantas = new ArrayList<>();
    List<PerformancePatroli> listPatroli = new ArrayList<>();

    RotateLoading loadingChartLogin, loadingChartLapInfo, loadingChartLakaLantas, loadingChartPatroli;
    Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_anggota);
        Helper.darkenStatusBar(ActivityPerformanceAnggota.this, R.color.color_laporan_performance);
        initToolbar();

        loadingChartLogin = findViewById(R.id.loadingChartLogin);
        loadingChartLapInfo = findViewById(R.id.loadingChartLapInfo);
        loadingChartLakaLantas = findViewById(R.id.loadingChartLakaLantas);
        loadingChartPatroli = findViewById(R.id.loadingChartPatroli);

        mChartLogin = findViewById(R.id.chartLogin);
        mChartLapInfo = findViewById(R.id.chartLapInfo);
        mChartLakaLantas = findViewById(R.id.chartLakaLantas);
        mChartPatroli = findViewById(R.id.chartPatroli);

        initPieChart(mChartLogin);
        initPieChart(mChartLapInfo);
        initBarChart(mChartLakaLantas);
        initBarChart(mChartPatroli);

        getListFungsi(SharedPreferences.getToken(this));
        getPerfLapInfo(SharedPreferences.getToken(this));
        getPerfLakaLantas(SharedPreferences.getToken(this));
        getPerfPatroli(SharedPreferences.getToken(this));
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Performance Anggota");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initPieChart(PieChart pieChart) {
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setDrawHoleEnabled(false);
        pieChart.setDrawCenterText(false);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        pieChart.setUsePercentValues(false);


        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(7f);
        l.setYOffset(15f);
        l.setWordWrapEnabled(true);

        // entry label styling
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setEntryLabelTextSize(12f);
        pieChart.setVisibility(View.INVISIBLE);
    }

    private void setToPieChart(ArrayList<PieEntry> entries, PieChart pieChart, String satuan) {
        PieDataSet dataSet = new PieDataSet(entries, " ");
        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new SimpleFormatter(satuan));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        pieChart.setData(data);

        // undo all highlights
        pieChart.highlightValues(null);
        pieChart.invalidate();
        pieChart.setVisibility(View.VISIBLE);
    }

    private void initBarChart(BarChart mChart) {
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        IAxisValueFormatter formatter = new SimpleFormatter("");

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(formatter);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setGranularity(1f);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(formatter);
        rightAxis.setGranularity(1f);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setEnabled(false);

        mChart.setVisibility(View.INVISIBLE);

//        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
//        mv.setChartView(mChart); // For bounds control
//        mChart.setMarker(mv); // Set the marker to the chart

        // setting data
//        mSeekBarY.setProgress(50);
//        mSeekBarX.setProgress(12);
//
//        mSeekBarY.setOnSeekBarChangeListener(this);
//        mSeekBarX.setOnSeekBarChangeListener(this);
    }

    private void setToBarChart(BarChart mChart, ArrayList<BarEntry> yVals1, String[] labels) {
        mChart.getXAxis().setValueFormatter(new LabelFormatter(labels));

        BarDataSet set1 = new BarDataSet(yVals1, " ");
        set1.setDrawIcons(false);
        set1.setValueFormatter(new SimpleFormatter(""));

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        set1.setColors(colors);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);

        mChart.setData(data);
        mChart.setVisibility(View.VISIBLE);
    }

    // Chart User Aktif per Satuan Fungsi
    private void setDataLogin() {
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        LinkedHashMap<Integer, Float> sortedList = Helper.sortHashMapByValues(listLogin);

        int i = 0;
        for (Integer key: sortedList.keySet()) {
            if (sortedList.get(key) > 0) {
                entries.add(new PieEntry(sortedList.get(key), listFungsi.get(key).getFungsi()));
                if (i < 4) i++;
                else break;
            }
        }

        setToPieChart(entries, mChartLogin, "Personil");
    }

    private void loginToFirebase(final String fungsi, final int i) {
        String email = getString(R.string.firebase_email);
        String password = getString(R.string.firebase_password);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser == null) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        subscribeToUpdates(fungsi, i);
                        Log.d(TAG, "firebase auth success");
                    } else {
                        Log.d(TAG, "firebase auth failed");
                    }
                }
            });
        } else {
            subscribeToUpdates(fungsi, i);
        }
    }

    private void subscribeToUpdates(final String fungsi, final int i) {
        String path = (fungsi == null) ? getString(R.string.firebase_path) : "fungsi/"+fungsi;

        listReff.put(i, FirebaseDatabase.getInstance().getReference(path));

        listReff.get(i).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listLogin.put(i, (float) dataSnapshot.getChildrenCount());
                setDataLogin();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getListFungsi(String token) {
        loadingChartLogin.start();

        ApiInterface.Factory.getInstance().listfungsi("Bearer " + token).enqueue(new Callback<Response.ResponseListFungsi>() {
            @Override
            public void onResponse(Call<Response.ResponseListFungsi> call, retrofit2.Response<Response.ResponseListFungsi> response) {
                loadingChartLogin.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            listFungsi.put(i, response.body().getData().get(i));
                            listLogin.put(i, 0f);
                            loginToFirebase(listFungsi.get(i).getFungsi(), i);
                        }
                        setDataLogin();
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mActivity, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mActivity);
                        Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mActivity, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mActivity);
                    Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListFungsi> call, Throwable t) {
                loadingChartLogin.stop();
                t.printStackTrace();
            }
        });
    }

    // Chart Lap Info per Satuan Fungsi
    private void setDataLapInfo() {
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int i = 0; i < listLapInfo.size(); i++) {
            entries.add(new PieEntry(listLapInfo.get(i).getJumlah(), listLapInfo.get(i).getFungsi()));
        }

        setToPieChart(entries, mChartLapInfo, "");
    }

    private void getPerfLapInfo(String token) {
        loadingChartLapInfo.start();

        ApiInterface.Factory.getInstance().performanceLapInfo("Bearer " + token).enqueue(new Callback<Response.ResponsePerformanceLapInfo>() {
            @Override
            public void onResponse(Call<Response.ResponsePerformanceLapInfo> call, retrofit2.Response<Response.ResponsePerformanceLapInfo> response) {
                loadingChartLapInfo.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listLapInfo = response.body().getData();

                        setDataLapInfo();
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mActivity, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mActivity);
                        Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mActivity, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mActivity);
                    Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponsePerformanceLapInfo> call, Throwable t) {
                loadingChartLapInfo.stop();
                t.printStackTrace();
            }
        });
    }

    // Chart Laka Lantas
    private void setDataLakaLantas() {
        ArrayList<BarEntry> yVals1 = new ArrayList<>();
        String[] labels = new String[listLakaLantas.size()];

        for (int i = 0; i < labels.length; i++) {
            yVals1.add(new BarEntry(i, listLakaLantas.get(i).getJumlah()));
            labels[i] = listLakaLantas.get(i).getBulan();
        }

        setToBarChart(mChartLakaLantas, yVals1, labels);
    }

    private void getPerfLakaLantas(String token) {
        loadingChartLakaLantas.start();

        ApiInterface.Factory.getInstance().performanceLakaLantas("Bearer " + token).enqueue(new Callback<Response.ResponsePerformanceLakaLantas>() {
            @Override
            public void onResponse(Call<Response.ResponsePerformanceLakaLantas> call, retrofit2.Response<Response.ResponsePerformanceLakaLantas> response) {
                loadingChartLakaLantas.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listLakaLantas = response.body().getData();

                        setDataLakaLantas();
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mActivity, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mActivity);
                        Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mActivity, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mActivity);
                    Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponsePerformanceLakaLantas> call, Throwable t) {
                loadingChartLakaLantas.stop();
                t.printStackTrace();
            }
        });
    }

    // Chart Patroli Barcode
    private void setDataPatroli() {
        ArrayList<BarEntry> yVals1 = new ArrayList<>();
        String[] labels = new String[listPatroli.size()];

        for (int i = 0; i < labels.length; i++) {
            yVals1.add(new BarEntry(i, listPatroli.get(i).getJumlah()));
            labels[i] = Helper.fullDateFormatDateMonth(Helper.stringToDate(listPatroli.get(i).getTgl()));
        }

        setToBarChart(mChartPatroli, yVals1, labels);
    }

    private void getPerfPatroli(String token) {
        loadingChartPatroli.start();

        ApiInterface.Factory.getInstance().performancePatroli("Bearer " + token).enqueue(new Callback<Response.ResponsePerformancePatroli>() {
            @Override
            public void onResponse(Call<Response.ResponsePerformancePatroli> call, retrofit2.Response<Response.ResponsePerformancePatroli> response) {
                loadingChartPatroli.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listPatroli = response.body().getData();

                        setDataPatroli();
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mActivity, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mActivity);
                        Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mActivity, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mActivity);
                    Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponsePerformancePatroli> call, Throwable t) {
                loadingChartPatroli.stop();
                t.printStackTrace();
            }
        });
    }

    // Formatter
    public class SimpleFormatter implements IValueFormatter, IAxisValueFormatter {

        protected DecimalFormat mFormat;
        private String satuan;

        public SimpleFormatter(String satuan) {
            this.satuan = satuan;
            mFormat = new DecimalFormat("###,###,###");
        }

        public SimpleFormatter(DecimalFormat format) {
            this.mFormat = format;
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value) + " " + satuan;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return mFormat.format(value) + " " + satuan;
        }
    }

    public class LabelFormatter implements IAxisValueFormatter {
        private final String[] mLabels;

        public LabelFormatter(String[] labels) {
            mLabels = labels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return mLabels[(int) value];
        }
    }
}
