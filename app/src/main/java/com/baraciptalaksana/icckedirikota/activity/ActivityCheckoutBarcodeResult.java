package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityCheckoutBarcodeResult extends AppCompatActivity {

    @BindView(R.id.iv_history_barcode_history)
    ImageView iv_history_barcode_history;
    private Context mContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_checkout_result);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityCheckoutBarcodeResult.this, R.color.color_patroli_barcode);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Checkout Barcode Patroli");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.iv_history_barcode_history)
    public void onGotoHistory(){
        Intent logInIntent = new Intent(ActivityCheckoutBarcodeResult.this, ActivityHistoryBarcode.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
