package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.adapter.PetugasTpsAdapter;
import com.baraciptalaksana.icckedirikota.adapter.PlottingTpsAdapter;
import com.baraciptalaksana.icckedirikota.models.Kecamatan;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Tps;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.paginate.Paginate;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPlottingTps extends AppCompatActivity{

    private AppCompatSpinner spnr_kecematan;
    RotateLoading loading;
    private Context mContext = this;
    ProgressBar progressBar;

    private List<Kecamatan> listKecamatan = new ArrayList<>();
    private int lastKecamatan = -1;

    private ArrayList<Tps> rowListItem = new ArrayList<Tps>();
    private PlottingTpsAdapter rcAdapter;
    private GridLayoutManager lLayout;


    RecyclerView recyclerview;
    PullRefreshLayout swipeRefreshLayout;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plotting_tps);
        Helper.darkenStatusBar(ActivityPlottingTps.this, R.color.colorAccentRed);
        initToolbar();
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        checkCurrentLocation();
        spnr_kecematan = (AppCompatSpinner) findViewById(R.id.spnr_kecematan);
        loading = (RotateLoading) findViewById(R.id.loading);
        progressBar = findViewById(R.id.progressBar);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        swipeRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        getListKecamatan(SharedPreferences.getToken(mContext));
        spnr_kecematan.setEnabled(false);
        spnr_kecematan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getListPetugasTps(SharedPreferences.getToken(mContext), lattitude, longitude, listKecamatan.get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new PlottingTpsAdapter(ActivityPlottingTps.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rowListItem.clear();
                        rcAdapter.notifyDataSetChanged();
                        getListPetugasTps(SharedPreferences.getToken(mContext), lattitude, longitude, spnr_kecematan.getSelectedItemPosition());
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
            }
        });

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lokasi TPS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getListPetugasTps(String token, String lat, String lon, long idKecamatan) {
        loading.start();
        ApiInterface.Factory.getInstance().listlokasitpsjarak("Bearer " + token, lat, lon, idKecamatan).enqueue(new Callback<Response.ResponseListPetugasTps>() {
            @Override
            public void onResponse(Call<Response.ResponseListPetugasTps> call, retrofit2.Response<Response.ResponseListPetugasTps> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                    } else {
                        rowListItem.clear();
                        rcAdapter.notifyDataSetChanged();
                        if (lastKecamatan >= 0)  spnr_kecematan.setSelection(lastKecamatan);

                        Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                    }
                } else {
                    rowListItem.clear();
                    rcAdapter.notifyDataSetChanged();
                    if (lastKecamatan >= 0)  spnr_kecematan.setSelection(lastKecamatan);

                    Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPetugasTps> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListKecamatan(String token){
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface.Factory.getInstance().listkecamatan("Bearer " + token).enqueue(new Callback<Response.ResponseListKecamatan>() {
            @Override
            public void onResponse(Call<Response.ResponseListKecamatan> call, retrofit2.Response<Response.ResponseListKecamatan> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listKecamatan = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < listKecamatan.size(); i++){
                            listSpinner.add(listKecamatan.get(i).getKecamatan());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_kecematan.setAdapter(adapter);
                        spnr_kecematan.setEnabled(true);
                    } else {
                        Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityPlottingTps.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityPlottingTps.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListKecamatan> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void checkCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ActivityPlottingTps.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (ActivityPlottingTps.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ActivityPlottingTps.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            }else{
                Toasty.warning(mContext, "Unble to Trace your location", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}