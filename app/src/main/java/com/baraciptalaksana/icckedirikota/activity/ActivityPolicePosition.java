package com.baraciptalaksana.icckedirikota.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Fungsi;
import com.baraciptalaksana.icckedirikota.models.OfficerLocation;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPolicePosition extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private static final String TAG = ActivityPolicePosition.class.getSimpleName();

    private GoogleMap mMap;
    private AppCompatSpinner spn_timezone;
    RotateLoading loading;
    ProgressBar progressBar;
    private Context mContext = this;
    private HashMap<String, Marker> mMarkers = new HashMap<>();
    private HashMap<String, OfficerLocation> mOL = new HashMap<>();
    private List<Fungsi> listFungsi = new ArrayList<>();
    private List<ChildEventListener> listListener = new ArrayList<>();
    private List<DatabaseReference> listReff = new ArrayList<>();
    private boolean isInit = true;
    ImageButton sidebar_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_police_position);
        Helper.darkenStatusBar(ActivityPolicePosition.this, R.color.color_posisi_anggota);
        spn_timezone = (AppCompatSpinner) findViewById(R.id.spn_timezone);
        sidebar_button = (ImageButton) findViewById(R.id.sidebar_button);
        loading = findViewById(R.id.loading);
        progressBar = findViewById(R.id.progressBar);

        getListFungsi(SharedPreferences.getToken(mContext));
        spn_timezone.setEnabled(false);
        spn_timezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mMap != null) {
                    isInit = true;
                    loginToFirebase(listFungsi.get(i).getFungsi(), i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sidebar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                showDialogPoliceInfo(""+marker.getTag());
                return false;
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("GoogleMapActivity", "onMarkerClick");
        if(marker.getTitle().equalsIgnoreCase("Police")){
            Toast.makeText(getApplicationContext(),
                    "Marker Clicked: " + marker.getTitle(), Toast.LENGTH_LONG)
                    .show();
            return true;
        }else{
            return false;
        }
    }

    private void getListFungsi(String token){
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface.Factory.getInstance().listfungsi("Bearer " + token).enqueue(new Callback<Response.ResponseListFungsi>() {
            @Override
            public void onResponse(Call<Response.ResponseListFungsi> call, retrofit2.Response<Response.ResponseListFungsi> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listFungsi = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < listFungsi.size(); i++){
                            listSpinner.add(listFungsi.get(i).getFungsi());
                            listReff.add(i, null);
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spn_timezone.setAdapter(adapter);
                        spn_timezone.setEnabled(true);
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityPolicePosition.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityPolicePosition.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityPolicePosition.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListFungsi> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void loginToFirebase(final String fungsi, final int i) {
        String email = getString(R.string.firebase_email);
        String password = getString(R.string.firebase_password);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser == null) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(
                    email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        subscribeToUpdates(fungsi, i);
                        Log.d(TAG, "firebase auth success");
                    } else {
                        Log.d(TAG, "firebase auth failed");
                    }
                }
            });
        } else {
            subscribeToUpdates(fungsi, i);
        }
    }

    private void subscribeToUpdates(String fungsi, int i) {
        loading.start();
        String path = (fungsi == null) ? getString(R.string.firebase_path) : "fungsi/"+fungsi;

        mMap.clear();
        mOL.clear();

        listReff.add(i, FirebaseDatabase.getInstance().getReference(path));

        for (DatabaseReference rf: listReff) {
            if (rf != null) {
                for (ChildEventListener ls : listListener) {
                    rf.removeEventListener(ls);
                }
            }
        }

        listListener.add(listReff.get(i).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                loading.stop();
                setMarker(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                loading.stop();
                setMarker(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                loading.stop();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                loading.stop();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                loading.stop();
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        }));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loading.stop();
            }
        }, 2500);
    }

    private void setMarker(DataSnapshot dataSnapshot) {
        String key = dataSnapshot.getKey();

        HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();

        if (!Helper.isEmptyString(""+value.get("latitude"))
                && !Helper.isEmptyString(""+value.get("longitude"))
                && !Helper.isEmptyString(key)) {

            double lat = Double.parseDouble(value.get("latitude").toString());
            double lng = Double.parseDouble(value.get("longitude").toString());
            LatLng location = new LatLng(lat, lng);

            String olName = ""+value.get("name");
            String olJabatan = ""+value.get("jabatan");
            String olLevel = ""+value.get("level");
            String olPhone = ""+value.get("phone");
            String olPhoto = ""+value.get("photo");

            if (!mOL.containsKey(key)) {
                OfficerLocation ol = new OfficerLocation();
                ol.setName(olName);
                ol.setJabatan(olJabatan);
                ol.setLevel(olLevel);
                ol.setPhone(olPhone);
                ol.setPhoto(olPhoto);
                mOL.put(key, ol);

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(Helper.getPangkatImg(olLevel)))
                        .position(location));
                marker.setTag(key);
                marker.showInfoWindow();
                mMarkers.put(key, marker);
            } else {
                mOL.get(key).setName(olName);
                mOL.get(key).setJabatan(olJabatan);
                mOL.get(key).setLevel(olLevel);
                mOL.get(key).setPhone(olPhone);
                mOL.get(key).setPhoto(olPhone);

                mMarkers.get(key).setPosition(location);
            }

            if (isInit) {

                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                for (Marker marker : mMarkers.values()) {
                    builder.include(marker.getPosition());
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location,12));
                isInit = false;
            }
        }
    }

    private void showDialogPoliceInfo(String key) {
        final String phone = mOL.get(key).getPhone();
        String photo = mOL.get(key).getPhoto();
        String level = mOL.get(key).getLevel();
        String name = mOL.get(key).getName();
        String jabatan = mOL.get(key).getJabatan();

        
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_popup_police_position);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.title)).setText(name + "\n" + jabatan + "\n" + level);

        Picasso.with(this).load(photo)
            .placeholder(R.drawable.ic_icon_police)
            .error(R.drawable.ic_icon_police)
            .into((CircleImageView) dialog.findViewById(R.id.image));

        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((ImageView) dialog.findViewById(R.id.iv_call)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(!Helper.isEmptyString(phone) || phone == null){
                    showConfirmDialogCall(phone);
                }else{
                    Toasty.warning(mContext, "No telp belum tersedia", Toast.LENGTH_LONG, true).show();
                }
            }
        });

        ((ImageView) dialog.findViewById(R.id.iv_sms)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(!Helper.isEmptyString(phone) || phone == null){
                    showConfirmDialogSms(phone);
                }else{
                    Toasty.warning(mContext, "No telp belum tersedia", Toast.LENGTH_LONG, true).show();
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void showConfirmDialogCall(final String telp) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("iCC Mobile");
        builder.setMessage("Apakah anda akan panggilan?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intentDial = new Intent(Intent.ACTION_DIAL,
                        Uri.parse("tel:"+telp));
                mContext.startActivity(intentDial);
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void showConfirmDialogSms(final String sms){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("iCC Mobile");
        builder.setMessage("Apakah anda akan sms?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("smsto:" + sms);
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.putExtra("sms_body", "");
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

}