package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.CommentarAdapter;
import com.baraciptalaksana.icckedirikota.models.Comment;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.google.common.collect.Lists;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailLaka extends AppCompatActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener, Paginate.Callbacks{

    Bundle mBundle;
    private HashMap<String,String> url_maps;
    private Context mContext = this;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.slider) SliderLayout mDemoSlider;
    @BindView(R.id.tv_content_date)
    TextView tv_content_date;

    @BindView(R.id.tv_content_time)
    TextView tv_content_time;

    @BindView(R.id.tv_content_address)
    TextView tv_content_address;

    @BindView(R.id.tv_content_category)
    TextView tv_content_category;

    @BindView(R.id.tv_content_desc)
    TextView tv_content_desc;

    @BindView(R.id.tv_comment)
    TextView tv_comment;

    @BindView(R.id.tv_seen)
    TextView tv_seen;

    @BindView(R.id.input_box)
    CardView input_box;
    @BindView(R.id.et_comment)
    EditText et_comment;
    @BindView(R.id.ll_send)
    LinearLayout ll_send;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    private CommentarAdapter rcAdapter;
    private List<Comment> rowListItem = new ArrayList<>();
    private Paginate paginate;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;

    int IdSambang = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_laka);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        Helper.darkenStatusBar(ActivityDetailLaka.this, R.color.color_laporan_laka);
        initToolbar();
        initData();

        recyclerview.setLayoutManager(new GridLayoutManager(mContext, 1));
        recyclerview.setHasFixedSize(true);
        rcAdapter = new CommentarAdapter(ActivityDetailLaka.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        paginate = Paginate.with(recyclerview, this)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(false)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .build();

        paginate.setHasMoreDataToLoad(false);

        et_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().trim().length()>0){
                    ll_send.setVisibility(View.VISIBLE);
                }else{
                    ll_send.setVisibility(View.GONE);
                }
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        input_box.setVisibility(View.GONE);
    }

    private void initData() {
        if (mBundle != null) {
            IdSambang = mBundle.getInt("Id");
            getDetailLaka(SharedPreferences.getToken(mContext), IdSambang);
            getListCommentLaka(SharedPreferences.getToken(mContext), IdSambang, offset );
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_content_address)
    public void onGotoLocation(){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:"+tv_content_address.getText().toString()));
        startActivity(intent);
    }

    @OnClick(R.id.ivb_send)
    public void onSubmitComment() {
        if (mBundle != null) {
            IdSambang = mBundle.getInt("Id");
            postComment(IdSambang,SharedPreferences.getToken(mContext));
        }
    }

    private void postComment(int id, String token) {
        Request.CommentRequestLaporan request = new Request().new CommentRequestLaporan();
        request.setId(id);
        request.setKomentar(et_comment.getText().toString());
        submitComment(request, token);
    }

    private void submitComment(final Request.CommentRequestLaporan request, String token) {
        loading.start();
        ApiInterface.Factory.getInstance().submitcommentlaka(request, "Bearer " + token).enqueue(new Callback<Response.ResponseSubmitComment>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitComment> call, retrofit2.Response<Response.ResponseSubmitComment> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        et_comment.setText("");
                        Comment comment = new Comment();
                        comment.setThumbnail(SharedPreferences.getFotoRaw(mContext));
                        comment.setNama(SharedPreferences.getNama(mContext));
                        comment.setJabatan(SharedPreferences.getJabatan(mContext));
                        comment.setKomentar(request.getKomentar());
                        comment.setCreatedAt(new Date().toString());
                        List<Comment> reverse = Lists.reverse(rowListItem);
                        reverse.add(comment);
                        rowListItem = Lists.reverse(reverse);
                        rcAdapter.notifyDataSetChanged();
                    } else {
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailLaka.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailLaka.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitComment> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });

    }

    private void getListCommentLaka(String token, int id, final int offsets){
        ApiInterface.Factory.getInstance().listcommentlaka("Bearer " + token, id, offsets).enqueue(new Callback<Response.ResponseReportCommment>() {
            @Override
            public void onResponse(Call<Response.ResponseReportCommment> call, retrofit2.Response<Response.ResponseReportCommment> response) {
                input_box.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                        if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                            Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                            SharedPreferences.logout(mContext);
                            Intent logInIntent = new Intent(ActivityDetailLaka.this, ActivityLogin.class);
                            Helper.gotoActivityWithFinish(ActivityDetailLaka.this, logInIntent, Helper.Transition.FADE);
                        }
                        paginate.setHasMoreDataToLoad(false);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailLaka.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailLaka.this, logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
            }

            @Override
            public void onFailure(Call<Response.ResponseReportCommment> call, Throwable t) {
                input_box.setVisibility(View.GONE);
                loadingmore = false;
                t.printStackTrace();
            }
        });
    }

    private void getDetailLaka(String token, int offset){
        loading.start();
        ApiInterface.Factory.getInstance().detaillaka("Bearer " + token, offset).enqueue(new Callback<Response.ResponseDetailLaka>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailLaka> call, retrofit2.Response<Response.ResponseDetailLaka> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {

                        Response.ResponseDetailLaka.Data data = response.body().getData();

                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .placeholder(R.drawable.no_image)
                                .error(R.drawable.no_image);

                        for (Response.ResponseDetailLaka.Thumbnail thumb: data.getThumbnail()) {
                            DefaultSliderView textSliderView = new DefaultSliderView(ActivityDetailLaka.this);
                            textSliderView
                                .image(Helper.imgUrl(thumb.getNama(), Constants.IMG_DIR_LAP_LAKALANTAS))
                                .setRequestOption(requestOptions)
                                .setBackgroundColor(Color.WHITE)
                                .setProgressBarVisible(true)
                                .setOnSliderClickListener(ActivityDetailLaka.this);

                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle().putString("extra", thumb.getNama());

                            mDemoSlider.addSlider(textSliderView);
                        }

                        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                        mDemoSlider.setDuration(4000);
                        mDemoSlider.addOnPageChangeListener(ActivityDetailLaka.this);
                        tv_content_date.setText(response.body().getData().getTglMasuk().toString());
                        tv_content_time.setText(response.body().getData().getWaktuMasuk().toString());
                        tv_content_address.setText(response.body().getData().getLat().toString()+","+response.body().getData().getLon().toString());
                        tv_content_category.setText(response.body().getData().getKategori().toString());
                        tv_content_desc.setText(response.body().getData().getUraian().toString());
                        tv_comment.setText(""+data.getComment());
                        tv_seen.setText(""+data.getViewer());
                    } else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityDetailLaka.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityDetailLaka.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailLaka.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailLaka.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailLaka> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onStop() {
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListCommentLaka(SharedPreferences.getToken(mContext), IdSambang, offset);
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_laka));
                loading.start();
            }
        }
    }
}
