package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;
import com.baraciptalaksana.icckedirikota.R;;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.bumptech.glide.Glide;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;
import java.io.File;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityEditProfile extends AppCompatActivity {
    @BindView(R.id.et_nama) AppCompatEditText et_nama;
    @BindView(R.id.et_email) AppCompatEditText et_email;
    @BindView(R.id.et_telp) AppCompatEditText et_telp;
    @BindView(R.id.et_password) AppCompatEditText et_password;
    @BindView(R.id.et_newpassword) AppCompatEditText et_newpassword;
    @BindView(R.id.et_confirmpassword) AppCompatEditText et_confirmpassword;
    @BindView(R.id.btn_submit) Button btn_submit;
    @BindView(R.id.fab_foto) FloatingActionButton fab_foto;
    @BindView(R.id.edit_photo) ImageView ivPhoto;
    @BindView(R.id.bottom_sheet) View bottom_sheet;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.cb_changepassword) CheckBox cb_changepassword;

    private Context mContext = this;
    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    private String mCurrentPhotoPath;
    private File upload;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityEditProfile.this, R.color.color_laporan_sambang);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        String foto = SharedPreferences.getFoto(mContext);
        if (!Helper.isEmptyString(foto))
            Picasso.with(mContext).load(foto)
                    .placeholder(R.drawable.no_image).into(ivPhoto);

        et_nama.setText(SharedPreferences.getNama(mContext));
        et_telp.setText(SharedPreferences.getTlp(mContext));

        cb_changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    et_newpassword.setVisibility(View.VISIBLE);
                    et_confirmpassword.setVisibility(View.VISIBLE);
                }else{
                    et_newpassword.setVisibility(View.GONE);
                    et_confirmpassword.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_foto)
    public void DialogFoto(){
        showBottomSheetDialog();
    }

    @OnClick(R.id.btn_submit)
    public void onLogin() {
        String oldPass = SharedPreferences.getPassword(mContext);
        String currentPass = et_password.getText().toString();
        String newPass = et_newpassword.getText().toString();
        String confirmPass = et_confirmpassword.getText().toString();

        if (checkForm()) {
            if(cb_changepassword.isChecked()){
                if(checkFormPassword()){
                    if(oldPass.matches(currentPass)){
                        if(newPass.matches(confirmPass)){
                            doUpdateProfile(SharedPreferences.getToken(mContext));
                        }else{
                            Toasty.warning(mContext, "Password baru tidak cocok dengan password konfirmasi, silahkan ulangi kembali", Toast.LENGTH_LONG, true).show();
                        }
                    }else{
                        Toasty.warning(mContext, "Password lama tidak cocok", Toast.LENGTH_LONG, true).show();
                    }
                }
            }else{
                doUpdateProfile(SharedPreferences.getToken(mContext));
            }
        }
    }

    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_list, null);


        ((View) view.findViewById(R.id.lyt_camera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openCamera(ActivityEditProfile.this,REQUEST_CODE_CAMERA);
                mBottomSheetDialog.dismiss();
            }
        });

        ((View) view.findViewById(R.id.lyt_album)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(ActivityEditProfile.this, REQUEST_CODE_GALLERY);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                for (int i = 0; i < imageFiles.size(); i++) {
                    switch (type){
                        case REQUEST_CODE_CAMERA:
                            Glide.with(ActivityEditProfile.this)
                                    .load(imageFiles.get(i).getAbsoluteFile())
                                    .into(ivPhoto);
                            mCurrentPhotoPath = imageFiles.get(i).getAbsolutePath();
                            upload = imageFiles.get(i).getAbsoluteFile();
                            break;
                        case REQUEST_CODE_GALLERY:
                            Glide.with(ActivityEditProfile.this)
                                    .load(imageFiles.get(i).getAbsoluteFile())
                                    .into(ivPhoto);
                            mCurrentPhotoPath = imageFiles.get(i).getAbsolutePath();
                            upload = imageFiles.get(i).getAbsoluteFile();
                            break;
                    }
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ActivityEditProfile.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private boolean checkForm() {
        if (et_nama.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Nama tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else if (et_email.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Email tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else if (et_telp.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Telp tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        } else if (et_password.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Password tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else {
            return true;
        }
    }

    private boolean checkFormPassword() {
        if (et_newpassword.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Masukan password baru", Toast.LENGTH_LONG, true).show();
            return false;
        }else if (et_confirmpassword.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Ulangi masukan password baru", Toast.LENGTH_LONG, true).show();
            return false;
        }else {
            return true;
        }
    }

    private void doUpdateProfile(String token) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("nrp", SharedPreferences.getNrp(mContext));
        builder.addFormDataPart("nama",et_nama.getText().toString());
        builder.addFormDataPart("telp", et_telp.getText().toString());
        builder.addFormDataPart("email", et_email.getText().toString());
        builder.addFormDataPart("password", et_newpassword.getText().toString());

        if (mCurrentPhotoPath != null) {
            if (upload != null)
                builder.addFormDataPart("foto", upload.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), upload));
        }

        MultipartBody requestBody = builder.build();
        loading.start();
        ApiInterface.Factory.getInstance().editprofile(requestBody, "Bearer " + token).enqueue(new Callback<Response.LoginResponse>() {
            @Override
            public void onResponse(Call<Response.LoginResponse> call, retrofit2.Response<Response.LoginResponse> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, "Update profil berhasil, silahkan login kembali", Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                        stopTrackerService();
                        Helper.gotoActivityWithFinish(ActivityEditProfile.this, logInIntent, Helper.Transition.FADE);
                    }else{
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.LoginResponse> call, Throwable t) {
                loading.stop();
                Toasty.info(mContext, "Gagal Koneksi", Toast.LENGTH_SHORT, true).show();
                t.printStackTrace();
            }
        });

    }

    private void stopTrackerService() {
        stopService(new Intent(ActivityEditProfile.this, TrackerService.class));
    }

}
