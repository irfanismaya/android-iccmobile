package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.baraciptalaksana.icckedirikota.adapter.PerhitunganSuaraDetailAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.DetailSuara;
import com.baraciptalaksana.icckedirikota.utils.Helper;

import java.util.ArrayList;

public class ActivityInputPerhitunganSuara extends AppCompatActivity {
    ArrayList<DetailSuara> rowListItem = new ArrayList<DetailSuara>();
    PerhitunganSuaraDetailAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext;
    RecyclerView rView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_perhitungansuara);
        initToolbar();
        Helper.darkenStatusBar(ActivityInputPerhitunganSuara.this, R.color.color_laporan_sambang);

        ArrayList<DetailSuara> rowListItem = getAllItemList();

        rView = (RecyclerView) findViewById(R.id.recyclerview);
        lLayout = new GridLayoutManager(getApplicationContext(), 1);
        rView.setLayoutManager(lLayout);
        rView.setHasFixedSize(true);
        rcAdapter = new PerhitunganSuaraDetailAdapter(ActivityInputPerhitunganSuara.this, rowListItem);
        rView.setItemAnimator(new DefaultItemAnimator());
        rView.setAdapter(rcAdapter);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Input Perhitungan Suara");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<DetailSuara> getAllItemList() {
        ArrayList<DetailSuara> allItems = new ArrayList<DetailSuara>();
//        allItems.add(new Mainmenu(R.drawable.performance, "Performance"));
//        allItems.add(new Mainmenu(R.drawable.laporan_anggota, "Posisi Anggota"));
//        allItems.add(new Mainmenu(R.drawable.jadwal_kegiatan, "Jadwal & Kegiatan"));
        return allItems;
    }

}
