package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.ScheduleAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.JadwalBulan;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Schedule;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.victor.loading.rotate.RotateLoading;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityJadwalKegiatan extends AppCompatActivity implements Paginate.Callbacks, OnMonthChangedListener, OnDateSelectedListener {
    ArrayList<Schedule> rowListItem = new ArrayList<Schedule>();
    ScheduleAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    private Paginate paginate;
    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;
    private String selectedDate = "";

    @BindView(R.id.recyclerview) RecyclerView recyclerview;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.calendarView) MaterialCalendarView mCalendarView;
    @BindView(R.id.loadingCal) RotateLoading loadingCal;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.jadwal_date_title) TextView jadwalDateTitle;
    @BindView(R.id.jadwal_date) TextView jadwalDateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_kegiatan);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityJadwalKegiatan.this, R.color.color_jadwal_kegiatan);

        lLayout = new GridLayoutManager(getApplicationContext(), 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new ScheduleAdapter(ActivityJadwalKegiatan.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(rcAdapter);

        fab.setVisibility(View.GONE);

        paginate = Paginate.with(recyclerview, this)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(false)
            .setLoadingListItemCreator(new CustomLoadingListItemCreator())
            .build();

        paginate.setHasMoreDataToLoad(false);

        initCalendarView();
        mCalendarView.setOnMonthChangedListener(this);
        mCalendarView.setOnDateChangedListener(this);

        getJadwalBln(new Date());
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Jadwal & Kegiatan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCalendarView() {
        mCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
    }

    private void getJadwalBln(Date date) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM");
        String bulan = format1.format(date);
        getListJadwalPerBulan(bulan);
    }

    private void setJadwalTitle(Date date) {
        if (date.compareTo(new Date()) == 0)
            jadwalDateTitle.setText("Hari Ini:");
        else
            jadwalDateTitle.setText("Hari:");

        jadwalDateText.setText(Helper.fullDateFormatNoDay(date));
    }

    private void getJadwal(Date date) {
        offset = 1;
        selectedDate = Helper.dateToString(date);

        getListJadwal(SharedPreferences.getToken(mContext), selectedDate, offset);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    public void onCreateJadwal() {
        Intent logInIntent = new Intent(ActivityJadwalKegiatan.this, ActivityCreateJadwal.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        getJadwalBln(date.getDate());
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        rowListItem.clear();
        rcAdapter.notifyDataSetChanged();
        mCalendarView.setDateSelected(date, !selected);
        setJadwalTitle(date.getDate());

        if (!selected) {
            getJadwal(date.getDate());
        }
    }

    private void getListJadwalPerBulan(String bulan) {
        loadingCal.start();
        ApiInterface.Factory.getInstance().listJadwalPerBulan("Bearer " + SharedPreferences.getToken(mContext), bulan).enqueue(new Callback<Response.ResponseListJadwalBulan>() {
            @Override
            public void onResponse(Call<Response.ResponseListJadwalBulan> call, retrofit2.Response<Response.ResponseListJadwalBulan> response) {
                loadingCal.stop();
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatus() != null && response.body().getStatus().equals("success")) {
                        if (response.body().getData() != null) {
                            for (JadwalBulan jb : response.body().getData()) {
                                mCalendarView.setDateSelected(Helper.stringToDate(jb.getTgl()), true);
                            }
                        }
                    }
                } else if (response.code() == 400 || response.code() == 401) {
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityJadwalKegiatan.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityJadwalKegiatan.this, logInIntent, Helper.Transition.FADE);
                } else {
                    Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListJadwalBulan> call, Throwable t) {
                loadingCal.stop();
                t.printStackTrace();
                Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
            }
        });
    }

    private void getListJadwal(String token, String tanggal, final int offsets){
        loading.start();
        ApiInterface.Factory.getInstance().listjadwal("Bearer " + token, tanggal, offsets).enqueue(new Callback<Response.ResponseListJadwal>() {
            @Override
            public void onResponse(Call<Response.ResponseListJadwal> call, retrofit2.Response<Response.ResponseListJadwal> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                        paginate.setHasMoreDataToLoad(false);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityJadwalKegiatan.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityJadwalKegiatan.this, logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
            }

            @Override
            public void onFailure(Call<Response.ResponseListJadwal> call, Throwable t) {
                loading.stop();
                loadingmore = false;
                t.printStackTrace();
            }
        });
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListJadwal(SharedPreferences.getToken(mContext), selectedDate, offset);
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_laka));
                loading.start();
            }
        }
    }
}
