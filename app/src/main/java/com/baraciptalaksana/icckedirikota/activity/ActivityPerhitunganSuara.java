package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.PerhitunganSuaraAdapter;
import com.baraciptalaksana.icckedirikota.adapter.PetugasTpsAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.CategoryReport;
import com.baraciptalaksana.icckedirikota.models.Mainmenu;
import com.baraciptalaksana.icckedirikota.models.Pemilu;
import com.baraciptalaksana.icckedirikota.models.Peserta;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Suara;
import com.baraciptalaksana.icckedirikota.models.Tps;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPerhitunganSuara extends AppCompatActivity implements Paginate.Callbacks{
    private ArrayList<Suara> rowListItem = new ArrayList<Suara>();
    private PerhitunganSuaraAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    private Paginate paginate;
    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;

    @BindView(R.id.recyclerview) RecyclerView recyclerview;
    @BindView(R.id.swipeRefreshLayout) PullRefreshLayout swipeRefreshLayout;

    @BindView(R.id.desc) LinearLayout llDesc;
    @BindView(R.id.row_paslon) LinearLayout llRowPaslon;

    AppCompatSpinner spnr_gubernur;
    ProgressBar progressBar;

    List<Pemilu> pemilus;
    Pemilu selectedPemilu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perhitungan_suara);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityPerhitunganSuara.this, R.color.color_laporan_sambang);

        spnr_gubernur = (AppCompatSpinner) findViewById(R.id.spnr_gubernur);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        getListPemilu(SharedPreferences.getToken(mContext));

        spnr_gubernur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    swipeRefreshLayout.setRefreshing(true);
                    selectedPemilu = pemilus.get(i-1);

                    rowListItem.clear();
                    rcAdapter.notifyDataSetChanged();

                    getListSuara(SharedPreferences.getToken(mContext), ""+selectedPemilu.getId(), 1);
                    getListPeserta(SharedPreferences.getToken(mContext), ""+selectedPemilu.getId());
                } else {
                    llDesc.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new PerhitunganSuaraAdapter(ActivityPerhitunganSuara.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (spnr_gubernur.getSelectedItemPosition()!=0){
                    swipeRefreshLayout.setRefreshing(true);
                    getListSuara(SharedPreferences.getToken(mContext), String.valueOf(spnr_gubernur.getSelectedItemPosition()), 1);
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    Toasty.warning(mContext, "Pilih hasil perhitungan suara", Toast.LENGTH_LONG, true).show();
                }
            }
        });

       paginate = Paginate.with(recyclerview, this)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(false)
            .setLoadingListItemCreator(new CustomLoadingListItemCreator())
            .build();

        paginate.setHasMoreDataToLoad(false);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Hasil Perhitungan Suara");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void getListPemilu(String token){
        ApiInterface.Factory.getInstance().listpemilu("Bearer " + token).enqueue(new Callback<Response.ResponseListPemilu>() {
            @Override
            public void onResponse(Call<Response.ResponseListPemilu> call, retrofit2.Response<Response.ResponseListPemilu> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        pemilus = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < pemilus.size(); i++){
                            listSpinner.add(pemilus.get(i).getPemilu());
                        }
                        listSpinner.add(0,"Pilih hasil perhitungan suara");
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_gubernur.setAdapter(adapter);
                    }else if(response.body().getMessage().equalsIgnoreCase("Kategori tidak ditemukan")){
                        Toasty.info(mContext, response.body().getMessage().toString(), Toast.LENGTH_SHORT, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPemilu> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void getListSuara(String token, String pemilu, final int offsets){
        ApiInterface.Factory.getInstance().listsuara("Bearer " + token, pemilu, offsets).enqueue(new Callback<Response.ResponseListSuara>() {
            @Override
            public void onResponse(Call<Response.ResponseListSuara> call, retrofit2.Response<Response.ResponseListSuara> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();

                        rcAdapter.setPemiluId(""+selectedPemilu.getId());
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                       if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                            Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                            SharedPreferences.logout(mContext);
                            Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                            Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                       } else {
                           if (response.body().getStatus().equalsIgnoreCase("error")
                               && response.body().getMessage() != null && offsets == 1)
                               Toasty.error(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();
                       }
                       paginate.setHasMoreDataToLoad(false);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response.ResponseListSuara> call, Throwable t) {
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void getListPeserta(String token, String pemilu) {
        ApiInterface.Factory.getInstance().listpeserta("Bearer " + token, pemilu).enqueue(new Callback<Response.ResponseListPeserta>() {
            @Override
            public void onResponse(Call<Response.ResponseListPeserta> call, retrofit2.Response<Response.ResponseListPeserta> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        llDesc.setVisibility(View.VISIBLE);
                        llRowPaslon.removeAllViews();
                        for (Peserta p: response.body().getData()) {
                            if (p.getNomorUrut() != 0 ) {
                                LinearLayout inflate = (LinearLayout) View.inflate(mContext, R.layout.row_paslon, null);
                                ImageView img = inflate.findViewById(R.id.img_paslon);
                                TextView text = inflate.findViewById(R.id.text_paslon);

                                int i = p.getNomorUrut() % Constants.PEMILU_COLORS.length;
                                img.setColorFilter(mContext.getResources().getColor(Constants.PEMILU_COLORS[i]));

                                text.setText(p.getPeserta());
                                llRowPaslon.addView(inflate);
                            }
                        }

                    } else if (response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityPerhitunganSuara.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPeserta> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListSuara(SharedPreferences.getToken(mContext),"1",offset);;
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_sambang));
                loading.start();
            }
        }
    }
}
