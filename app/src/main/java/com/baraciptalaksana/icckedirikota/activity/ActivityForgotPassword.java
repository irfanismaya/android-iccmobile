package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityForgotPassword extends AppCompatActivity {
    @BindView(R.id.et_forgotpassword_email) TextInputEditText et_forgotpassword_email;
    @BindView(R.id.btn_forgotpassword_submit) Button btn_forgotpassword_submit;
    @BindView(R.id.loading) RotateLoading loading;
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        ButterKnife.bind(this);
        initToolbar();
    }

    @OnClick(R.id.btn_forgotpassword_submit)
    public void onSubmit() {
        if(checkForm()){
            forgotpassword();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reset Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void forgotpassword() {
        Request.UserRequestForgotPassword request = new Request().new UserRequestForgotPassword();
        request.setEmail(et_forgotpassword_email.getText().toString());
        callForgotPassword(request);
    }

    private void callForgotPassword(final Request.UserRequestForgotPassword request) {
        loading.start();
        ApiInterface.Factory.getInstance().forgotpassword(request).enqueue(new Callback<Response.ResponseForgotPassword>() {
            @Override
            public void onResponse(Call<Response.ResponseForgotPassword> call, retrofit2.Response<Response.ResponseForgotPassword> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        finish();
                    } else {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }else{
                    Toasty.success(mContext, "Email tidak ditemukan", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseForgotPassword> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });

    }

    private boolean checkForm() {
        if (et_forgotpassword_email.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Silahkan masukan Email", Toast.LENGTH_LONG, true).show();
            return false;
        } else {
            return true;
        }
    }
}
