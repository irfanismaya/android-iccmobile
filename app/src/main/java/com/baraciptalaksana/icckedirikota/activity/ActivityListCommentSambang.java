package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.CommentarAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Comment;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityListCommentSambang extends AppCompatActivity implements Paginate.Callbacks{

    private ArrayList<Comment> rowListItem = new ArrayList<Comment>();
    private CommentarAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    private Paginate paginate;
    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;

    int IdLapinfo = 0;
    Bundle mBundle;

    @BindView(R.id.recyclerview) RecyclerView recyclerview;
    @BindView(R.id.ivb_send) ImageButton ivb_send;
    @BindView(R.id.ll_send)
    LinearLayout ll_send;
    @BindView(R.id.et_comment) EditText et_comment;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.swipeRefreshLayout) PullRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listcomment_sambang);
        mBundle = getIntent().getExtras();
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityListCommentSambang.this, R.color.color_laporan_sambang);
        initToolbar();
        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new CommentarAdapter(ActivityListCommentSambang.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            if (mBundle != null) {
                IdLapinfo = mBundle.getInt("Id");
                getListCommentSambang(SharedPreferences.getToken(mContext), IdLapinfo, 1 );
            }
            }
        });

        swipeRefreshLayout.setRefreshing(true);
        initData();

        paginate = Paginate.with(recyclerview, this)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .build();

        paginate.setHasMoreDataToLoad(false);

        et_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().trim().length()>0){
                    ll_send.setVisibility(View.VISIBLE);
                }else{
                    ll_send.setVisibility(View.GONE);
                }

            }
        });

        initData();
    }

    @OnClick(R.id.ivb_send)
    public void onSubmitComment() {
        if (mBundle != null) {
            IdLapinfo = mBundle.getInt("Id");
            postComment(IdLapinfo,SharedPreferences.getToken(mContext));
        }
    }

    private void initData() {
        if (mBundle != null) {
            IdLapinfo = mBundle.getInt("Id");
            getListCommentSambang(SharedPreferences.getToken(mContext), IdLapinfo, offset );
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Comment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getListCommentSambang(String token, int id, final int offsets){
        ApiInterface.Factory.getInstance().listcommentsambang("Bearer " + token, id, offsets).enqueue(new Callback<Response.ResponseReportCommment>() {
            @Override
            public void onResponse(Call<Response.ResponseReportCommment> call, retrofit2.Response<Response.ResponseReportCommment> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                        paginate.setHasMoreDataToLoad(false);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityListCommentSambang.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityListCommentSambang.this, logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response.ResponseReportCommment> call, Throwable t) {
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void postComment(int id, String token) {
        Request.CommentRequestLaporan request = new Request().new CommentRequestLaporan();
        request.setId(id);
        request.setKomentar(et_comment.getText().toString());
        submitComment(request, token);
    }

    private void submitComment(final Request.CommentRequestLaporan request, String token) {
        loading.start();
        ApiInterface.Factory.getInstance().submitcommentsambang(request, "Bearer " + token).enqueue(new Callback<Response.ResponseSubmitComment>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitComment> call, retrofit2.Response<Response.ResponseSubmitComment> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        et_comment.setText("");
                    } else {
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityListCommentSambang.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityListCommentSambang.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitComment> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });

    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListCommentSambang(SharedPreferences.getToken(mContext), IdLapinfo, offset);
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_sambang));
                loading.start();
            }
        }
    }
}
