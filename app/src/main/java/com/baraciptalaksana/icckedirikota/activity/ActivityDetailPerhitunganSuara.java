package com.baraciptalaksana.icckedirikota.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.PerhitunganSuaraDetailAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.DetailSuara;
import com.baraciptalaksana.icckedirikota.models.Peserta;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailPerhitunganSuara extends AppCompatActivity {
    private ArrayList<DetailSuara> rowListItem = new ArrayList<DetailSuara>();
    private PerhitunganSuaraDetailAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private Activity mActivity = this;

    Bundle mBundle;
    String Tps, alamat1 = "-", alamat2 = "-", pemilu, tpsId;
    int Id = 0;

    @BindView(R.id.tv_title) TextView tv_title;
    @BindView(R.id.tv_tps) TextView tv_tps;
    @BindView(R.id.tv_address1) TextView tv_address1;
    @BindView(R.id.tv_address2) TextView tv_address2;
    @BindView(R.id.tv_is_editable) TextView tv_is_editable;
    @BindView(R.id.recyclerview) RecyclerView recyclerview;
    @BindView(R.id.loading) RotateLoading loading;

    @BindView(R.id.ll_foto) LinearLayout ll_foto;
    @BindView(R.id.iv_foto) ImageView iv_foto;
    @BindView(R.id.btn_submit) Button btn_submit;

    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    private String mCurrentPhotoPath;
    private File upload;
    private boolean noEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_perhitungansuara);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailPerhitunganSuara.this, R.color.color_laporan_sambang);

        initData();
        tv_tps.setText(Tps);
        tv_address1.setText(Helper.isEmptyString(alamat1) ? "-" : alamat1);
        tv_address2.setText(Helper.isEmptyString(alamat2) ? "-" : alamat2);
        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new PerhitunganSuaraDetailAdapter(ActivityDetailPerhitunganSuara.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        recyclerview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(ActivityDetailPerhitunganSuara.this, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View view = rv.findChildViewUnder(e.getX(), e.getY());
                if (view != null && gestureDetector.onTouchEvent(e) && !noEdit) {
                    //Getting clicked value.
                    final int pos = rv.getChildAdapterPosition(view);

                    final TextView tvSuara = view.findViewById(R.id.tv_suara);

                    View inflate = LayoutInflater.from(mActivity).inflate(R.layout.dialog_input_suara, null);
                    final AppCompatEditText input = inflate.findViewById(R.id.et_suara);

                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

                    String title = "Paslon " + rcAdapter.mFilteredList.get(pos).getNomorUrut();
                    if (rcAdapter.mFilteredList.get(pos).getNomorUrut() == 0)
                        title = "Suara Tidak Sah";

                    builder.setTitle(title)
                        .setView(inflate)
                        .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final Request.HasilPemiluRequest request = new Request().new HasilPemiluRequest();
                                request.setTps(tpsId);
                                request.setPeserta("" + rcAdapter.mFilteredList.get(pos).getPesertaId());
                                request.setJumlah("" + input.getText());

                                String token = "Bearer " + SharedPreferences.getToken(mActivity);

                                submitSuara(request, token, tvSuara);
                            }
                        }).create().show();

                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        EasyImage.configuration(this)
            .setImagesFolderName("Lampiran Pemilu")
            .setCopyTakenPhotosToPublicGalleryAppFolder(false)
            .setCopyPickedImagesToPublicGalleryAppFolder(false)
            .setAllowMultiplePickInGallery(false);

        if (noEdit) {
            btn_submit.setVisibility(View.GONE);
        }
    }

    private void submitSuara(final Request.HasilPemiluRequest request, String token, final TextView tvSuara) {
        loading.start();

        ApiInterface.Factory.getInstance().submitQuickCount(request, token)
            .enqueue(new Callback<Response.ResponseBase>() {
                @Override
                public void onResponse(Call<Response.ResponseBase> call, retrofit2.Response<Response.ResponseBase> response) {
                    loading.stop();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                            Toasty.success(mActivity, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();

                            tvSuara.setText(request.getJumlah());
                        } else {
                            Toasty.info(mActivity, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        }
                    } else if (response.code() == 400 || response.code() == 401) {
                        Toasty.warning(mActivity, "Token is Expired", Toast.LENGTH_LONG,true).show();

                        SharedPreferences.logout(mActivity);
                        Intent logInIntent = new Intent(mActivity, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }

                @Override
                public void onFailure(Call<Response.ResponseBase> call, Throwable t) {
                    loading.stop();
                    t.printStackTrace();
                }
            });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Perhitungan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.iv_foto)
    public void getFOto(){
        if(!noEdit)
            EasyImage.openCamera(ActivityDetailPerhitunganSuara.this,REQUEST_CODE_CAMERA);
    }

    @OnClick(R.id.btn_submit)
    public void submitFoto() {
        if (mCurrentPhotoPath != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            if (upload != null) {
                builder.addFormDataPart("foto", upload.getName(),
                    RequestBody.create(MediaType.parse("multipart/form-data"), upload));

                builder.addFormDataPart("id_tps", "" + tpsId);

                MultipartBody requestBody = builder.build();

                String token = "Bearer " + SharedPreferences.getToken(mActivity);

                loading.start();
                ApiInterface.Factory.getInstance().submitFotoPemilu(requestBody, token).enqueue(new Callback<Response.ResponseBase>() {
                    @Override
                    public void onResponse(Call<Response.ResponseBase> call, retrofit2.Response<Response.ResponseBase> response) {
                        loading.stop();

                        if (response.isSuccessful()) {
                            if (response.body() != null
                                    && response.body().getStatus().equals("success")) {
                                Toasty.success(mContext, "Upload berhasil!").show();
                            } else if (response.body().getMessage().equalsIgnoreCase("Token is Expired")) {
                                Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_SHORT, true).show();
                                SharedPreferences.logout(mContext);
                                Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                                Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                            }
                        } else if (response.code() == 400 || response.code() == 401) {
                            Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_SHORT, true).show();
                            SharedPreferences.logout(mContext);
                            Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                            Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                        }
                    }

                    @Override
                    public void onFailure(Call<Response.ResponseBase> call, Throwable t) {
                        loading.stop();
                        t.printStackTrace();
                    }
                });
            } else {
                Toasty.error(mContext, "Foto harus dipilih!").show();
            }
        } else {
            Toasty.error(mContext, "Foto harus dipilih!").show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                for (int i = 0; i < imageFiles.size(); i++) {
                    switch (type){
                        case REQUEST_CODE_CAMERA:
                            Glide.with(ActivityDetailPerhitunganSuara.this)
                                    .load(imageFiles.get(i).getAbsoluteFile())
                                    .into(iv_foto);
                            mCurrentPhotoPath = imageFiles.get(i).getAbsolutePath();
                            upload = imageFiles.get(i).getAbsoluteFile();
                            break;
                    }
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ActivityDetailPerhitunganSuara.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void initData() {
        if (mBundle != null) {
            Id = mBundle.getInt("Id");
            Tps = mBundle.getString("Tps");
            alamat1 = mBundle.getString("alamat1");
            alamat2 = mBundle.getString("alamat2");
            tpsId = mBundle.getString("tps_id");
            pemilu = mBundle.getString("pemilu");
            noEdit = mBundle.getBoolean("no_edit", false);

            getDetailSuaraTps(SharedPreferences.getToken(mContext));
        }
    }

    private void getDetailSuaraTps(final String token) {
        loading.start();
        ApiInterface.Factory.getInstance().detailsuaratps("Bearer " + token, ""+tpsId).enqueue(new Callback<Response.ResponseDetailSuaraTps>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailSuaraTps> call, retrofit2.Response<Response.ResponseDetailSuaraTps> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        tv_title.setText(response.body().getData().get(0).getPemilu());
                        String thumbnail = Helper.imgUrl(response.body().getThumbnail(), Constants.IMG_DIR_PEMILU);
                        rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        Picasso.with(ActivityDetailPerhitunganSuara.this)
                                .load(thumbnail)
                                .placeholder(R.drawable.ic_add_foto_pemilu)
                                .error(R.drawable.ic_add_foto_pemilu)
                                .into(iv_foto);

                        ll_foto.setVisibility(View.VISIBLE);
                    }else if(response.body().getMessage().equalsIgnoreCase("Detail Suara TPS tidak ditemukan")) {
                        tv_is_editable.setVisibility(View.VISIBLE);
                        Toasty.warning(mContext, "Belum ada data jumlah suara", Toast.LENGTH_SHORT, true).show();
                        getListPeserta(token);
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_SHORT, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                    } else {
                        Toasty.warning(mContext, "Tidak dapat mengambil data, Coba lagi!", Toast.LENGTH_SHORT, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_SHORT, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailSuaraTps> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListPeserta(String token) {
        ApiInterface.Factory.getInstance().listpeserta("Bearer " + token, pemilu).enqueue(new Callback<Response.ResponseListPeserta>() {
            @Override
            public void onResponse(Call<Response.ResponseListPeserta> call, retrofit2.Response<Response.ResponseListPeserta> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItem.clear();

                        for (Peserta p: response.body().getData()) {
                            DetailSuara ds = new DetailSuara();
                            ds.setId(p.getId());
                            ds.setNomorUrut(p.getNomorUrut());
                            ds.setPeserta(p.getPeserta());
                            ds.setSuara(0);
                            ds.setTps(tpsId);
                            rowListItem.add(ds);
                        }

                        rcAdapter.notifyDataSetChanged();

                    } else if (response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityDetailPerhitunganSuara.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailPerhitunganSuara.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailPerhitunganSuara.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPeserta> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
