package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.ReportInformationAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.CategoryReport;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.utils.Tools;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.LinePagerIndicatorDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityCheckinBarcodeReport extends AppCompatActivity {

    @BindView(R.id.et_lapinfo_note) AppCompatEditText et_lapinfo_note;
    @BindView(R.id.iv_lapinfo_history) ImageView iv_lapinfo_history;
    @BindView(R.id.btn_lapinfo_camera) Button btn_lapinfo_camera;
    @BindView(R.id.btn_lapinfo_submit) Button btn_lapinfo_submit;
    @BindView(R.id.rv_lapinfo_image) RecyclerView rv_lapinfo_image;
    @BindView(R.id.iv_foto_laporan) ImageView iv_foto_laporan;
    @BindView(R.id.loading)
    RotateLoading loading;

    private Context mContext = this;
    AppCompatSpinner spnr_laporan_informasi_category;

    private static final String PHOTOS_KEY = "easy_image_photos_list";
    private ReportInformationAdapter imagesAdapter;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    LinearLayoutManager HorizontalLayout;

    private ArrayList<File> photos = new ArrayList<>();
    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_barcode_report);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityCheckinBarcodeReport.this, R.color.color_patroli_barcode);
        spnr_laporan_informasi_category = (AppCompatSpinner) findViewById(R.id.spnr_laporan_informasi_category);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_lapinfo_image.setLayoutManager(RecyclerViewLayoutManager);
        HorizontalLayout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        if (savedInstanceState != null) {
            photos = (ArrayList<File>) savedInstanceState.getSerializable(PHOTOS_KEY);
        }

        imagesAdapter = new ReportInformationAdapter(this, photos);
        rv_lapinfo_image.setLayoutManager(HorizontalLayout);
        rv_lapinfo_image.setHasFixedSize(true);
        rv_lapinfo_image.setAdapter(imagesAdapter);

        rv_lapinfo_image.setVisibility(View.GONE);
        btn_lapinfo_camera.setVisibility(View.GONE);

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rv_lapinfo_image);
        rv_lapinfo_image.addItemDecoration(new LinePagerIndicatorDecoration());

        EasyImage.configuration(this)
                .setImagesFolderName("Report Information")
                .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                .setCopyPickedImagesToPublicGalleryAppFolder(true)
                .setAllowMultiplePickInGallery(true);

        getCategoryLapinfo(SharedPreferences.getToken(mContext));
        spnr_laporan_informasi_category.setSelection(0);
        spnr_laporan_informasi_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        checkCurrentLocation();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buat Laporan Patroli");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_lapinfo_camera)
    public void onCamera() {
        EasyImage.openCamera(ActivityCheckinBarcodeReport.this, 0);
    }


    @OnClick(R.id.iv_foto_laporan)
    public void onCameraFoto() {
        EasyImage.openCamera(ActivityCheckinBarcodeReport.this, 0);
    }

    @OnClick(R.id.iv_lapinfo_history)
    public void onHistoryInformation() {
        Intent logInIntent = new Intent(ActivityCheckinBarcodeReport.this, ActivityHistoryBarcode.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @OnClick(R.id.btn_lapinfo_submit)
    public void onSubmitData() {
        if(checkForm() && photos.size() <= 5){
            if(Helper.isNetworkAvailable(mContext)){
                uploadMultiFile(SharedPreferences.getToken(mContext));
            }else {
                Toasty.warning(mContext, "Silahkan periksa kembali jaringan ada", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkForm() {
        if (photos.size() == 0) {
            Toasty.warning(mContext, "Foto tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_lapinfo_note.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Catatan tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else {
            return true;
        }
    }

    private void clearForm(){
        et_lapinfo_note.setText("");
        spnr_laporan_informasi_category.setSelection(0);
        photos.clear();
        rv_lapinfo_image.setVisibility(View.GONE);
        btn_lapinfo_camera.setVisibility(View.GONE);
        iv_foto_laporan.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PHOTOS_KEY, photos);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                onPhotosReturned(imageFiles);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ActivityCheckinBarcodeReport.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotosReturned(List<File> returnedPhotos) {
        photos.addAll(returnedPhotos);
        imagesAdapter.notifyDataSetChanged();
        rv_lapinfo_image.scrollToPosition(photos.size() - 1);
        rv_lapinfo_image.setVisibility(View.VISIBLE);
        iv_foto_laporan.setVisibility(View.GONE);
        btn_lapinfo_camera.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        EasyImage.clearConfiguration(this);
        super.onDestroy();
    }

    private void getCategoryLapinfo(String token){
        loading.start();
        ApiInterface.Factory.getInstance().categorybarcode("Bearer " + token).enqueue(new Callback<Response.ResponseCategoryAllReport>() {
            @Override
            public void onResponse(Call<Response.ResponseCategoryAllReport> call, retrofit2.Response<Response.ResponseCategoryAllReport> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        List<CategoryReport> categoryreport = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < categoryreport.size(); i++){
                            listSpinner.add(categoryreport.get(i).getKategori());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_laporan_informasi_category.setAdapter(adapter);
                    } else {
                        Toasty.info(mContext, "Data kategori informasi tidak ada", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseCategoryAllReport> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void uploadMultiFile(final String token) {
        checkCurrentLocation();
        loading.start();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("nrp", SharedPreferences.getNrp(mContext));
        builder.addFormDataPart("kategori", spnr_laporan_informasi_category.getSelectedItem().toString());
        builder.addFormDataPart("uraian", et_lapinfo_note.getText().toString());
        builder.addFormDataPart("lat", lattitude);
        builder.addFormDataPart("lon", longitude);
        builder.addFormDataPart("tgl_masuk", Tools.getTodayDate().toString());
        builder.addFormDataPart("waktu_masuk", Tools.getTodayTime().toString());

        for (int i = 0; i < photos.size(); i++) {
            File file = new File(photos.get(i).getPath());
            builder.addFormDataPart("foto[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

        MultipartBody requestBody = builder.build();
        loading.start();
        ApiInterface.Factory.getInstance().uploadMultiFilePatroliBarcode(requestBody, "Bearer " + token).enqueue(new Callback<Response.ResponsePostLaporan>() {
            @Override
            public void onResponse(Call<Response.ResponsePostLaporan> call, retrofit2.Response<Response.ResponsePostLaporan> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_SHORT, true).show();
                        clearForm();
                    }else{
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_SHORT, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityCheckinBarcodeReport.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityCheckinBarcodeReport.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponsePostLaporan> call, Throwable t) {
                loading.stop();
                Toasty.info(mContext, "Gagal Koneksi", Toast.LENGTH_SHORT, true).show();
                t.printStackTrace();
            }
        });
    }

    private void checkCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ActivityCheckinBarcodeReport.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (ActivityCheckinBarcodeReport.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ActivityCheckinBarcodeReport.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            }else{
                Toasty.warning(mContext, "Unble to Trace your location", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
