package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityCheckinBarcodeResult extends AppCompatActivity {
    @BindView(R.id.btn_buat_laporan)
    Button btn_buat_laporan;

    @BindView(R.id.btn_checkout)
    Button btn_checkout;

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.loading)
    RotateLoading loading;

    private Context mContext = this;
    private Location myLocation;

    int Id = 0;
    Bundle mBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_checkin_result);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityCheckinBarcodeResult.this, R.color.color_patroli_barcode);
        initData();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Check-in Pos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initData() {
        if (mBundle != null) {
            Id = mBundle.getInt("Id");
            getDetailObvit(SharedPreferences.getToken(mContext), Id);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_buat_laporan)
    public void onLaporanPatroliDialogis(){
        Intent logInIntent = new Intent(ActivityCheckinBarcodeResult.this, ActivityLaporanPatroliDialogis.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @OnClick(R.id.btn_checkout)
    public void onCheckout(){
        Intent logInIntent = new Intent(ActivityCheckinBarcodeResult.this, ActivityCheckoutBarcode.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

    private void getDetailObvit(String token, int id){
        loading.start();
        ApiInterface.Factory.getInstance().detailobvit("Bearer " + token, id).enqueue(new Callback<Response.ResponseDetailObvit>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailObvit> call, retrofit2.Response<Response.ResponseDetailObvit> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
//                        tv_checkin.setText("Anda telah berhasil melakukan checkin pada pkl "+response.body().getData().getCheckIn()+" di pos gatur:");
//                        tv_address.setText(response.body().getData().getAlamat().toString());
//                        tv_time.setText(response.body().getData().getDurasi().toString());
                        showMap(Double.parseDouble(response.body().getData().getLat()) ,Double.parseDouble(response.body().getData().getLon()));
                    } else if(response.body().getMessage().toString().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityCheckinBarcodeResult.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityCheckinBarcodeResult.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityCheckinBarcodeResult.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityCheckinBarcodeResult.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailObvit> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

}
