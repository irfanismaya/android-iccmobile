package com.baraciptalaksana.icckedirikota.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.icckedirikota.MainActivity;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.utils.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityPosGaturCheckoutResult extends AppCompatActivity {
    String alamat, durasi = "";
    Bundle mBundle;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_durasi)
    TextView tv_durasi;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.iv_history)
    ImageView iv_history;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posgatur_checkout_result);
        mBundle = getIntent().getExtras();
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityPosGaturCheckoutResult.this, R.color.color_checkin_postgatur);
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            alamat = mBundle.getString("alamat");
            durasi = mBundle.getString("durasi");
            tv_address.setText(alamat);
            tv_durasi.setText(durasi);
        }
    }

    @OnClick(R.id.btn_finish)
    public void onFinish() {
        Intent logInIntent = new Intent(ActivityPosGaturCheckoutResult.this, MainActivity.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Check-out Pos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.iv_history)
    public void onHistoryPosGatur() {
        Intent logInIntent = new Intent(ActivityPosGaturCheckoutResult.this, ActivityHistoryPosGatur.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
