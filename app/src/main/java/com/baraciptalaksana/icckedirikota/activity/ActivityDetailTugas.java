package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.victor.loading.rotate.RotateLoading;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityDetailTugas extends AppCompatActivity {

    Bundle mBundle;
    private Context mContext = this;
    @BindView(R.id.loading) RotateLoading loading;

    @BindView(R.id.tv_jabatan)
    TextView tv_jabatan;

    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.tv_uraian)
    TextView tv_uraian;

    int id = 0;
    String jabatan, waktu, uraian = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tugas);
        mBundle = getIntent().getExtras();
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityDetailTugas.this, R.color.color_laporan_sambang);
        initToolbar();
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            id = mBundle.getInt("id");
            jabatan = mBundle.getString("jabatan");
            waktu = mBundle.getString("waktu");
            uraian = mBundle.getString("uraian");
            tv_jabatan.setText(jabatan);
            tv_date.setText(waktu);
            tv_uraian.setText(uraian);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Perintah");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
