package com.baraciptalaksana.icckedirikota.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Kecamatan;
import com.baraciptalaksana.icckedirikota.models.Pemilu;
import com.baraciptalaksana.icckedirikota.models.Peserta;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.SuaraKapolsek;
import com.baraciptalaksana.icckedirikota.models.SummarySuaraKapolsek;
import com.baraciptalaksana.icckedirikota.models.Tps;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPlottingPerhitunganSuara extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private AppCompatSpinner spn_timezone;
    RotateLoading loading;
    private Context mContext = this;
    private Activity mActivity = this;
    ProgressBar progressBar;

    private List<SuaraKapolsek> mListMarker = new ArrayList<>();
    private List<Marker> mMarkers = new ArrayList<>();
    private List<Kecamatan> listKecamatan = new ArrayList<>();
    private int lastKecamatan = -1;
    ImageButton sidebar_button;

    List<Pemilu> pemilus;
    Pemilu selectedPemilu;

    @BindView(R.id.btn_more) ImageView btnMore;
    @BindView(R.id.detail_title) TextView tvTitle;
    @BindView(R.id.detail_kota) TextView tvKota;

    @BindView(R.id.box_more) NestedScrollView llMore;
    @BindView(R.id.detail_circle_tps) LinearLayout llCircle;
    @BindView(R.id.detail_box_tps) LinearLayout llBox;
    @BindView(R.id.detail_box_paslon) LinearLayout llPaslon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plotting_perhitungan_suara);
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityPlottingPerhitunganSuara.this, R.color.colorAccentRed);
        spn_timezone = (AppCompatSpinner) findViewById(R.id.spn_timezone);
        sidebar_button = (ImageButton) findViewById(R.id.sidebar_button);
        loading = (RotateLoading) findViewById(R.id.loading);
        progressBar = findViewById(R.id.progressBar);

        getListPemilu(SharedPreferences.getToken(mContext));
        spn_timezone.setEnabled(false);
        spn_timezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    selectedPemilu = pemilus.get(i-1);

                    if (mMap != null) {
                        getListTps(SharedPreferences.getToken(mContext), "" + selectedPemilu.getId());
                        getListPeserta(SharedPreferences.getToken(mContext), "" + selectedPemilu.getId());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sidebar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                SuaraKapolsek data = (SuaraKapolsek) marker.getTag();
                onClickMarker(data);
                return false;
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("GoogleMapActivity", "onMarkerClick");
        if(marker.getTitle().equalsIgnoreCase("Police")){
            Toast.makeText(getApplicationContext(),
                    "Marker Clicked: " + marker.getTitle(), Toast.LENGTH_LONG)
                    .show();
            return true;
        }else{
            return false;
        }
    }

    @OnClick(R.id.btn_more)
    public void showMore() {
        int vis = llMore.getVisibility();
        hideMore(vis == View.VISIBLE);
    }

    private void hideMore(boolean hide) {
        if (hide) {
            llMore.setVisibility(View.GONE);
            btnMore.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        } else {
            llMore.setVisibility(View.VISIBLE);
            btnMore.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        }
    }

    private void getListTps(String token, final String idPemilu) {
        loading.start();
        ApiInterface.Factory.getInstance().listSuaraKapolsek("Bearer " + token, idPemilu).enqueue(new Callback<Response.ResponseListSuaraKapolsek>() {
            @Override
            public void onResponse(Call<Response.ResponseListSuaraKapolsek> call, retrofit2.Response<Response.ResponseListSuaraKapolsek> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        mListMarker =  response.body().getData();
                        initMarker(mListMarker);
                    } else {
                        mMap.clear();
                        if (lastKecamatan >= 0)  spn_timezone.setSelection(lastKecamatan);

                        Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                    }
                } else {
                    mMap.clear();
                    if (lastKecamatan >= 0)  spn_timezone.setSelection(lastKecamatan);

                    Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListSuaraKapolsek> call, Throwable t) {
                mMap.clear();
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListPemilu(String token){
        ApiInterface.Factory.getInstance().listpemilu("Bearer " + token).enqueue(new Callback<Response.ResponseListPemilu>() {
            @Override
            public void onResponse(Call<Response.ResponseListPemilu> call, retrofit2.Response<Response.ResponseListPemilu> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        pemilus = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < pemilus.size(); i++){
                            listSpinner.add(pemilus.get(i).getPemilu());
                        }
                        listSpinner.add(0,"Pilih hasil perhitungan suara");
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spn_timezone.setAdapter(adapter);
                        spn_timezone.setEnabled(true);
                    }else if(response.body().getMessage().equalsIgnoreCase("Kategori tidak ditemukan")){
                        Toasty.info(mContext, response.body().getMessage().toString(), Toast.LENGTH_SHORT, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPemilu> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void getListPeserta(String token, String pemilu) {
        hideMore(true);

        ApiInterface.Factory.getInstance().listSuaraKapolsekSummary("Bearer " + token, pemilu).enqueue(new Callback<Response.ResponseSummarySuaraKapolsek>() {
            @Override
            public void onResponse(Call<Response.ResponseSummarySuaraKapolsek> call, retrofit2.Response<Response.ResponseSummarySuaraKapolsek> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        hideMore(false);

                        tvTitle.setText("Total Hasil Perhitungan Suara " + selectedPemilu.getPemilu());
                        tvKota.setText(Constants.CITY_NAME);

                        llCircle.removeAllViews();
                        llBox.removeAllViews();
                        llPaslon.removeAllViews();

                        for (SummarySuaraKapolsek p: response.body().getData()) {
                            if (p.getNomorUrut() != 0) {
                                int i = p.getNomorUrut() % Constants.PEMILU_COLORS.length;

                                // Circle
                                LinearLayout inflateCircle = (LinearLayout) View.inflate(mContext, R.layout.row_circle_tps, null);
                                ImageView circleImg = inflateCircle.findViewById(R.id.circle_img);
                                TextView circleText = inflateCircle.findViewById(R.id.circle_text);

                                Picasso.with(mActivity).load(R.drawable.ic_circle_pemilu).into(circleImg);
                                circleImg.setColorFilter(mActivity.getResources().getColor(Constants.PEMILU_COLORS[i]));

                                circleText.setText(p.getPersentaseSuara());
                                llCircle.addView(inflateCircle);

                                // Box
                                LinearLayout inflateBox = (LinearLayout) View.inflate(mContext, R.layout.row_box_tps, null);
                                View boxView = inflateBox.findViewById(R.id.box_tps);
                                TextView boxText = inflateBox.findViewById(R.id.box_tps_text);

                                String percent = p.getPersentaseTps().replace("%", "");

                                setBoxWidth(boxView, Integer.parseInt(percent));

                                boxView.setBackgroundColor(mContext.getResources().getColor(Constants.PEMILU_COLORS[i]));
                                boxText.setText(p.getTps() + " TPS");
                                llBox.addView(inflateBox);

                                // Paslon
                                LinearLayout inflatePaslon = (LinearLayout) View.inflate(mContext, R.layout.row_paslon, null);
                                ImageView imgPaslon = inflatePaslon.findViewById(R.id.img_paslon);
                                TextView textPaslon = inflatePaslon.findViewById(R.id.text_paslon);

                                imgPaslon.setColorFilter(mContext.getResources().getColor(Constants.PEMILU_COLORS[i]));
                                textPaslon.setText(p.getPeserta());
                                llPaslon.addView(inflatePaslon);
                            }
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        hideMore(true);

                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    hideMore(true);

                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSummarySuaraKapolsek> call, Throwable t) {
                hideMore(true);
                t.printStackTrace();
            }
        });
    }

    private void setBoxWidth(View view, int percentage) {
        if (percentage > 100) percentage = 100;

        float weight = (float) (percentage / 100d) * 0.9f;

        LinearLayout.LayoutParams px = new LinearLayout.LayoutParams(0, 30, weight);
        px.gravity = Gravity.CENTER;
        view.setLayoutParams(px);
    }

    private void initMarker(List<SuaraKapolsek> listData) {
        mMap.clear();

        for (int i=0; i<listData.size(); i++){
            if (listData.get(i).getNomorUrut() != null) {
                if (!Helper.isEmptyString(listData.get(i).getLat()) && !Helper.isEmptyString(listData.get(i).getLon())) {
                    int x = listData.get(i).getNomorUrut() % Constants.PEMILU_COLORS.length;

                    Bitmap icon = BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_circle_pemilu);

                    Bitmap bitmap = Helper.tintImage(icon, mContext.getResources().getColor(Constants.PEMILU_COLORS[x]));

                    Bitmap iconBitmap = Helper.getScaledDownBitmap(bitmap, 40, true);

                    LatLng location = new LatLng(Double.parseDouble(listData.get(i).getLat()), Double.parseDouble(listData.get(i).getLon()));
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromBitmap(iconBitmap))
                            .position(location).title(listData.get(i).getTps()));
                    marker.setTag(listData.get(i));
                    mMarkers.add(marker);
                }
            }
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < mMarkers.size(); i++) {
            builder.include(mMarkers.get(i).getPosition());
        }

        if (!mMarkers.isEmpty())
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 300));
    }

    private void onClickMarker(SuaraKapolsek sk) {
        Intent intent = new Intent(mContext, ActivityDetailPerhitunganSuara.class);
        intent.putExtra("Id", sk.getId());
        intent.putExtra("Tps", sk.getTps());
        intent.putExtra("Alamat", sk.getAlamat());
        intent.putExtra("tps_id", ""+sk.getId());
        intent.putExtra("pemilu", selectedPemilu.getId());
        intent.putExtra("alamat1", sk.getAlamat());
        intent.putExtra("alamat2", "Kec. " + sk.getKecamatan() +", Kel. " + sk.getKelurahan());
        intent.putExtra("no_edit", true);
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

}