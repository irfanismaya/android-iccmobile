package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.ReportInformationAdapter;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.CategoryReport;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.utils.Tools;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.LinePagerIndicatorDecoration;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityProblemSolving extends AppCompatActivity {
    @BindView(R.id.rv_problemsolve_image) RecyclerView rv_problemsolve_image;

    @BindView(R.id.btn_problemsolve_camera) Button btn_problemsolve_camera;
    @BindView(R.id.btn_problemsolve_submit) Button btn_problemsolve_submit;
    @BindView(R.id.spnr_problemsolve_category) AppCompatSpinner spnr_problemsolve_category;

    @BindView(R.id.et_problemsolve_date_perkara) AppCompatEditText et_problemsolve_date_perkara;
    @BindView(R.id.et_problemsolve_time_perkara) AppCompatEditText et_problemsolve_time_perkara;
    @BindView(R.id.et_problemsolve_coordinat_perkara) AppCompatEditText et_problemsolve_coordinat_perkara;

    @BindView(R.id.et_problemsolve_nama_pelapor) AppCompatEditText et_problemsolve_nama_pelapor;
    @BindView(R.id.et_problemsolve_alamat_pelapor) AppCompatEditText et_problemsolve_alamat_pelapor;
    @BindView(R.id.et_problemsolve_pekerjaan_pelapor) AppCompatEditText et_problemsolve_pekerjaan_pelapor;

    @BindView(R.id.et_problemsolve_nama_terlapor) AppCompatEditText et_problemsolve_nama_terlapor;
    @BindView(R.id.et_problemsolve_alamat_terlapor) AppCompatEditText et_problemsolve_alamat_terlapor;
    @BindView(R.id.et_problemsolve_pekerjaan_terlapor) AppCompatEditText et_problemsolve_pekerjaan_terlapor;

    @BindView(R.id.et_problemsolve_desc) AppCompatEditText et_problemsolve_desc;
    @BindView(R.id.et_problemsolve_uraian) AppCompatEditText et_problemsolve_uraian;
    @BindView(R.id.spnr_problemsolve_penanganan) AppCompatSpinner spnr_problemsolve_penanganan;
    @BindView(R.id.iv_history_problemsolve) ImageView iv_history_problemsolve;
    @BindView(R.id.iv_foto_laporan) ImageView iv_foto_laporan;

    @BindView(R.id.loading)
    RotateLoading loading;

    private static final String PHOTOS_KEY = "easy_image_photos_list";
    private ReportInformationAdapter imagesAdapter;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    LinearLayoutManager HorizontalLayout;
    DatePickerDialog pickerperkara;
    private Context mContext = this;

    private ArrayList<File> photos = new ArrayList<>();
    private int PLACE_PICKER_REQUEST = 1;
    private int PLACE_PICKER_REQUEST_PERKARA = 2;
    private static final int REQUEST_LOCATION = 3;
    LocationManager locationManager;
    String lattitude,longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_solving);
        initToolbar();
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityProblemSolving.this, R.color.color_problem_solving);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_problemsolve_image.setLayoutManager(RecyclerViewLayoutManager);
        HorizontalLayout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        if (savedInstanceState != null) {
            photos = (ArrayList<File>) savedInstanceState.getSerializable(PHOTOS_KEY);
        }

        imagesAdapter = new ReportInformationAdapter(this, photos);
        rv_problemsolve_image.setLayoutManager(HorizontalLayout);
        rv_problemsolve_image.setHasFixedSize(true);
        rv_problemsolve_image.setAdapter(imagesAdapter);

        rv_problemsolve_image.setVisibility(View.GONE);
        btn_problemsolve_camera.setVisibility(View.GONE);

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rv_problemsolve_image);
        rv_problemsolve_image.addItemDecoration(new LinePagerIndicatorDecoration());

        EasyImage.configuration(this)
                .setImagesFolderName("Report Information")
                .setCopyTakenPhotosToPublicGalleryAppFolder(false)
                .setCopyPickedImagesToPublicGalleryAppFolder(false)
                .setAllowMultiplePickInGallery(true);

        getCategoryProblem(SharedPreferences.getToken(mContext));
        spnr_problemsolve_category.setSelection(0);
        spnr_problemsolve_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String[] penanganan = getResources().getStringArray(R.array.status_problemsolve);
        ArrayAdapter<String> arraypenanganan = new ArrayAdapter<>(getApplicationContext(), R.layout.simple_spinner_item, penanganan);
        arraypenanganan.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spnr_problemsolve_penanganan.setAdapter(arraypenanganan);
        spnr_problemsolve_penanganan.setSelection(0);

        spnr_problemsolve_penanganan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        checkCurrentLocation();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.et_problemsolve_coordinat_perkara)
    public void onChangeLocationPerkara() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(ActivityProblemSolving.this), PLACE_PICKER_REQUEST_PERKARA);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_problemsolve_camera)
    public void onCamera() {
        EasyImage.openCamera(ActivityProblemSolving.this, 0);
    }

    @OnClick(R.id.iv_foto_laporan)
    public void onCameraFoto() {
        EasyImage.openCamera(ActivityProblemSolving.this, 0);
    }

    @OnClick(R.id.iv_history_problemsolve)
    public void onHistoryProblemSolve() {
        Intent logInIntent = new Intent(ActivityProblemSolving.this, ActivityHistoryProblemSolve.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @OnClick(R.id.et_problemsolve_date_perkara)
    public void onDatePickerPerkara() {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        pickerperkara = new DatePickerDialog(ActivityProblemSolving.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_problemsolve_date_perkara.setText(year+ "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                    }
                }, year, month, day);

        pickerperkara.show();
    }

    @OnClick(R.id.et_problemsolve_time_perkara)
    public void onTimePickerPerkara() {
        Calendar cur_calender = Calendar.getInstance();
        TimePickerDialog datePicker = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                et_problemsolve_time_perkara.setText(hourOfDay + " : " + minute);
            }
        }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.color_problem_solving));
        datePicker.show(getFragmentManager(), "Timepickerdialog");
    }

    @OnClick(R.id.btn_problemsolve_submit)
    public void onSubmitData() {
        if(checkForm() && photos.size() <= 5){
            if(Helper.isNetworkAvailable(mContext)){
                uploadMultiFile(SharedPreferences.getToken(mContext));
            }else {
                Toasty.warning(mContext, "Silahkan periksa kembali jaringan ada", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    private boolean checkForm() {
        if (photos.size() == 0) {
            Toasty.warning(mContext, "Foto tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(spnr_problemsolve_category.getSelectedItemPosition()==0) {
            Toasty.warning(mContext, "Pilih kategori laporan", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_nama_pelapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Nama pelapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_pekerjaan_pelapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Pekerjaan pelapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_alamat_pelapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Alamat pelapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_nama_terlapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Alamat terlapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_pekerjaan_terlapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Pekerjaan terlapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_alamat_terlapor.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Alamat terlapor tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_uraian.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Uraian tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(spnr_problemsolve_penanganan.getSelectedItemPosition()==0) {
            Toasty.warning(mContext, "Pilih status laporan", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_desc.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Keterangan tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_problemsolve_coordinat_perkara.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Alamat tidak boleh kosong", Toast.LENGTH_SHORT, true).show();
            return false;
        }else {
            return true;
        }
    }

    private void clearForm(){
        et_problemsolve_nama_pelapor.setText("");
        et_problemsolve_date_perkara.setText("");
        et_problemsolve_time_perkara.setText("");
        et_problemsolve_pekerjaan_pelapor.setText("");
        et_problemsolve_alamat_pelapor.setText("");
        et_problemsolve_nama_terlapor.setText("");
        et_problemsolve_pekerjaan_terlapor.setText("");
        et_problemsolve_alamat_terlapor.setText("");
        et_problemsolve_uraian.setText("");
        et_problemsolve_desc.setText("");
        et_problemsolve_coordinat_perkara.setText("");
        spnr_problemsolve_category.setSelection(0);
        spnr_problemsolve_penanganan.setSelection(0);
        photos.clear();
        rv_problemsolve_image.setVisibility(View.GONE);
        btn_problemsolve_camera.setVisibility(View.GONE);
        iv_foto_laporan.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PHOTOS_KEY, photos);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format(
                        "Place: %s \n" +
                                "Alamat: %s \n" +
                                "Latlng %s \n", place.getName(), place.getAddress(), place.getLatLng().latitude+" "+place.getLatLng().longitude);
                //et_problemsolve_coordinat.setText(toastMsg);
            }
        }

        if (requestCode == PLACE_PICKER_REQUEST_PERKARA) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format(
                        "Place: %s \n" +
                                "Alamat: %s \n" +
                                "Latlng %s \n", place.getName(), place.getAddress(), place.getLatLng().latitude+" "+place.getLatLng().longitude);
                et_problemsolve_coordinat_perkara.setText(place.getName());
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                onPhotosReturned(imageFiles);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ActivityProblemSolving.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotosReturned(List<File> returnedPhotos) {
        photos.addAll(returnedPhotos);
        imagesAdapter.notifyDataSetChanged();
        rv_problemsolve_image.scrollToPosition(photos.size() - 1);
        rv_problemsolve_image.setVisibility(View.VISIBLE);
        iv_foto_laporan.setVisibility(View.GONE);
        btn_problemsolve_camera.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        EasyImage.clearConfiguration(this);
        super.onDestroy();
    }

    private void getCategoryProblem(String token){
        loading.start();

        ApiInterface.Factory.getInstance().categoryproblem("Bearer " + token).enqueue(new Callback<Response.ResponseCategoryAllReport>() {
            @Override
            public void onResponse(Call<Response.ResponseCategoryAllReport> call, retrofit2.Response<Response.ResponseCategoryAllReport> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        List<CategoryReport> semuadosenItems = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < semuadosenItems.size(); i++){
                            listSpinner.add(semuadosenItems.get(i).getKategori());
                        }
                        listSpinner.add(0,"Pilih kategori laporan");
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_problemsolve_category.setAdapter(adapter);
                    }else if(response.body().getMessage().equalsIgnoreCase("Kategori tidak ditemukan")){
                        Toasty.info(mContext, response.body().getMessage().toString(), Toast.LENGTH_SHORT, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityProblemSolving.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityProblemSolving.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityProblemSolving.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityProblemSolving.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseCategoryAllReport> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void disableButton(){
        btn_problemsolve_submit.setEnabled(false);
        btn_problemsolve_submit.setText("Loading...");
    }

    private void enableButton(){
        btn_problemsolve_submit.setEnabled(true);
        btn_problemsolve_submit.setText("Simpan");
    }

    private void uploadMultiFile(final String token) {
        checkCurrentLocation();
        loading.start();
        disableButton();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("nrp", SharedPreferences.getNrp(mContext));
        builder.addFormDataPart("kategori", spnr_problemsolve_category.getSelectedItem().toString());
        builder.addFormDataPart("uraian", et_problemsolve_uraian.getText().toString());
        builder.addFormDataPart("lat", lattitude);
        builder.addFormDataPart("lon", longitude);
        builder.addFormDataPart("waktu_kejadian", Tools.getTodayDate()+" "+Tools.getTodayTime());
        builder.addFormDataPart("tempat_kejadian", et_problemsolve_coordinat_perkara.getText().toString());
        builder.addFormDataPart("pelapor", et_problemsolve_nama_pelapor.getText().toString());
        builder.addFormDataPart("pekerjaan_pelapor", et_problemsolve_pekerjaan_pelapor.getText().toString());
        builder.addFormDataPart("alamat_pelapor", et_problemsolve_alamat_pelapor.getText().toString());
        builder.addFormDataPart("terlapor", et_problemsolve_nama_terlapor.getText().toString());
        builder.addFormDataPart("pekerjaan_terlapor", et_problemsolve_pekerjaan_terlapor.getText().toString());
        builder.addFormDataPart("alamat_terlapor", et_problemsolve_alamat_terlapor.getText().toString());
        builder.addFormDataPart("penanganan", spnr_problemsolve_penanganan.getSelectedItem().toString());
        builder.addFormDataPart("tgl_masuk", Tools.getTodayDate().toString());
        builder.addFormDataPart("waktu_masuk", Tools.getTodayTime().toString());
        builder.addFormDataPart("keterangan", et_problemsolve_desc.getText().toString());

        for (int i = 0; i < photos.size(); i++) {
            File file = new File(photos.get(i).getPath());
            builder.addFormDataPart("foto[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

        MultipartBody requestBody = builder.build();
        ApiInterface.Factory.getInstance().uploadMultiFileProblemSolve(requestBody, "Bearer " + token).enqueue(new Callback<Response.ResponsePostLaporan>() {
            @Override
            public void onResponse(Call<Response.ResponsePostLaporan> call, retrofit2.Response<Response.ResponsePostLaporan> response) {
                loading.stop();
                enableButton();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_SHORT, true).show();
                        clearForm();
                    }else{
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_SHORT, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityProblemSolving.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityProblemSolving.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponsePostLaporan> call, Throwable t) {
                loading.stop();
                enableButton();
                Toasty.info(mContext, "Gagal Koneksi", Toast.LENGTH_SHORT, true).show();
                t.printStackTrace();
            }
        });
    }

    private void checkCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ActivityProblemSolving.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (ActivityProblemSolving.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ActivityProblemSolving.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            }else{
                Toasty.warning(mContext, "Unble to Trace your location", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

}
