package com.baraciptalaksana.icckedirikota.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Disposisi;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailSchedule extends AppCompatActivity {
    @BindView(R.id.cd_detail) CardView cdDetail;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.tv_description) TextView tvDesc;
    @BindView(R.id.tv_location) TextView tvLocation;
    @BindView(R.id.tv_peserta) TextView tvPeserta;
    @BindView(R.id.tv_note) TextView tvNotes;
    @BindView(R.id.image) ImageView image;

    @BindView(R.id.spnr_disposisi) AppCompatSpinner spDisposisi;
    @BindView(R.id.spnr_wakapolres) AppCompatSpinner spWakapolres;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    @BindView(R.id.loading) RotateLoading loading;

    Context mContext = this;
    Activity mActivity = this;

    List<Disposisi> listDisposisi = new ArrayList<>();
    List<String> listSpinner = new ArrayList<>();
    List<String> listNrp = new ArrayList<>();

    String id;
    String[] jadwalStatus;
    String disposisi = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_schedule);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailSchedule.this, R.color.color_jadwal_kegiatan);

        getWakapolres();

        id = getIntent().getStringExtra("id");

        jadwalStatus = getResources().getStringArray(R.array.jadwal_status);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Jadwal & Kegiatan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDetails(String id) {
        loading.start();
        ApiInterface.Factory.getInstance().detailJadwal("Bearer " + SharedPreferences.getToken(mContext), id).enqueue(new Callback<Response.ResponseDetailJadwal>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailJadwal> call, retrofit2.Response<Response.ResponseDetailJadwal> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatus() != null && response.body().getStatus().equals("success")) {
                        Response.ResponseDetailJadwal.Jadwal data = response.body().getData();
                        if (data != null) {
                            tvDate.setText(""+data.getWaktu());
                            tvDesc.setText(""+data.getKegiatan());
                            tvNotes.setText(""+data.getNotes());
                            tvPeserta.setText(""+data.getPeserta());
                            tvLocation.setText(""+data.getLokasi());

                            int color = R.color.color_jadwal_kegiatan_back3;
                            int icon = R.drawable.ic_jadwal_1;

                            if (data.getJenis().equalsIgnoreCase("pertemuan")) {
                                color = R.color.color_jadwal_kegiatan_back1;
                                icon = R.drawable.ic_jadwal_1;
                            }

                            if (data.getJenis().equalsIgnoreCase("kegiatan")) {
                                color = R.color.color_jadwal_kegiatan_back2;
                                icon = R.drawable.ic_jadwal_2;
                            }

                            cdDetail.setCardBackgroundColor(mContext.getResources().getColor(color));
                            Picasso.with(mContext).load(icon).placeholder(icon).into(image);

                            int index = Helper.getIndexOf(jadwalStatus, data.getStatus());
                            if (index >= 0) spDisposisi.setSelection(index);

                            int indexW = Helper.getIndexOf(listNrp, data.getDisposisi());
                            if (indexW >= 0) spWakapolres.setSelection(indexW);
                        }
                    }
                } else if (response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                } else {
                    Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailJadwal> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
                Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
            }
        });
    }

    @OnItemSelected(R.id.spnr_disposisi)
    public void disposisiSelected(int position) {
        if (position == 2) {
            spWakapolres.setEnabled(true);
        } else {
            spWakapolres.setEnabled(false);
        }
    }

    @OnItemSelected(R.id.spnr_wakapolres)
    public void wakapolresSelected(int position) {
        disposisi = listDisposisi.get(position).getNrp();
    }

    private void getWakapolres() {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface.Factory.getInstance().listDisposisi("Bearer " + SharedPreferences.getToken(mContext)).enqueue(new Callback<Response.ResponseListDisposisi>() {
            @Override
            public void onResponse(Call<Response.ResponseListDisposisi> call, retrofit2.Response<Response.ResponseListDisposisi> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatus() != null && response.body().getStatus().equals("success")) {
                        if (response.body().getData() != null) {
                            listDisposisi = response.body().getData();

                            listSpinner = new ArrayList<String>();
                            listNrp = new ArrayList<String>();
                            for (int i = 0; i < listDisposisi.size(); i++){
                                listSpinner.add(listDisposisi.get(i).getNama());
                                listNrp.add(listDisposisi.get(i).getNrp());
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                    android.R.layout.simple_spinner_item, listSpinner);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spWakapolres.setAdapter(adapter);

                            getDetails(id);
                        }
                    }
                } else if (response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                } else {
                    Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListDisposisi> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
                Toasty.error(mContext, "Tidak dapat mengambil data, coba lagi!", Toast.LENGTH_LONG, true).show();
            }
        });
    }

    @OnClick(R.id.btn_submit)
    public void updateStatus() {
        loading.start();

        Request.JadwalStatusRequest request = new Request().new JadwalStatusRequest();
        request.setId(id);
        request.setStatus(jadwalStatus[spDisposisi.getSelectedItemPosition()].toLowerCase());
        if (spDisposisi.getSelectedItemPosition() == 2) {
            request.setDisposisi(Integer.parseInt(listNrp.get(spWakapolres.getSelectedItemPosition())));
        } else {
            request.setDisposisi(null);
        }

        ApiInterface.Factory.getInstance().updateJadwal("Bearer " + SharedPreferences.getToken(mContext), request).enqueue(new Callback<Response.ResponseBase>() {
            @Override
            public void onResponse(Call<Response.ResponseBase> call, retrofit2.Response<Response.ResponseBase> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatus() != null && response.body().getStatus().equals("success")) {
                        Toasty.success(mContext,"Data berhasil diubah", Toast.LENGTH_LONG, true).show();
                    }
                } else if (response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(mContext, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(mActivity, logInIntent, Helper.Transition.FADE);
                } else {
                    Toasty.error(mContext, "Tidak dapat mengubah data, coba lagi!", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseBase> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
                Toasty.error(mContext, "Tidak dapat mengubah data, coba lagi!", Toast.LENGTH_LONG, true).show();
            }
        });
    }
}
