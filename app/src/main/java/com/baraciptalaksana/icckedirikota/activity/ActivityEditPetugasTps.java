package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityEditPetugasTps extends AppCompatActivity {
    @BindView(R.id.et_editpetugas_address) AppCompatEditText et_editpetugas_address;
    @BindView(R.id.et_editpetugas_penanggungjawab) AppCompatEditText et_editpetugas_penanggungjawab;
    @BindView(R.id.et_editpetugas_notelp) AppCompatEditText et_editpetugas_notelp;
    @BindView(R.id.et_editpetugas_latlon) AppCompatEditText et_editpetugas_latlon;
    @BindView(R.id.btn_editpetugas_submit) Button btn_editpetugas_submit;
    @BindView(R.id.et_editpetugas_kecamatan) TextView et_editpetugas_kecamatan;
    @BindView(R.id.tv_editpetugas_title) TextView tv_editpetugas_title;

    @BindView(R.id.loading) RotateLoading loading;

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    private Context mContext = this;

    private int PLACE_PICKER_REQUEST = 1;

    Bundle mBundle;
    int id = 0;

    String tps, lat, lon, nama, alamat, penanggung_jawab, tlp_penanggung_jawab, kecamatan, kelurahan ="";

    Double latvalue, longvalue = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_petugastps);
        initToolbar();
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        Helper.darkenStatusBar(ActivityEditPetugasTps.this, R.color.color_laporan_sambang);
        initData();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail data TPS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initData() {
        if (mBundle != null) {
            id = mBundle.getInt("id");
            tps = mBundle.getString("tps");
            lat = mBundle.getString("lat");
            lon = mBundle.getString("lon");
            nama = mBundle.getString("nama");
            alamat = mBundle.getString("alamat");
            kecamatan = mBundle.getString("kecamatan");
            kelurahan = mBundle.getString("kelurahan");
            penanggung_jawab = mBundle.getString("penanggung_jawab");
            tlp_penanggung_jawab = mBundle.getString("tlp_penanggung_jawab");

            tv_editpetugas_title.setText(tps);
            et_editpetugas_kecamatan.setText("Kec. "+kecamatan+", "+"Kel. "+kelurahan);
            et_editpetugas_address.setText(alamat);
            et_editpetugas_penanggungjawab.setText(penanggung_jawab);
            et_editpetugas_notelp.setText(tlp_penanggung_jawab);
            et_editpetugas_latlon.setText(lat+","+lon);

            showMap(Double.parseDouble(lat) ,Double.parseDouble(lon));
        }
    }

    @OnClick(R.id.et_editpetugas_latlon)
    public void onChangeLocationPerkara() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(ActivityEditPetugasTps.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_editpetugas_submit)
    public void onEditDataPetugas() {
        if(checkForm()){
            editDataPetugasTps(SharedPreferences.getToken(mContext));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format(
                        "Place: %s \n" +
                                "Alamat: %s \n" +
                                "Latlng %s \n", place.getName(), place.getAddress(), place.getLatLng().latitude+" "+place.getLatLng().longitude);
                et_editpetugas_latlon.setText(place.getLatLng().latitude+","+place.getLatLng().longitude);
                latvalue = place.getLatLng().latitude;
                longvalue = place.getLatLng().longitude;
                showMap(place.getLatLng().latitude, place.getLatLng().longitude);
            }
        }
    }

    private boolean checkForm() {
        if (et_editpetugas_address.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Alamat tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else if (et_editpetugas_penanggungjawab.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Penanggung jawab tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else if (et_editpetugas_notelp.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Telp tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        } else if (et_editpetugas_latlon.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Koordinat tidak boleh kosong", Toast.LENGTH_LONG, true).show();
            return false;
        }else {
            return true;
        }
    }

    private void editDataPetugasTps(String token) {
        Request.EditPetugasTpsRequest request = new Request().new EditPetugasTpsRequest();
        request.setId(id);
        request.setAlamat(et_editpetugas_address.getText().toString());
        request.setPenanggungJawab(et_editpetugas_penanggungjawab.getText().toString());
        request.setTlpPenanggungJawab(et_editpetugas_notelp.getText().toString());
        request.setLat(String.valueOf(latvalue));
        request.setLon(String.valueOf(longvalue));
        editDataPetugas(token, request);
    }

    private void editDataPetugas(String token, final Request.EditPetugasTpsRequest request) {
        loading.start();
        ApiInterface.Factory.getInstance().editpetugastps("Bearer " + token, request).enqueue(new Callback<Response.ResponseSubmitData>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitData> call, retrofit2.Response<Response.ResponseSubmitData> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        Intent logInIntentPilkada = new Intent(mContext, ActivityLokasiPetugasTps.class);
                        startActivity(logInIntentPilkada);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        finish();
                    } else {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }else{
                    Toasty.success(mContext, "Email tidak ditemukan", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitData> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });

    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

}
