package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailHumas extends AppCompatActivity {

    Bundle mBundle;
    private Context mContext = this;
    @BindView(R.id.loading) RotateLoading loading;

    @BindView(R.id.tv_content_date)
    TextView tv_content_date;

    @BindView(R.id.tv_content_title)
    TextView tv_content_title;

    @BindView(R.id.tv_content_desc)
    TextView tv_content_desc;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.iv_foto)
    ImageView iv_foto;

    int Id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_humas);
        mBundle = getIntent().getExtras();
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityDetailHumas.this, R.color.colorAccentRed);
        initToolbar();
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            Id = mBundle.getInt("Id");
            getDetailHumas(SharedPreferences.getToken(mContext), Id);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDetailHumas(String token, int id){
        loading.start();

        ApiInterface.Factory.getInstance().detailhumas("Bearer " + token, id).enqueue(new Callback<Response.ResponseDetailHumas>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailHumas> call, retrofit2.Response<Response.ResponseDetailHumas> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Picasso.with(mContext)
                            .load(Helper.imgUrl(response.body().getData().getThumbnail(), Constants.IMG_DIR_BERITA))
                            .placeholder(R.drawable.no_image)
                            .resize(300,300)
                            .centerCrop()
                            .into(iv_foto, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {

                                }
                            });
                        tv_content_date.setText(response.body().getData().getCreatedAt().toString());
                        tv_content_title.setText(response.body().getData().getJudul().toString());
                        tv_content_desc.setText(response.body().getData().getKonten().toString());
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityDetailHumas.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityDetailHumas.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailHumas.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailHumas.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailHumas> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }
}
