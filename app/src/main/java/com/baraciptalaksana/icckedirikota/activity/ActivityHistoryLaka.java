package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.LakaAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Laka;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.services.TrackerService;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityHistoryLaka extends AppCompatActivity implements Paginate.Callbacks{

    private ArrayList<Laka> rowListItem = new ArrayList<Laka>();
    private LakaAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    private Paginate paginate;
    protected int threshold = 16;
    protected boolean addLoadingRow = true;
    private boolean loadingmore = false;
    private boolean isLoadedAllItems = false;
    private int offset = 1;

    @BindView(R.id.recyclerview) RecyclerView recyclerview;
    @BindView(R.id.swipeRefreshLayout) PullRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_laka);
        ButterKnife.bind(this);
        Helper.darkenStatusBar(ActivityHistoryLaka.this, R.color.color_laporan_laka);
        initToolbar();
        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new LakaAdapter(ActivityHistoryLaka.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListHistoryLaka(SharedPreferences.getToken(mContext), 1);
            }
        });

        swipeRefreshLayout.setRefreshing(true);
        getListHistoryLaka(SharedPreferences.getToken(mContext), offset);

        paginate = Paginate.with(recyclerview, this)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .build();

        paginate.setHasMoreDataToLoad(false);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("History Laporan Sambang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getListHistoryLaka(String token, final int offsets){
        ApiInterface.Factory.getInstance().listlaka("Bearer " + token, offsets).enqueue(new Callback<Response.ResponseListLaka>() {
            @Override
            public void onResponse(Call<Response.ResponseListLaka> call, retrofit2.Response<Response.ResponseListLaka> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        if (offsets == 1) rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                        paginate.setHasMoreDataToLoad(true);
                        offset++;
                    } else {
                        if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                            Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                            stopService(new Intent(mContext, TrackerService.class));
                            SharedPreferences.logout(mContext);
                            Intent logInIntent = new Intent(ActivityHistoryLaka.this, ActivityLogin.class);
                            Helper.gotoActivityWithFinish(ActivityHistoryLaka.this, logInIntent, Helper.Transition.FADE);
                        }
                        paginate.setHasMoreDataToLoad(false);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    paginate.setHasMoreDataToLoad(false);
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityHistoryLaka.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityHistoryLaka.this, logInIntent, Helper.Transition.FADE);
                }
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response.ResponseListLaka> call, Throwable t) {
                loadingmore = false;
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onLoadMore() {
        loadingmore = true;
        if (offset > 1) getListHistoryLaka(SharedPreferences.getToken(mContext), offset);
    }

    @Override
    public boolean isLoading() {
        return loadingmore;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return isLoadedAllItems;
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.common_list_progressbar, parent, false);
            return new CustomLoadingListItemCreator.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // Bind custom loading row if needed
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RotateLoading loading;

            public ViewHolder(View view) {
                super(view);
                loading = view.findViewById(R.id.loading);
                loading.setLoadingColor(getResources().getColor(R.color.color_laporan_laka));
                loading.start();
            }
        }
    }
}
