package com.baraciptalaksana.icckedirikota.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Kecamatan;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.models.Tps;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityLokasiPlottingPam extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private AppCompatSpinner spn_timezone;
    RotateLoading loading;
    private Context mContext = this;
    ProgressBar progressBar;

    private List<Tps> mListMarker = new ArrayList<>();
    private List<Marker> mMarkers = new ArrayList<>();
    private List<Kecamatan> listKecamatan = new ArrayList<>();
    private int lastKecamatan = -1;
    ImageButton sidebar_button, ib_lokasitps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lokasi_plotingpam);
        Helper.darkenStatusBar(ActivityLokasiPlottingPam.this, R.color.colorAccentRed);
        spn_timezone = (AppCompatSpinner) findViewById(R.id.spn_timezone);
        sidebar_button = (ImageButton) findViewById(R.id.sidebar_button);
        ib_lokasitps = (ImageButton) findViewById(R.id.ib_lokasitps);
        loading = (RotateLoading) findViewById(R.id.loading);
        progressBar = findViewById(R.id.progressBar);

        getListKecamatan(SharedPreferences.getToken(mContext));
        spn_timezone.setEnabled(false);
        spn_timezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mMap != null)
                    getListPetugasTps(SharedPreferences.getToken(mContext), listKecamatan.get(i).getId(), lastKecamatan);

                lastKecamatan = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sidebar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ib_lokasitps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent logInIntent = new Intent(ActivityLokasiPlottingPam.this, ActivityPlottingTps.class);
                startActivity(logInIntent);
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                showDialogPoliceInfo(marker.getTag().toString());
                return false;
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("GoogleMapActivity", "onMarkerClick");
        if(marker.getTitle().equalsIgnoreCase("Police")){
            Toast.makeText(getApplicationContext(),
                    "Marker Clicked: " + marker.getTitle(), Toast.LENGTH_LONG)
                    .show();
            return true;
        }else{
            return false;
        }
    }

    private void showDialogPoliceInfo(String tag) {
        String typeTps = ""+tag.split("_")[0];
        String typeAnggota = ""+tag.split("_")[1];
        int posTps = Integer.parseInt(""+tag.split("_")[1]);
        final int pos = Integer.parseInt(""+tag.split("_")[1]);

        if(typeTps.equalsIgnoreCase("tps")){
            Intent logInIntent = new Intent(ActivityLokasiPlottingPam.this, ActivityDetailPlottingTps.class);
            logInIntent.putExtra("id", mListMarker.get(posTps).getId());
            startActivity(logInIntent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }else{
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_popup_police_position);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((TextView) dialog.findViewById(R.id.title)).setText(mListMarker.get(pos).getNama()+"\n" +mListMarker.get(pos).getPangkat()+ "\n" +mListMarker.get(pos).getJabatan());
            ((CircleImageView) dialog.findViewById(R.id.image)).setImageResource(R.drawable.ic_icon_police);

            ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ((ImageView) dialog.findViewById(R.id.iv_call)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+mListMarker.get(pos).getTlpPenanggungJawab()));
                    startActivity(intent);
                }
            });

            ((ImageView) dialog.findViewById(R.id.iv_sms)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent =new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("smsto:"));
                    intent.setType("vnd.android-dir/mms-sms");
                    intent.putExtra("address", new String (mListMarker.get(pos).getTlpPenanggungJawab().toCharArray()));
                    intent.putExtra("sms_body","Enter your Sms here..");

                    try
                    {
                        startActivity(intent);
                    }
                    catch(Exception e)
                    {
                        Toast.makeText(ActivityLokasiPlottingPam.this,"Sms not send",Toast.LENGTH_LONG).show();
                    }
                }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }


    }

    private void getListPetugasTps(String token, long idKecamatan, final int lastKecamatan) {
        loading.start();
        ApiInterface.Factory.getInstance().listlokasitps("Bearer " + token, idKecamatan).enqueue(new Callback<Response.ResponseListPetugasTps>() {
            @Override
            public void onResponse(Call<Response.ResponseListPetugasTps> call, retrofit2.Response<Response.ResponseListPetugasTps> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        mListMarker =  response.body().getData();
                        initMarker(mListMarker);
                    } else {
                        mMap.clear();

                        Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                    }
                } else {
                    mMap.clear();

                    Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPetugasTps> call, Throwable t) {
                loading.stop();
                mMap.clear();
                t.printStackTrace();
            }
        });
    }

    private void getListKecamatan(String token){
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface.Factory.getInstance().listkecamatan("Bearer " + token).enqueue(new Callback<Response.ResponseListKecamatan>() {
            @Override
            public void onResponse(Call<Response.ResponseListKecamatan> call, retrofit2.Response<Response.ResponseListKecamatan> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listKecamatan = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < listKecamatan.size(); i++){
                            listSpinner.add(listKecamatan.get(i).getKecamatan());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spn_timezone.setAdapter(adapter);
                        spn_timezone.setEnabled(true);
                    } else {
                        Toasty.warning(mContext, getString(R.string.text_no_data), Toast.LENGTH_LONG).show();
                    }
                }else if(response.code() == 400 || response.code() == 401) {
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityLokasiPlottingPam.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityLokasiPlottingPam.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListKecamatan> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void initMarker(List<Tps> listData) {
        mMap.clear();

        for (int i=0; i<listData.size(); i++){
            if (!Helper.isEmptyString(listData.get(i).getLat()) && !Helper.isEmptyString(listData.get(i).getLon())) {
                LatLng location = new LatLng(Double.parseDouble(listData.get(i).getLat()), Double.parseDouble(listData.get(i).getLon()));
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_tps))
                        .position(location).title(listData.get(i).getTps()));
                marker.setTag("tps_" + i);
                mMarkers.add(marker);
            }

            if (!Helper.isEmptyString(listData.get(i).getLatAnggota()) && !Helper.isEmptyString(listData.get(i).getLonAnggota())) {
                LatLng location1 = new LatLng(Double.parseDouble(listData.get(i).getLatAnggota()),
                        Double.parseDouble(listData.get(i).getLonAnggota()));
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_pol))
                        .position(location1).title(listData.get(i).getNama()));
                marker.setTag("anggota_" + i);
                mMarkers.add(marker);
            }
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < mMarkers.size(); i++) {
            builder.include(mMarkers.get(i).getPosition());
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 300));
    }

}