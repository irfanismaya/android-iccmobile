package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailPlottingTps extends AppCompatActivity {
    @BindView(R.id.btn_checkin)
    Button btn_checkin;

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.tv_edit)
    TextView tv_edit;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_address_full)
    TextView tv_address_full;

    @BindView(R.id.tv_name_officer)
    TextView tv_name_officer;

    @BindView(R.id.tv_telp)
    TextView tv_telp;

    @BindView(R.id.tv_koordinat)
    TextView tv_koordinat;

    @BindView(R.id.loading)
    RotateLoading loading;

    Bundle mBundle;
    int id = 0;
    String lat, lon ="";

    private Context mContext = this;
    private Location myLocation;
    Response.ResponseDetailPlottingTps.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_plottingtps);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailPlottingTps.this, R.color.colorAccentRed);
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            id = mBundle.getInt("id");
            getDetailPlottingTps(SharedPreferences.getToken(mContext), id);

        }
    }

    @OnClick(R.id.btn_checkin)
    public void onCheckin() {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:"+lat+","+lon));
        startActivity(intent);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail TPS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_checkin)
    public void onGotoLocation(){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:"+tv_koordinat.getText().toString()));
        startActivity(intent);
    }

    @OnClick(R.id.btn_perhitungan)
    public void onPerhitunganSuara(){
        if (data != null) {
            Intent intent = new Intent(mContext, ActivityDetailPerhitunganSuara.class);
            intent.putExtra("Id", data.getId());
            intent.putExtra("Tps", data.getTps());
            intent.putExtra("Alamat", data.getAlamat());
            intent.putExtra("tps_id", "" + data.getId());
            intent.putExtra("pemilu", data.getIdPemilu());
            intent.putExtra("alamat1", data.getAlamat());
            intent.putExtra("alamat2", "Kec. " + data.getKecamatan() + ", Kel. " + data.getKelurahan());
            intent.putExtra("no_edit", true);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

    private void getDetailPlottingTps(String token, int offset){
        loading.start();

        ApiInterface.Factory.getInstance().detailplottingtps("Bearer " + token, offset).enqueue(new Callback<Response.ResponseDetailPlottingTps>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailPlottingTps> call, retrofit2.Response<Response.ResponseDetailPlottingTps> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        data = response.body().getData();
                        tv_edit.setText(data.getTps());
                        tv_address.setText("Kec. "+ data.getKecamatan()+", "+"Kel. "+ data.getKelurahan());
                        tv_address_full.setText(data.getAlamat());
                        tv_name_officer.setText(data.getPenanggungJawab());
                        tv_telp.setText(data.getTlpPenanggungJawab());
                        tv_koordinat.setText(data.getLat()+","+ data.getLon());
                        if(!Helper.isEmptyString(data.getLon()) && !Helper.isEmptyString(data.getLon()))
                            showMap(Double.parseDouble(data.getLat()) ,Double.parseDouble(data.getLon()));
                    }else if(response.body().getMessage().equalsIgnoreCase("Kategori tidak ditemukan")){
                        Toasty.info(mContext, response.body().getMessage().toString(), Toast.LENGTH_SHORT, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityDetailPlottingTps.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityDetailPlottingTps.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailPlottingTps.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailPlottingTps.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailPlottingTps> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

}
