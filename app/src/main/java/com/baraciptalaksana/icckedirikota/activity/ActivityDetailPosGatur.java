package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityDetailPosGatur extends AppCompatActivity {
    @BindView(R.id.btn_checkin)
    Button btn_checkin;

    @BindView(R.id.btn_posgatur_petunjuk)
    Button btn_posgatur_petunjuk;

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.loading)
    RotateLoading loading;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.tv_telp)
    TextView tv_telp;

    @BindView(R.id.tv_address)
    TextView tv_address;

    private Context mContext = this;
    private Location myLocation;

    Bundle mBundle;
    int Id = 0;
    String lat, lon, alamat, pimpinan, tlp ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_posgatur);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailPosGatur.this, R.color.color_checkin_postgatur);
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            Id = mBundle.getInt("Id");
            lat = mBundle.getString("lat");
            lon = mBundle.getString("lon");
            alamat = mBundle.getString("alamat");
            pimpinan = mBundle.getString("pimpinan");
            tlp = mBundle.getString("tlp");
            tv_address.setText(alamat);
            tv_name.setText(pimpinan);
            tv_telp.setText(tlp);
            showMap(Double.parseDouble(lat) ,Double.parseDouble(lon));
        }
    }

    @OnClick(R.id.btn_checkin)
    public void onCheckin() {
        Intent logInIntent = new Intent(ActivityDetailPosGatur.this, ActivityPosgaturCheckinBarcode.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }

    @OnClick(R.id.btn_posgatur_petunjuk)
    public void onDirection() {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
        Uri.parse("geo:"+lat+","+lon));
        startActivity(intent);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Pos Gatur");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

    private void getDetailPostGatur(String token, int id){
        loading.start();
        ApiInterface.Factory.getInstance().detailpostgatur("Bearer " + token, id).enqueue(new Callback<Response.ResponseDetailPosGatur>() {
            @Override
            public void onResponse(Call<Response.ResponseDetailPosGatur> call, retrofit2.Response<Response.ResponseDetailPosGatur> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        tv_name.setText(response.body().getData().getPimpinan().toString());
                        tv_telp.setText(response.body().getData().getTlp().toString());

                    } else {
                        Toast.makeText(getApplicationContext(), "FAILED", Toast.LENGTH_LONG).show();
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityDetailPosGatur.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityDetailPosGatur.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseDetailPosGatur> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

}
