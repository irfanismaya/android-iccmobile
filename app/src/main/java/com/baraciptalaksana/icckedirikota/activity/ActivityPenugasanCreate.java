package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.ReportInformationAdapter;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.CategoryReport;
import com.baraciptalaksana.icckedirikota.models.Kesatuan;
import com.baraciptalaksana.icckedirikota.models.Personil;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.baraciptalaksana.icckedirikota.utils.Tools;
import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;


public class ActivityPenugasanCreate extends AppCompatActivity {

    @BindView(R.id.spnr_kesatuan) AppCompatSpinner spnr_kesatuan;
    @BindView(R.id.spnr_personil) AppCompatSpinner spnr_personil;
    @BindView(R.id.et_note) AppCompatEditText et_note;
    @BindView(R.id.et_ket) AppCompatEditText et_ket;
    @BindView(R.id.btn_submit) Button btn_submit;
    @BindView(R.id.loading) RotateLoading loading;
    private List<Kesatuan> listKesatuan = new ArrayList<>();
    private List<Personil> listPersonil = new ArrayList<>();
    List<Personil> listSpinner = new ArrayList<Personil>();

    String nrpvalue = "";
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penugasan_create);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityPenugasanCreate.this, R.color.color_laporan_sambang);

        getListPersonil(SharedPreferences.getToken(mContext));
        spnr_personil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                nrpvalue = listSpinner.get(spnr_personil.getSelectedItemPosition()).getNrp();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getListKesatuan(SharedPreferences.getToken(mContext));
        spnr_kesatuan.setSelection(0);
        spnr_kesatuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buat Penugasan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitData() {
        if(checkForm()){
            postPenugasan(SharedPreferences.getToken(mContext));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkForm() {
        if(spnr_personil.getSelectedItemPosition()==0) {
            Toasty.warning(mContext, "Pilih personil", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(spnr_kesatuan.getSelectedItemPosition()==0) {
            Toasty.warning(mContext, "Pilih kesatuan", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_note.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Uraian harus diisi", Toast.LENGTH_SHORT, true).show();
            return false;
        }else if(et_ket.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Keterangan harus diisi", Toast.LENGTH_SHORT, true).show();
            return false;
        }else {
            return true;
        }


    }

    private void clearForm(){
        et_note.setText("");
        spnr_kesatuan.setSelection(0);
        spnr_personil.setSelection(0);
    }

    private void getListPersonil(String token){
        loading.start();
        ApiInterface.Factory.getInstance().listpersonil("Bearer " + token).enqueue(new Callback<Response.ResponseListPersonil>() {
            @Override
            public void onResponse(Call<Response.ResponseListPersonil> call, retrofit2.Response<Response.ResponseListPersonil> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listPersonil = response.body().getData();
                        listSpinner = new ArrayList<Personil>();
                        for (int i = 0; i < listPersonil.size(); i++){
                            listSpinner.add(listPersonil.get(i));
                        }

                        Personil personil = new Personil();
                        personil.setNrp("0");
                        personil.setNama("Pilih anggota");
                        listSpinner.add(0, personil);

                        ArrayAdapter<Personil> adapter = new ArrayAdapter<Personil>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_personil.setAdapter(adapter);
                    } else {
                        Toasty.info(mContext, "Data kategori informasi tidak ada", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListPersonil> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void getListKesatuan(String token){
        loading.start();
        ApiInterface.Factory.getInstance().listkesatuan("Bearer " + token).enqueue(new Callback<Response.ResponseListKesatuan>() {
            @Override
            public void onResponse(Call<Response.ResponseListKesatuan> call, retrofit2.Response<Response.ResponseListKesatuan> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getData()!=null) {
                        listKesatuan = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < listKesatuan.size(); i++){
                            listSpinner.add(listKesatuan.get(i).getFungsi());
                        }
                        listSpinner.add(0,"Pilih kesatuan");

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                android.R.layout.simple_spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnr_kesatuan.setAdapter(adapter);
                    } else {
                        Toasty.info(mContext, "Data kategori informasi tidak ada", Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListKesatuan> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

    private void postPenugasan(String token) {
        Request.PenugasanRequest request = new Request().new PenugasanRequest();
        request.setNrp(SharedPreferences.getNrp(mContext));
        request.setNrpPersonil(nrpvalue);
        request.setSatuan(""+spnr_kesatuan.getSelectedItemPosition()+1);
        request.setUraian(et_note.getText().toString());
        request.setKeterangan(et_ket.getText().toString());
        submitPenugasan(request, token);
    }

    private void submitPenugasan(final Request.PenugasanRequest request, String token) {
        loading.start();
        ApiInterface.Factory.getInstance().submitpenugasan(request, "Bearer " + token).enqueue(new Callback<Response.ResponseSubmitData>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitData> call, retrofit2.Response<Response.ResponseSubmitData> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        Toasty.success(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                        clearForm();
                        finish();
                    } else {
                        Toasty.info(mContext, response.body().getMessage().toLowerCase(), Toast.LENGTH_LONG, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitData> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }

}
