package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.zxing.Result;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityCheckoutBarcode extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;

    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.tv_skip) TextView tv_skip;
    @BindView(R.id.tv_loading) TextView tv_loading;
    private Context mContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_barcode);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityCheckoutBarcode.this, R.color.color_patroli_barcode);
        ViewGroup contentFrame =  (FrameLayout) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Checkout Barcode Patroli");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_skip)
    public void onSkip(){
        Intent logInIntent = new Intent(ActivityCheckoutBarcode.this, ActivityCheckoutBarcodeResult.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void handleResult(final Result result) {
        Toast.makeText(this, "Contents = " + result.getText().toLowerCase() +
                ", Format = " + result.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ActivityCheckoutBarcode.this);
                checkoutbarcode(result.getText().toLowerCase());
            }
        }, 2000);
    }

    private void checkoutbarcode(String codebarcode) {
        Request.PatroliBarcodeRequestCheckout request = new Request().new PatroliBarcodeRequestCheckout();
        request.setNrp(SharedPreferences.getNrp(mContext));
        request.setLat("-6.8933901");
        request.setLon("107.6175652");
        request.setKode(codebarcode);
        scanbarcode(request, SharedPreferences.getToken(mContext));
    }

    private void scanbarcode(final Request.PatroliBarcodeRequestCheckout request, String token) {
        loading.start();
        ApiInterface.Factory.getInstance().checkoutbarcode(request, "Bearer " + token).enqueue(new Callback<Response.ResponseCheckoutcode>() {
            @Override
            public void onResponse(Call<Response.ResponseCheckoutcode> call, retrofit2.Response<Response.ResponseCheckoutcode> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        SharedPreferences.saveAbsence(mContext,"1");
                        Intent logInIntent = new Intent(ActivityCheckoutBarcode.this, ActivityCheckinBarcodeReport.class);
                        startActivity(logInIntent);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        finish();
                    }else {
                        Toasty.warning(mContext, response.body().getStatus().toString(), Toast.LENGTH_LONG, true).show();
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityCheckoutBarcode.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityCheckoutBarcode.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseCheckoutcode> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }
}
