package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.zxing.Result;
import com.baraciptalaksana.icckedirikota.MainActivity;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityCheckinBarcode extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude,longitude;

    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.iv_history_barcode_history) ImageView iv_history_barcode_history;
    @BindView(R.id.tv_skip) TextView tv_skip;
    @BindView(R.id.tv_loading) TextView tv_loading;
    private Context mContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_barcode);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityCheckinBarcode.this, R.color.color_patroli_barcode);
        ViewGroup contentFrame =  (FrameLayout) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        checkCurrentLocation();
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Checkin Barcode Patroli");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.iv_history_barcode_history)
    public void onGotoHistory(){
        Intent logInIntent = new Intent(ActivityCheckinBarcode.this, ActivityHistoryPatroli.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @OnClick(R.id.tv_skip)
    public void onSkip(){
        Intent logInIntent = new Intent(ActivityCheckinBarcode.this, ActivityCheckinBarcodeResult.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void handleResult(final Result result) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ActivityCheckinBarcode.this);
                checkinbarcode(result.getText().toLowerCase());
            }
        }, 2000);
    }

    private void checkinbarcode(String codebarcode) {
        Request.PatroliBarcodeRequestCheckin request = new Request().new PatroliBarcodeRequestCheckin();
        request.setNrp(SharedPreferences.getNrp(mContext));
        request.setLat(lattitude);
        request.setLon(longitude);
        request.setKode(codebarcode);
        scanbarcode(request, SharedPreferences.getToken(mContext));
    }

    private void scanbarcode(final Request.PatroliBarcodeRequestCheckin request, String token) {
        loading.start();
        tv_loading.setVisibility(View.VISIBLE);
        ApiInterface.Factory.getInstance().checkinbarcode(request, "Bearer " + token).enqueue(new Callback<Response.ResponseSubmitData>() {
            @Override
            public void onResponse(Call<Response.ResponseSubmitData> call, retrofit2.Response<Response.ResponseSubmitData> response) {
                loading.stop();
                tv_loading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        SharedPreferences.savePosPatroliStatus(mContext,"true");
                        SharedPreferences.savePosPatroliId(mContext,String.valueOf(response.body().getId()));
                        Intent logInIntent = new Intent(ActivityCheckinBarcode.this, ActivityCheckinBarcodeResult.class);
                        logInIntent.putExtra("Id", response.body().getId());
                        startActivity(logInIntent);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                        finish();
                    }else if(response.body().getMessage().equalsIgnoreCase("Lokasi Anda terlalu jauh dari Pos.")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Kode tidak terdaftar.")){
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityCheckinBarcode.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityCheckinBarcode.this, logInIntent, Helper.Transition.FADE);
                    }
                }else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityCheckinBarcode.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityCheckinBarcode.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseSubmitData> call, Throwable t) {
                loading.stop();
                tv_loading.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void checkCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(ActivityCheckinBarcode.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (ActivityCheckinBarcode.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ActivityCheckinBarcode.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
            }else{
                Toasty.warning(mContext, "Unble to Trace your location", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    protected void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
