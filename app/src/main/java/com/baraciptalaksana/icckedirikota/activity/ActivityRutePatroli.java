package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.icckedirikota.adapter.PosGaturAdapter;
import com.baraciptalaksana.icckedirikota.adapter.RutePatroliAdapter;
import com.baoyz.widget.PullRefreshLayout;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.RutePatroli;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import com.baraciptalaksana.icckedirikota.views.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityRutePatroli extends AppCompatActivity {

    private ArrayList<RutePatroli> rowListItem = new ArrayList<RutePatroli>();
    private RutePatroliAdapter rcAdapter;
    private GridLayoutManager lLayout;
    private Context mContext = this;

    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.loading)
    RotateLoading loading;
    @BindView(R.id.swipeRefreshLayout)
    PullRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rute_patroli);
        ButterKnife.bind(this);
        initToolbar();
        Helper.darkenStatusBar(ActivityRutePatroli.this, R.color.colorAccentRed);

        String datenow = Helper.fullDateFormatNoDay(new Date());
        tv_date.setText(datenow);

        lLayout = new GridLayoutManager(mContext, 1);
        recyclerview.setLayoutManager(lLayout);
        recyclerview.setHasFixedSize(true);
        rcAdapter = new RutePatroliAdapter(ActivityRutePatroli.this, rowListItem);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new GridSpacingItemDecoration(1, 20, true));
        recyclerview.setAdapter(rcAdapter);

        swipeRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rowListItem.clear();
                        rcAdapter.notifyDataSetChanged();
                        getListRutePatroli(SharedPreferences.getToken(mContext),SharedPreferences.getNrp(mContext));
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
            }
        });
        getListRutePatroli(SharedPreferences.getToken(mContext),SharedPreferences.getNrp(mContext));

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Rute Patroli Barcode");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (rcAdapter != null) rcAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void getListRutePatroli(String token, String nrp) {
        loading.start();
        ApiInterface.Factory.getInstance().listrutepatroli("Bearer " + token, nrp).enqueue(new Callback<Response.ResponseListRutePatroli>() {
            @Override
            public void onResponse(Call<Response.ResponseListRutePatroli> call, retrofit2.Response<Response.ResponseListRutePatroli> response) {
                loading.stop();
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        rowListItem.clear();
                        rowListItem.addAll(response.body().getData());
                        rcAdapter.notifyDataSetChanged();
                    }else if(response.body().getMessage().equalsIgnoreCase("Rute Patroli tidak ditemukan")){
                        Toasty.warning(mContext, response.body().getMessage(), Toast.LENGTH_LONG, true).show();
                    }else if(response.body().getMessage().equalsIgnoreCase("Token is Expired")){
                        Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                        SharedPreferences.logout(mContext);
                        Intent logInIntent = new Intent(ActivityRutePatroli.this, ActivityLogin.class);
                        Helper.gotoActivityWithFinish(ActivityRutePatroli.this, logInIntent, Helper.Transition.FADE);
                    }
                } else if(response.code() == 400 || response.code() == 401){
                    Toasty.warning(mContext, "Token is Expired", Toast.LENGTH_LONG, true).show();
                    SharedPreferences.logout(mContext);
                    Intent logInIntent = new Intent(ActivityRutePatroli.this, ActivityLogin.class);
                    Helper.gotoActivityWithFinish(ActivityRutePatroli.this, logInIntent, Helper.Transition.FADE);
                }
            }

            @Override
            public void onFailure(Call<Response.ResponseListRutePatroli> call, Throwable t) {
                loading.stop();
                t.printStackTrace();
            }
        });
    }
}
