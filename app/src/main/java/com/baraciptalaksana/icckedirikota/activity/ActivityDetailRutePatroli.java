package com.baraciptalaksana.icckedirikota.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.baraciptalaksana.icckedirikota.R;
import com.squareup.picasso.Picasso;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.victor.loading.rotate.RotateLoading;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityDetailRutePatroli extends AppCompatActivity {

    @BindView(R.id.iv_location)
    ImageView imageLocation;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.loading)
    RotateLoading loading;

    @BindView(R.id.tv_officer)
    TextView tv_officer;

    @BindView(R.id.tv_telp)
    TextView tv_telp;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_category)
    TextView tv_category;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.btn_direction)
    Button btn_direction;

    @BindView(R.id.btn_checkin)
    Button btn_checkin;

    private Context mContext = this;
    private Location myLocation;

    Bundle mBundle;
    int Id = 0;
    String Officer, Telp, Category, Name, Address, Lat, Lon ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rutepatroli);
        ButterKnife.bind(this);
        mBundle = getIntent().getExtras();
        initToolbar();
        Helper.darkenStatusBar(ActivityDetailRutePatroli.this, R.color.colorAccentRed);
        initData();
    }

    private void initData() {
        if (mBundle != null) {
            Officer = mBundle.getString("Officer");
            Address = mBundle.getString("Address");
            Name = mBundle.getString("Name");
            Category = mBundle.getString("Category");
            Telp = mBundle.getString("Telp");
            Lat = mBundle.getString("Lat");
            Lon = mBundle.getString("Lon");

            tv_name.setText(Name);
            tv_address.setText(Address);
            tv_telp.setText(Telp);
            tv_officer.setText(Officer);
            tv_category.setText(Category);
            showMap(Double.parseDouble(Lat) ,Double.parseDouble(Lon));
        }
    }

    @OnClick(R.id.btn_direction)
    public void onDirection() {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:"+Lat+","+Lon));
        startActivity(intent);
    }

    @OnClick(R.id.btn_checkin)
    public void onCheckin() {
        Intent logInIntent = new Intent(ActivityDetailRutePatroli.this, ActivityCheckinBarcode.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Pos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showMap(Double latitude, Double longitude){
        String staticMapUrl = Constants.STATIC_MAP_BASE_URL + "center=" + latitude
                + "," + longitude + "&zoom=" + 16
                + "&size=500x300&maptype=roadmap&markers=color:red%7Clabel:%7C"
                + latitude + "," + longitude + "&key="
                + Constants.GOOGLE_API_KEY;

        Log.d("URL", staticMapUrl);
        Picasso.with(mContext).load(staticMapUrl).into(imageLocation, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }
}
