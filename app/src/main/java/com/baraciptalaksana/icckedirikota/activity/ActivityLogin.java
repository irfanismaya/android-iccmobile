package com.baraciptalaksana.icckedirikota.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.baraciptalaksana.icckedirikota.MainActivity;
import com.baraciptalaksana.icckedirikota.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.baraciptalaksana.icckedirikota.utils.ApiInterface;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;
import com.victor.loading.rotate.RotateLoading;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityLogin extends AppCompatActivity {
    private static final String TAG = "ReactiveNetwork";

    @BindView(R.id.et_login_username)
    TextInputEditText et_login_username;
    @BindView(R.id.et_login_password)
    TextInputEditText et_login_password;
    @BindView(R.id.btn_login_login)
    Button btn_login_login;
    @BindView(R.id.tv_login_forgotpassword)
    TextView tv_login_forgotpassword;

    @BindView(R.id.loading)
    RotateLoading loading;
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        requestPermission();
        checkLogin();
    }

    @OnClick(R.id.btn_login_login)
    public void onLogin() {
        if (checkForm()) {
            login();
        }
    }

    @OnClick(R.id.tv_login_forgotpassword)
    public void onForgotPassword() {
        Intent logInIntent = new Intent(ActivityLogin.this, ActivityForgotPassword.class);
        startActivity(logInIntent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void checkLogin() {
        String token = SharedPreferences.getToken(this);
        if (!Helper.isEmptyString(token)) {
            gotoMain();
        }
    }

    private void gotoMain() {
        Intent logInIntent = new Intent(ActivityLogin.this, MainActivity.class);
        Helper.gotoActivityWithFinish(this, logInIntent, Helper.Transition.FADE);
    }

    private void requestPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Toasty.success(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT, true).show();
                        }

                        if (!report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        new SweetAlertDialog(ActivityLogin.this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Need Permissions")
                .setContentText("This app needs permission to use this feature. You can grant them in app settings.")
                .setCancelText("Cancel")
                .setConfirmText("Goto Settings")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
                openSettings();
            }
        }).show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void login() {
        Request.UserRequestLogin request = new Request().new UserRequestLogin();
        request.setNrp(et_login_username.getText().toString());
        request.setPassword(et_login_password.getText().toString());
        callLogin(request);
    }

    private void callLogin(final Request.UserRequestLogin request) {
        loading.start();
        ApiInterface.Factory.getInstance().login(request).enqueue(new Callback<Response.LoginResponse>() {
            @Override
            public void onResponse(Call<Response.LoginResponse> call, retrofit2.Response<Response.LoginResponse> response) {
                loading.stop();

                if (response.isSuccessful()) {
                    if (response.body().getStatus().toString().equalsIgnoreCase("success")) {
                        SharedPreferences.saveToken(mContext, response.body().getAccessToken());
                        SharedPreferences.savePassword(mContext, et_login_password.getText().toString());
                        SharedPreferences.saveNama(mContext, response.body().getData().getNama());
                        SharedPreferences.saveNrp(mContext, response.body().getData().getNrp());
                        SharedPreferences.savePangkat(mContext, response.body().getData().getPangkat());
                        SharedPreferences.saveFungsi(mContext, response.body().getData().getFungsi());
                        SharedPreferences.saveFoto(mContext, response.body().getData().getThumbnail());
                        SharedPreferences.saveSatWil(mContext, response.body().getData().getSatuanWilayahKode());
                        SharedPreferences.saveTlp(mContext, ""+response.body().getData().getTlp());
                        SharedPreferences.saveJabatan(mContext, ""+response.body().getData().getJabatan());
                        SharedPreferences.saveJabatanLevel(mContext, ""+response.body().getData().getJabatanLevel());
                        SharedPreferences.saveSatwilText(mContext, ""+response.body().getData().getSatuanWilayah());
                        SharedPreferences.saveSatwilTextKode(mContext, ""+response.body().getData().getSatuanWilayahKode());
                        SharedPreferences.saveMarkerIcon(mContext, ""+response.body().getData().getMarkerIcon());
                        gotoMain();
                    }else{
                        Toasty.warning(mContext, response.body().getMessage().toString(), Toast.LENGTH_LONG, true).show();
                    }
                } else if(response.code() == 401) {
                    Toasty.warning(mContext, "Koneksi Error, silahkan coba kembali", Toast.LENGTH_LONG, true).show();
                }
            }

            @Override
            public void onFailure(Call<Response.LoginResponse> call, Throwable t) {
                loading.stop();
                Toasty.error(mContext, "Koneksi Error", Toast.LENGTH_LONG, true).show();
                t.printStackTrace();
            }
        });
    }

    private boolean checkForm() {
        if (et_login_username.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Silahkan masukan NRP", Toast.LENGTH_LONG, true).show();
            return false;
        } else if (et_login_password.getText().toString().isEmpty()) {
            Toasty.warning(mContext, "Silahkan masukan kata sandi", Toast.LENGTH_LONG, true).show();
            return false;
        } else {
            return true;
        }
    }
}
