package com.baraciptalaksana.icckedirikota.utils;

/**
 * Created by irfan-ismaya on 25/01/18.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;

import com.baraciptalaksana.icckedirikota.R;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

public class Helper {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

    public static void darkenStatusBar(Activity activity, int color) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(darkenColor(ContextCompat.getColor(activity, color)));
        }

    }

    private static int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 1.0f;
        return Color.HSVToColor(hsv);
    }


    public static void navigateMap(Activity activity, String lat, String lon) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                .parse("http://maps.google.com/maps?daddr=" + lat
                        + "," + lon));
        activity.startActivity(intent);
    }

    public static void navigetUrl(Activity activity, String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }

    public static String getDateFormat(String date) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("EEE, MMM d yyyy");
        Date inputDate;
        String result = "";
        try {
            inputDate = inputFormat.parse(date);
            result = outputFormat.format(inputDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getTimeFormat(String date) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm");
        Date inputDate;
        String result = "";
        try {
            inputDate = inputFormat.parse(date);
            result = outputFormat.format(inputDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isTimeBetweenTwoHours(int fromHour, int toHour, Calendar now) {
        //Start Time
        Calendar from = Calendar.getInstance();
        from.set(Calendar.HOUR_OF_DAY, fromHour);
        from.set(Calendar.MINUTE, 0);
        //Stop Time
        Calendar to = Calendar.getInstance();
        to.set(Calendar.HOUR_OF_DAY, toHour);
        to.set(Calendar.MINUTE, 0);

        if(to.before(from)) {
            if (now.after(to)) to.add(Calendar.DATE, 1);
            else from.add(Calendar.DATE, -1);
        }
        return now.after(from) && now.before(to);
    }

    public static String getMonthName(int i) {
        String[] mm = {"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Nop", "Des"};
        return mm[i];
    }

    public static String getMonthName(int i, int length) {
        return getMonthName(i).substring(0, length);
    }

    //STRING HELPER
    public static boolean isEmptyString(String input) {
        return input == null || input.equals("") || input.trim().equals("") || input.equals("null") || input.equals("0.0") || input.equals("0");
    }

    public static boolean isEmptyString(AppCompatEditText editText) {
        return editText == null || isEmptyString(""+editText.getText());
    }

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static String toCapitalize(String input) {
        return Character.toUpperCase(input.charAt(0)) + input.substring(1);
    }

    public static int getIndexOf(String [] arrs, String value) {
        return getIndexOf(Arrays.asList(arrs), value);
    }

    public static int getIndexOf(List<String> arrs, String value) {
        if (arrs == null || arrs.size() == 0 || value == null) return -1;
        else {
            String[] brrs = new String[arrs.size()];
            for (int i = 0; i < arrs.size(); i++) {
                brrs[i] = arrs.get(i).toLowerCase();
            }
            return Arrays.asList(brrs).indexOf(value);
        }
    }

    public static Date stringToDate(String sDate) {
        Date date = stringToDate(sDate, "yyyy-MM-dd");
        return date;
    }

    public static Date stringToDate(String sDate, String pattern) {
        try {
            Date parse = new SimpleDateFormat(pattern).parse(sDate);
            return parse;
        } catch (ParseException e) {
            return null;
        }
    }

    public static String dateToString(Date date) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(date);
        return format;
    }

    public static String fullDateFormat(Date date) {
        Format formatter = new SimpleDateFormat("EEEE, d MMMM yyyy", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    public static String fullDateFormatNoDay(Date date) {
        Format formatter = new SimpleDateFormat("d MMMM yyyy", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    public static String fullDateFormatDateMonth(Date date) {
        Format formatter = new SimpleDateFormat("d MMMM", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    //END OF STRING HELPER

    //ACTIVITY HELPER
    public enum Transition {
        FADE
    };

    public static void gotoActivity(Activity activity, Intent intent, Transition transition) {
        // Set Transition
        int enterAnim = -1, exitAnim = -1;
        switch (transition) {
            case FADE:
                enterAnim = R.anim.abc_fade_in;
                exitAnim = R.anim.abc_fade_out;
        }

        activity.startActivity(intent);
        if (enterAnim != -1 && exitAnim != -1)
            activity.overridePendingTransition(enterAnim, exitAnim);
    }

    public static void gotoActivity(Activity activity, Intent intent) {
        gotoActivity(activity, intent, null);
    }

    public static void gotoActivityWithFinish(Activity activity, Intent intent, Transition transition) {
        gotoActivity(activity, intent, transition);
        activity.finish();
    }

    public static void gotoActivityWithFinish(Activity activity, Intent intent) {
        gotoActivity(activity, intent, null);
        activity.finish();
    }
    //END OF ACTIVITY HELPER
    // URL HELPER
    public static String imgUrl(String foto, String folder) {
        return Constants.URL_IMAGE + folder + foto;
    }

    public static @DrawableRes int getPangkatImg(String pangkat) {
        switch (pangkat) {
            case "Binmas":
                return  R.drawable.ic_marker_binmas;
            case "Sabhara":
                return R.drawable.ic_marker_sabhara;
            case "Bareskrim":
                return R.drawable.ic_marker_bareskrim;
            case "Lantas":
                return R.drawable.ic_marker_lantas;
            default:
                return R.drawable.ic_marker_pol;
        }
    }
    // END OF URL HELPER
    // LIST HELPER
    public static LinkedHashMap<Integer, Float> sortHashMapByValues(
            HashMap<Integer, Float> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Float> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues, Collections.<Float>reverseOrder());
        Collections.sort(mapKeys, Collections.reverseOrder());

        LinkedHashMap<Integer, Float> sortedMap = new LinkedHashMap<>();

        Iterator<Float> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Float val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Float comp1 = passedMap.get(key);
                Float comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
    // END OF LIST HELPER
    public static Bitmap getScaledDownBitmap(Bitmap bitmap, int threshold, boolean isNecessaryToKeepOrig){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = width;
        int newHeight = height;

        if(width > height && width > threshold){
            newWidth = threshold;
            newHeight = (int)(height * (float)newWidth/width);
        }

        if(width > height && width <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if(width < height && height > threshold){
            newHeight = threshold;
            newWidth = (int)(width * (float)newHeight/height);
        }

        if(width < height && height <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if(width == height && width > threshold){
            newWidth = threshold;
            newHeight = newWidth;
        }

        if(width == height && width <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        return getResizedBitmap(bitmap, newWidth, newHeight, isNecessaryToKeepOrig);
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight, boolean isNecessaryToKeepOrig) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        if(!isNecessaryToKeepOrig){
            bm.recycle();
        }
        return resizedBitmap;
    }

    public static Bitmap tintImage(Bitmap bitmap, int color) {
        Paint paint = new Paint();
        paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        Bitmap bitmapResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapResult);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return bitmapResult;
    }
}
