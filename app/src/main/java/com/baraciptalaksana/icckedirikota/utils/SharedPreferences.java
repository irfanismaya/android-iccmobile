package com.baraciptalaksana.icckedirikota.utils;

/**
 * Created by irfan-ismaya on 25/01/18.
 */

import android.content.Context;

public class SharedPreferences {

    //TOKEN
    public static void saveToken(Context ctx, String token) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_TOKEN, token);
        editor.commit();
    }

    public static String getToken(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_TOKEN, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_TOKEN, null);
        }
    }

    //PANGKAT
    public static void savePangkat(Context ctx, String pangkat) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_PANGKAT, pangkat);
        editor.commit();
    }

    public static String getPangkat(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_PANGKAT, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_PANGKAT, null);
        }
    }

    //FUNGSI
    public static void saveFungsi(Context ctx, String fungsi) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_FUNGSI, fungsi);
        editor.commit();
    }

    public static String getFungsi(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_FUNGSI, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_FUNGSI, null);
        }
    }

    //MARKER ICON
    public static void saveMarkerIcon(Context ctx, String icon) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_MARKER_ICON, icon);
        editor.commit();
    }

    public static String getMarkerIcon(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_MARKER_ICON, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_MARKER_ICON, null);
        }
    }

    //NRP
    public static void saveNrp(Context ctx, String nrp) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_NRP, nrp);
        editor.commit();
    }

    public static String getNrp(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_NRP, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_NRP, null);
        }
    }

    //NAMA
    public static void saveNama(Context ctx, String nama) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_NAMA, nama);
        editor.commit();
    }

    public static String getNama(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_NAMA, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_NAMA, null);
        }
    }

    //SATUAN WILAYAH
    public static void saveSatWil(Context ctx, String satwil) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_SATWIL, satwil);
        editor.commit();
    }

    public static String getSatwil(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String satwil = pref.getString(Constants.SESSION_SATWIL, null);
        if (Helper.isEmptyString(satwil)) {
            return null;
        } else {
            return satwil;
        }
    }

    //PHONE
    public static void saveTlp(Context ctx, String tlp) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_PHONE, tlp);
        editor.commit();
    }

    public static String getTlp(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String tlp = pref.getString(Constants.SESSION_PHONE, null);
        if (Helper.isEmptyString(tlp)) {
            return null;
        } else {
            return tlp;
        }
    }

    //JABATAN
    public static void saveJabatan(Context ctx, String jabatan) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_JABATAN, jabatan);
        editor.commit();
    }

    public static String getJabatan(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String jabatan = pref.getString(Constants.SESSION_JABATAN, null);
        if (Helper.isEmptyString(jabatan)) {
            return null;
        } else {
            return jabatan;
        }
    }

    public static void saveJabatanLevel(Context ctx, String jabatanlevel) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_JABATAN_LEVEL, jabatanlevel);
        editor.commit();
    }

    public static String getJabatanLevel(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String jabatanlevel = pref.getString(Constants.SESSION_JABATAN_LEVEL, null);
        if (Helper.isEmptyString(jabatanlevel)) {
            return null;
        } else {
            return jabatanlevel;
        }
    }

    //SATUAN WILAYAH TEXT
    public static void saveSatwilText(Context ctx, String satwil) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_SATWIL_TEXT, satwil);
        editor.commit();
    }

    public static String getSatwilText(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String satwil = pref.getString(Constants.SESSION_SATWIL_TEXT, null);
        if (Helper.isEmptyString(satwil)) {
            return null;
        } else {
            return satwil;
        }
    }

    //KODE SATUAN WILAYAH TEXT
    public static void saveSatwilTextKode(Context ctx, String satwil) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_SATWIL_TEXT_KODE, satwil);
        editor.commit();
    }

    public static String getSatwilTextKode(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String satwilkode = pref.getString(Constants.SESSION_SATWIL_TEXT_KODE, null);
        if (Helper.isEmptyString(satwilkode)) {
            return null;
        } else {
            return satwilkode;
        }
    }

    //PICTURE
    public static void saveFoto(Context ctx, String foto) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_FOTO, foto);
        editor.commit();
    }

    public static String getFoto(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String foto = pref.getString(Constants.SESSION_FOTO, null);
        if (Helper.isEmptyString(foto)) {
            return null;
        } else {
            return Constants.URL_IMAGE + "anggota/" + foto;
        }
    }

    public static String getFotoRaw(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String foto = pref.getString(Constants.SESSION_FOTO, null);
        if (Helper.isEmptyString(foto)) {
            return null;
        } else {
            return foto;
        }
    }

    //ABSENCE
    public static void saveAbsence(Context ctx, String absence) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_ABSENCE, absence);
        editor.commit();
    }

    public static String getAbsence(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_ABSENCE, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_ABSENCE, null);
        }
    }


    //PATROLI BARCODE========================================================================
    public static void saveCode(Context ctx, String code) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_CODE, code);
        editor.commit();
    }

    public static String getCode(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_CODE, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_CODE, null);
        }
    }

    //POS GATUR
    public static void saveAbsencePosGatur(Context ctx, String absence) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_ABSENCE_POSGATUR, absence);
        editor.commit();
    }

    public static String getAbsencePosGatur(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_ABSENCE_POSGATUR, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_ABSENCE_POSGATUR, null);
        }
    }

    //POS PATROLI STATUS
    public static void savePosPatroliStatus(Context ctx, String status) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_PATROLI_STATUS, status);
        editor.commit();
    }

    public static String getPosPatroliStatus(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_PATROLI_STATUS, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_PATROLI_STATUS, null);
        }
    }

    //POS GATUR ID
    public static void savePosPatroliId(Context ctx, String id) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_PATROLI_ID, id);
        editor.commit();
    }

    public static String getPosPatroliId(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_PATROLI_ID, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_PATROLI_ID, null);
        }
    }


    //POS GATUR STATUS
    public static void savePosGaturStatus(Context ctx, String status) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_POSGATUR_STATUS, status);
        editor.commit();
    }

    public static String getPosGaturStatus(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_POSGATUR_STATUS, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_POSGATUR_STATUS, null);
        }
    }

    //POS GATUR ID
    public static void savePosGaturId(Context ctx, String id) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_POSGATUR_ID, id);
        editor.commit();
    }

    public static String getPosGaturId(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_POSGATUR_ID, null) == null) {
            return null;
        } else {
            return pref.getString(Constants.SESSION_POSGATUR_ID, null);
        }
    }

    //PASSWORD
    public static void savePassword(Context ctx, String password) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.putString(Constants.SESSION_PASSWORD, password);
        editor.commit();
    }

    public static String getPassword(Context ctx){
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        String password = pref.getString(Constants.SESSION_PASSWORD, null);
        if (Helper.isEmptyString(password)) {
            return null;
        } else {
            return password;
        }
    }

    public static boolean isLogin(Context ctx) {
        android.content.SharedPreferences pref = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE);
        if (pref.getString(Constants.SESSION_PASSWORD, null) == null) {
            return false;
        } else {
            return true;
        }
    }

    public static void clearData(Context ctx) {
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.remove(Constants.SESSION_PASSWORD);
        editor.commit();
    }

    public static void logout(Context ctx){
        android.content.SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREF_KEY,
                Context.MODE_PRIVATE).edit();
        editor.remove(Constants.SESSION_TOKEN);
        editor.remove(Constants.SESSION_NRP);
        editor.commit();
    }

}
