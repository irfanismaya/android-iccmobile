package com.baraciptalaksana.icckedirikota.utils;

import com.baraciptalaksana.icckedirikota.R;

/**
 * Created by irfan-ismaya on 25/01/18.
 */

public class Constants {

    public static final String PREF_KEY = "session";
    public static final String SESSION_TOKEN = "session-token";
    public static final String SESSION_PANGKAT = "session-pangkat";
    public static final String SESSION_FUNGSI = "session-fungsi";
    public static final String SESSION_MARKER_ICON = "session-marker-icon";
    public static final String SESSION_NRP = "session-nrp";
    public static final String SESSION_NAMA = "session-nama";
    public static final String SESSION_FOTO = "session-foto";
    public static final String SESSION_SATWIL = "session-satwil";
    public static final String SESSION_SATWIL_TEXT = "session-satwil-text";
    public static final String SESSION_SATWIL_TEXT_KODE = "session-satwil-text-kode";
    public static final String SESSION_JABATAN = "session-jabatan";
    public static final String SESSION_JABATAN_LEVEL = "session-jabatan-level";
    public static final String SESSION_PHONE = "session-phone";
    public static final String SESSION_PASSWORD = "session-password";
    public static final String SESSION_DEVICEID = "session-devideid";
    public static final String SESSION_ABSENCE = "session-absence";
    public static final String SESSION_CODE = "session-code";
    public static final String SESSION_ABSENCE_POSGATUR = "session-absence-posgatur";
    public static final String SESSION_POSGATUR_STATUS = "session-posgatur-status";
    public static final String SESSION_POSGATUR_ID = "session-posgatur-id";

    public static final String SESSION_PATROLI_STATUS = "session-patroli-status";
    public static final String SESSION_PATROLI_ID = "session-patroli-id";


    public static final String HOST = "http://icc.membara.net/api/";
    public static final String URL_IMAGE = "http://icc.membara.net/file/thumbnail/";
    public static final String CITY_NAME = "Kota Kediri";

//    public static final String HOST = "https://icc.polreskedirikota.net/api/";
//    public static final String URL_IMAGE = "https://icc.polreskedirikota.net/file/thumbnail/";
//    public static final String CITY_NAME = "Kota Kediri";
//    https://fileupload.polreskedirikota.net/file/thumbnail/

    // IMG DIR
    public static final String IMG_DIR_ANGGOTA = "anggota/";
    public static final String IMG_DIR_LAP_INFO = "laporan_informasi/";
    public static final String IMG_DIR_LAP_LAKALANTAS = "laporan_lakalantas/";
    public static final String IMG_DIR_LAP_PATROLI = "laporan_patroli/";
    public static final String IMG_DIR_LAP_SAMBANG = "laporan_sambang/";
    public static final String IMG_DIR_PROB_SOLVING = "problem_solving/";
    public static final String IMG_DIR_BERITA = "berita/";
    public static final String IMG_DIR_PEMILU = "pemilu/";

    //AUTHENTICATION
    public static final String LOGIN = HOST + "auth/anggota/login";
    public static final String FORGOTPASSWORD = HOST + "auth/anggota/forgot_password";
    public static final String EDITPROFILE = HOST + "auth/anggota/update";

    public static final String LISTMENU = HOST + "list_menu/{fungsi}";
    public static final String LISTMENUPEMILU = HOST + "list_menu_pemilu/{fungsi}";
    public static final String LISTINBOX = HOST + "list_notifikasi/{nrp}";
    public static final String LISTHOMEBERITA = HOST + "list_berita/{offset}";
    public static final String LISTBERITA = HOST + "list_berita_all/{offset}";
    public static final String DETAILBERITA = HOST + "detail_berita/{id}";


    //LAPINFO
    public static final String LAPINFO  = HOST + "submit_laporan_informasi";
    public static final String LAPPATROLI  = HOST + "submit_laporan_patroli";
    public static final String CATEGORYLAPINFO  = HOST + "laporan_informasi_kat";
    public static final String CATEGORYPATROLI  = HOST + "laporan_patroli_kat";
    public static final String LISTLAPINFO  = HOST + "history_laporan_informasi/{offset}";
    public static final String DETAILLAPINFO  = HOST + "detail_laporan_informasi/{id}";
    public static final String HOMELAPINFO  = HOST + "list_laporan_informasi/{offset}";
    public static final String LISTCOMMENTARLAPINFO  = HOST + "list_comment_laporan_informasi/{id}/{offset}";
    public static final String SUBMITCOMMENTLAPINFO  = HOST + "submit_comment_laporan_informasi";

    //LAKA
    public static final String LAKA  = HOST + "submit_laporan_lakalantas";
    public static final String LISTLAKA  = HOST + "history_laporan_lakalantas/{offset}";
    public static final String CATEGORYLAKA  = HOST + "laporan_lakalantas_kat";
    public static final String DETAILLAKA  = HOST + "detail_laporan_lakalantas/{id}";
    public static final String HOMELAKA  = HOST + "list_laporan_lakalantas/{offset}";
    public static final String LISTCOMMENTARLAKA  = HOST + "list_comment_laporan_lakalantas/{id}/{offset}";
    public static final String SUBMITCOMMENTLAKA  = HOST + "submit_comment_laporan_lakalantas";

    //SAMBANG
    public static final String SAMBANG  = HOST + "submit_laporan_sambang";
    public static final String LISTSAMBANG  = HOST + "history_laporan_sambang/{offset}";
    public static final String CATEGORYSAMBANG  = HOST + "laporan_lakalantas_kat";
    public static final String DETAILSAMBANG  = HOST + "detail_laporan_sambang/{id}";
    public static final String HOMESAMBANG  = HOST + "list_laporan_sambang/{offset}";
    public static final String LISTCOMMENTARSAMBANG  = HOST + "list_comment_laporan_sambang/{id}/{offset}";
    public static final String SUBMITCOMMENTSAMBANG  = HOST + "submit_comment_laporan_sambang";

    //PROBLEM SOLVE
    public static final String PROBLEMSOLVE  = HOST + "submit_problem_solving";
    public static final String LISTPROBLEMSOLVE  = HOST + "history_problem_solving/{offset}";
    public static final String CATEGORYPROBLEM  = HOST + "laporan_masalah_kat";
    public static final String DETAILPROBLEMSOLVE  = HOST + "detail_problem_solving/{id}";
    public static final String HOMEPROBLEMSOLVE  = HOST + "list_problem_solving/{offset}";
    public static final String LISTCOMMENTARPROBLEMSOLVE  = HOST + "list_comment_problem_solving/{id}/{offset}";
    public static final String SUBMITCOMMENTPROBLEMSOLVE  = HOST + "submit_comment_problem_solving";

    //LAPORAN BARCODE
    public static final String SUBMITLAPBARCODE  = HOST + "submit_laporan_patroli";
    public static final String CATEGORYBARCODE  = HOST + "laporan_patroli_kat";
    public static final String LISTBARCODE  = HOST + "history_laporan_patroli/{offset}";
    public static final String DETAILBARCODE  = HOST + "detail_laporan_patroli/{id}";
    public static final String SUBMITCOMMENTBARCODE  = HOST + "submit_comment_laporan_patroli";
    public static final String LISTCOMMENTARBARCODE  = HOST + "list_comment_laporan_patroli/{id}/{offset}";
    public static final String CHECKINBARCODE  = HOST + "check_in_pos";
    public static final String CHECKOUTBARCODE  = HOST + "check_out_pos";

    //PEMILU
    public static final String LISTLOKASITPS  = HOST + "list_lokasi_tps/{kecamatan}";
    public static final String SUBMITTPS  = HOST + "submit_tps";
    public static final String LISTKELURAHAN  = HOST + "list_kelurahan";
    public static final String LISTKECAMATAN  = HOST + "list_kecamatan";
    public static final String EDITTPS  = HOST + "edit_tps";
    public static final String DETAIPLOTTINGLTPS  = HOST + "detail_tps/{id}";
    public static final String LISTTPS  = HOST + "list_tps/{nrp}/{lat}/{lon}/{offset}";
    public static final String LISTTPSJARAK  = HOST + "list_jarak_tps/{lat}/{lon}/{kecamatan}";
    public static final String LISTPEMILU  = HOST + "list_pemilu";
    public static final String LISTPESERTA = HOST + "list_peserta/{idpemilu}";
    public static final String LISTSUARA  = HOST + "list_suara_tps/{idpemilu}/{offset}";
    public static final String LISTSUARAKAPOLSEK  = HOST + "list_suara_tps_kapolsek/{idpemilu}";
    public static final String LISTSUARAKAPOLSEKSUMMARY  = HOST + "list_suara_tps_kapolsek_summary/{idpemilu}";
    public static final String DETAILSUARATPS  = HOST + "detail_suara_tps/{idTps}";
    public static final String SUBMITQUICKQOUNT  = HOST + "submit_hasil_pemilu";
    public static final String SUBMITFOTOPEMILU  = HOST + "submit_foto_pemilu";

    //PERFORMANCE
    public static final String PERF_LAP_INFO  = HOST + "list_performance_lap_info";
    public static final String PERF_LAKA_LANTAS  = HOST + "list_performance_lakalantas";
    public static final String PERF_PATROLI  = HOST + "list_performance_patroli";

    //PENUGASAN
    public static final String SUBMITPENUGASAN  = HOST + "submit_penugasan";
    public static final String LISTPERSONIL  = HOST + "list_personil";
    public static final String LISTKESATUAN  = HOST + "list_kesatuan";
    public static final String LISTTUGAS  = HOST + "list_tugas/{nrp}/{offset}";
    public static final String DETAILTUGAS  = HOST + "detail_tugas/{id}";
    public static final String RESPONSETUGAS  = HOST + "submit_response";
    public static final String LISTPENUGASAN  = HOST + "list_penugasan/{nrp}/{offset}";
    public static final String LISTSATUANWILAYAH  = HOST + "list_satuan_wilayah/{lat}/{lon}";

    //JADWAL
    public static final String LISTJADWAL  = HOST + "list_jadwal_kegiatan/{tgl}/{offset}";
    public static final String LISTJADWALBLN  = HOST + "list_jadwal_kegiatan_bln/{bln}";
    public static final String SUBMITJADWAL  = HOST + "submit_jadwal_kegiatan";
    public static final String UPDATE_JADWAL  = HOST + "update_status_jadwal_kegiatan";
    public static final String DETAIL_JADWAL  = HOST + "detail_jadwal_kegiatan/{id}";
    public static final String LIST_DISPOSISI  = HOST + "list_disposisi";

    //POSTGATUR
    public static final String LISTPOSTGATUR  = HOST + "list_pos/{lat}/{lon}";
    public static final String DETAILPOSTGATUR  = HOST + "detail_pos/{id}";
    public static final String DETAILOBVIT  = HOST + "detail_obvit/{id}";
    public static final String STATUSCHECKINPOSTGATUR  = HOST + "check_in_status/{id}";
    public static final String CHECKINPOSGATUR  = HOST + "check_in_gatur";
    public static final String CHECKOUTPOSGATUR  = HOST + "check_out_gatur";
    public static final String LISTRUTEPATROLI  = HOST + "list_rute/{nrp}";
    public static final String HISTORYPOSGATUR  = HOST + "history_gatur/{offset}";
    public static final String HISTORYPATROLI = HOST + "history_patroli/{offset}";

    //OTHERS
    public static final String LISTFUNGSI  = HOST + "fungsi";
    public static final String CHECKVERSI  = HOST + "versi/icc";

    public static String STATIC_MAP_BASE_URL = "http://maps.googleapis.com/maps/api/staticmap?";
    public static final String GOOGLE_API_KEY = "AIzaSyBT5Ko0Z3ijDAwgtqeT4L96xVp9IzOKiF8";

    public static int PEMILU_COLORS[] = new int[] {
            R.color.color_patroli_barcode,
            R.color.color_laporan_informasi,
            R.color.color_lapsus_pilkada,
            R.color.color_laporan_kejadian,
            R.color.color_laporan_performance,
            R.color.color_laporan_sambang,
            R.color.color_rute_patroli,
            R.color.color_laporan_laka
    };
}
