package com.baraciptalaksana.icckedirikota.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.baraciptalaksana.icckedirikota.models.Request;
import com.baraciptalaksana.icckedirikota.models.Response;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    //http://icc.membara.net/file/foto/
    //======================================================AUTHENTICATION==================================================//
    @POST(Constants.LOGIN)
    Call<Response.LoginResponse> login(@Body Request.UserRequestLogin request);

    @POST(Constants.FORGOTPASSWORD)
    Call<Response.ResponseForgotPassword> forgotpassword(@Body Request.UserRequestForgotPassword request);

    @POST(Constants.EDITPROFILE)
    Call<Response.LoginResponse> editprofile(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.LISTMENU)
    Call<Response.ResponseListMenu> listmenu(@Header("Authorization") String token, @Path("fungsi") String fungsi);

    @GET(Constants.LISTMENUPEMILU)
    Call<Response.ResponseListMenu> listmenupemilu(@Header("Authorization") String token, @Path("fungsi") String fungsi);

    @GET(Constants.LISTHOMEBERITA)
    Call<Response.ResponseListBerita> listhomeberita(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.LISTBERITA)
    Call<Response.ResponseListBerita> listberita(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.DETAILBERITA)
    Call<Response.ResponseDetailHumas> detailhumas(@Header("Authorization") String token, @Path("id") int id);

    //======================================================LAPINFO=====================================================//
    @POST(Constants.LAPINFO)
    Call<Response.ResponsePostLaporan> uploadMultiFileLapinfo(@Body RequestBody file, @Header("Authorization") String token);

    @POST(Constants.LAPPATROLI)
    Call<Response.ResponsePostLaporan> uploadMultiFilePatroli(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.CATEGORYLAPINFO)
    Call<Response.ResponseCategoryAllReport> categorylapinfo(@Header("Authorization") String token);

    @GET(Constants.CATEGORYPATROLI)
    Call<Response.ResponseCategoryAllReport> categorypatroli(@Header("Authorization") String token);

    @GET(Constants.DETAILLAPINFO)
    Call<Response.ResponseDetailLapinfo> detaillapinfo(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.HOMELAPINFO)
    Call<Response.ResponseListLapinfo> homelapinfo(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.LISTLAPINFO)
    Call<Response.ResponseListLapinfo> listlapinfo(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.LISTCOMMENTARLAPINFO)
    Call<Response.ResponseReportCommment> listcommentlapinfo(@Header("Authorization") String token, @Path("id") int id,  @Path("offset") int offset);

    @POST(Constants.SUBMITCOMMENTLAPINFO)
    Call<Response.ResponseSubmitComment> submitcommentlapinfo(@Body Request.CommentRequestLaporan request, @Header("Authorization") String token);


    //===================================================LAKA============================================================//
    @POST(Constants.LAKA)
    Call<Response.ResponsePostLaporan> uploadMultiFileLaka(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.LISTLAKA)
    Call<Response.ResponseListLaka> listlaka(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.CATEGORYLAKA)
    Call<Response.ResponseCategoryAllReport> categorylaka(@Header("Authorization") String token);

    @GET(Constants.DETAILLAKA)
    Call<Response.ResponseDetailLaka> detaillaka(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.HOMELAKA)
    Call<Response.ResponseListLaka> homelaka(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.LISTCOMMENTARLAKA)
    Call<Response.ResponseReportCommment> listcommentlaka(@Header("Authorization") String token, @Path("id") int id,  @Path("offset") int offset);

    @POST(Constants.SUBMITCOMMENTLAKA)
    Call<Response.ResponseSubmitComment> submitcommentlaka(@Body Request.CommentRequestLaporan request, @Header("Authorization") String token);

    //========================================================SAMBANG======================================================//
    @POST(Constants.SAMBANG)
    Call<Response.ResponsePostLaporan> uploadMultiFileSambang(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.LISTSAMBANG)
    Call<Response.ResponseListSambang> listsambang(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.DETAILSAMBANG)
    Call<Response.ResponseDetailSambang> detailsambang(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.HOMESAMBANG)
    Call<Response.ResponseListSambang> homesambang(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.CATEGORYSAMBANG)
    Call<Response.ResponseCategoryAllReport> categorysambang(@Header("Authorization") String token);

    @GET(Constants.LISTCOMMENTARSAMBANG)
    Call<Response.ResponseReportCommment> listcommentsambang(@Header("Authorization") String token, @Path("id") int id,  @Path("offset") int offset);

    @POST(Constants.SUBMITCOMMENTSAMBANG)
    Call<Response.ResponseSubmitComment> submitcommentsambang(@Body Request.CommentRequestLaporan request, @Header("Authorization") String token);


    //===========================================PROBLEM SOLVE=====================================//
    @POST(Constants.PROBLEMSOLVE)
    Call<Response.ResponsePostLaporan> uploadMultiFileProblemSolve(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.LISTPROBLEMSOLVE)
    Call<Response.ResponseListProblemSolve> listproblemsolve(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.CATEGORYPROBLEM)
    Call<Response.ResponseCategoryAllReport> categoryproblem(@Header("Authorization") String token);

    @GET(Constants.DETAILPROBLEMSOLVE)
    Call<Response.ResponseDetailProblemSolve> detailproblemsolve(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.HOMEPROBLEMSOLVE)
    Call<Response.ResponseListProblemSolve> homeproblemsolve(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.LISTCOMMENTARPROBLEMSOLVE)
    Call<Response.ResponseReportCommment> listcommentproblemsolve(@Header("Authorization") String token, @Path("id") int id,  @Path("offset") int offset);

    @POST(Constants.SUBMITCOMMENTPROBLEMSOLVE)
    Call<Response.ResponseSubmitComment> submitcommentproblemsolve(@Body Request.CommentRequestLaporan request, @Header("Authorization") String token);


    //===========================================PATROLI BARCODE=====================================//
    @POST(Constants.SUBMITLAPBARCODE)
    Call<Response.ResponsePostLaporan> uploadMultiFilePatroliBarcode(@Body RequestBody file, @Header("Authorization") String token);

    @GET(Constants.CATEGORYBARCODE)
    Call<Response.ResponseCategoryAllReport> categorybarcode(@Header("Authorization") String token);

    @GET(Constants.LISTBARCODE)
    Call<Response.ResponseListBarcode> listbarcode(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.DETAILBARCODE)
    Call<Response.ResponseDetailListBarcode> detailbarcode(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.LISTCOMMENTARBARCODE)
    Call<Response.ResponseReportCommment> listcommentbarcode(@Header("Authorization") String token, @Path("id") int id,  @Path("offset") int offset);

    @POST(Constants.CHECKINBARCODE)
    Call<Response.ResponseSubmitData> checkinbarcode(@Body Request.PatroliBarcodeRequestCheckin request, @Header("Authorization") String token);

    @POST(Constants.CHECKOUTBARCODE)
    Call<Response.ResponseCheckoutcode> checkoutbarcode(@Body Request.PatroliBarcodeRequestCheckout request, @Header("Authorization") String token);

    @POST(Constants.SUBMITCOMMENTBARCODE)
    Call<Response.ResponseSubmitComment> submitcommentbarcode(@Body Request.CommentRequestLaporan request, @Header("Authorization") String token);


    //==================================================PEMILU==================================================//
    @GET(Constants.LISTTPS)
    Call<Response.ResponseListPetugasTps> listpetugastps(@Header("Authorization") String token, @Path("nrp") String nrp, @Path("lat") String lat, @Path("lon") String lon, @Path("offset") int offset);

    @GET(Constants.LISTLOKASITPS)
    Call<Response.ResponseListPetugasTps> listlokasitps(@Header("Authorization") String token, @Path("kecamatan") long kecamatan);

    @GET(Constants.LISTTPSJARAK)
    Call<Response.ResponseListPetugasTps> listlokasitpsjarak(@Header("Authorization") String token, @Path("lat") String lat, @Path("lon") String lon, @Path("kecamatan") long kecamatan);

    @GET(Constants.LISTKELURAHAN)
    Call<Response.ResponseListKelurahan> listkelurahan(@Header("Authorization") String token);

    @POST(Constants.EDITTPS)
    Call<Response.ResponseSubmitData> editpetugastps(@Header("Authorization") String token, @Body Request.EditPetugasTpsRequest  request);

    @GET(Constants.DETAIPLOTTINGLTPS)
    Call<Response.ResponseDetailPlottingTps> detailplottingtps(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.LISTKECAMATAN)
    Call<Response.ResponseListKecamatan> listkecamatan(@Header("Authorization") String token);

    @GET(Constants.LISTPEMILU)
    Call<Response.ResponseListPemilu> listpemilu(@Header("Authorization") String token);

    @GET(Constants.LISTPESERTA)
    Call<Response.ResponseListPeserta> listpeserta(@Header("Authorization") String token, @Path("idpemilu") String idpemilu);

    @GET(Constants.LISTSUARA)
    Call<Response.ResponseListSuara> listsuara(@Header("Authorization") String token, @Path("idpemilu") String idpemilu, @Path("offset") int offset);

    @GET(Constants.LISTSUARAKAPOLSEK)
    Call<Response.ResponseListSuaraKapolsek> listSuaraKapolsek(@Header("Authorization") String token, @Path("idpemilu") String idpemilu);

    @GET(Constants.LISTSUARAKAPOLSEKSUMMARY)
    Call<Response.ResponseSummarySuaraKapolsek> listSuaraKapolsekSummary(@Header("Authorization") String token, @Path("idpemilu") String idpemilu);

    @GET(Constants.DETAILSUARATPS)
    Call<Response.ResponseDetailSuaraTps> detailsuaratps(@Header("Authorization") String token, @Path("idTps") String idTps);

    @POST(Constants.SUBMITQUICKQOUNT)
    Call<Response.ResponseBase> submitQuickCount(@Body Request.HasilPemiluRequest request, @Header("Authorization") String token);

    @POST(Constants.SUBMITFOTOPEMILU)
    Call<Response.ResponseBase> submitFotoPemilu(@Body RequestBody request, @Header("Authorization") String token);

    //==================================================PERFORMANCE==================================================//
    @GET(Constants.PERF_LAP_INFO)
    Call<Response.ResponsePerformanceLapInfo> performanceLapInfo(@Header("Authorization") String token);

    @GET(Constants.PERF_LAKA_LANTAS)
    Call<Response.ResponsePerformanceLakaLantas> performanceLakaLantas(@Header("Authorization") String token);

    @GET(Constants.PERF_PATROLI)
    Call<Response.ResponsePerformancePatroli> performancePatroli(@Header("Authorization") String token);

    //==================================================PENUGASAN==================================================//
    @GET(Constants.LISTPERSONIL)
    Call<Response.ResponseListPersonil> listpersonil(@Header("Authorization") String token);

    @GET(Constants.LISTKESATUAN)
    Call<Response.ResponseListKesatuan> listkesatuan(@Header("Authorization") String token);

    @GET(Constants.DETAILTUGAS)
    Call<Response.ResponseDetailTugas> detailtugas(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.LISTSATUANWILAYAH)
    Call<Response.ResponseListSatuanWilayah> listsatuanwilayah(@Header("Authorization") String token, @Path("lat") String lat, @Path("lon") String lon);

    @GET(Constants.LISTPENUGASAN)
    Call<Response.ResponseListPenugasan> listpenugasan(@Header("Authorization") String token, @Path("nrp") String nrp,  @Path("offset") int offset);

    @GET(Constants.LISTTUGAS)
    Call<Response.ResponseListTugas> listtugas(@Header("Authorization") String token, @Path("nrp") String nrp,  @Path("offset") int offset);

    @POST(Constants.SUBMITPENUGASAN)
    Call<Response.ResponseSubmitData> submitpenugasan(@Body Request.PenugasanRequest request, @Header("Authorization") String token);


    //======================================================JADWAL====================================================//
    @POST(Constants.SUBMITJADWAL)
    Call<Response.ResponseSubmitData> submitjadwal(@Header("Authorization") String token, @Body Request.JadwalRequest request);

    @GET(Constants.LISTJADWAL)
    Call<Response.ResponseListJadwal> listjadwal(@Header("Authorization") String token, @Path("tgl") String tgl, @Path("offset") int offset);

    @GET(Constants.LISTJADWALBLN)
    Call<Response.ResponseListJadwalBulan> listJadwalPerBulan(@Header("Authorization") String token, @Path("bln") String bln);

    @GET(Constants.LIST_DISPOSISI)
    Call<Response.ResponseListDisposisi> listDisposisi(@Header("Authorization") String token);

    @GET(Constants.DETAIL_JADWAL)
    Call<Response.ResponseDetailJadwal> detailJadwal(@Header("Authorization") String token, @Path("id") String id);

    @POST(Constants.UPDATE_JADWAL)
    Call<Response.ResponseBase> updateJadwal(@Header("Authorization") String token, @Body Request.JadwalStatusRequest request);

    //======================================================POST GATUR====================================================//
    @GET(Constants.LISTPOSTGATUR)
    Call<Response.ResponseListPostGatur> listpostgatur(@Header("Authorization") String token, @Path("lat") String lat, @Path("lon") String lon);

    @GET(Constants.DETAILPOSTGATUR)
    Call<Response.ResponseDetailPosGatur> detailpostgatur(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.DETAILOBVIT)
    Call<Response.ResponseDetailObvit> detailobvit(@Header("Authorization") String token, @Path("id") int id);

    @GET(Constants.STATUSCHECKINPOSTGATUR)
    Call<Response.ResponseCheckinGatur> statuscheckingatur(@Header("Authorization") String token, @Path("id") int id);

    @POST(Constants.CHECKINPOSGATUR)
    Call<Response.ResponseSubmitData> checkinposgatur(@Body Request.PatroliBarcodeRequestCheckin request, @Header("Authorization") String token);

    @POST(Constants.CHECKOUTPOSGATUR)
    Call<Response.ResponseCheckoutcode> checkoutposgatur(@Body Request.PatroliBarcodeRequestCheckout request, @Header("Authorization") String token);

    @GET(Constants.LISTRUTEPATROLI)
    Call<Response.ResponseListRutePatroli> listrutepatroli(@Header("Authorization") String token, @Path("nrp") String nrp);

    @GET(Constants.HISTORYPOSGATUR)
    Call<Response.ResponseHistoryPosGatur> historypostgatur(@Header("Authorization") String token, @Path("offset") int offset);

    @GET(Constants.HISTORYPATROLI)
    Call<Response.ResponseHistoryPosGatur> historypatroli(@Header("Authorization") String token, @Path("offset") int offset);


    //======================================================POST GATUR====================================================//
    @GET(Constants.LISTFUNGSI)
    Call<Response.ResponseListFungsi> listfungsi(@Header("Authorization") String token);

    @GET(Constants.CHECKVERSI)
    Call<Response.ResponseCheckVersion> checkversion(@Header("Authorization") String token);


    class Factory {
        private boolean canCache = false;
        private static ApiInterface service;
        public static ApiInterface getInstance() {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60 / 2, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
//                    .cache(new Cache())
                    .cache(null)
                    .build();

            httpClient.addInterceptor(logging);

            Gson gson = new GsonBuilder()
                    .setDateFormat("")
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HOST)
                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .client(Injector.provideOkHttpClient())
                    .client(httpClient.build())
                    .build();

            service = retrofit.create(ApiInterface.class);
            return service;
        }

    }
}
