package com.baraciptalaksana.icckedirikota.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.baraciptalaksana.icckedirikota.R;
import com.baraciptalaksana.icckedirikota.models.OfficerLocation;
import com.baraciptalaksana.icckedirikota.utils.Constants;
import com.baraciptalaksana.icckedirikota.utils.Helper;
import com.baraciptalaksana.icckedirikota.utils.SharedPreferences;

public class TrackerService extends Service {
    private static final String TAG = TrackerService.class.getSimpleName();
    private boolean serviceRun = false;
    private FusedLocationProviderClient client;
    private MyLocationCallback callback;
    private Context mContext = this;

    public TrackerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground();
        loginToFirebase();
        serviceRun = true;
    }

    private void startForeground() {
        String channelId = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ? createNotificationChannel() : "";

        buildNotification(channelId);
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = "my_service";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    private void buildNotification(String channelId) {
        String stop = "stop";
        registerReceiver(stopReceiver, new IntentFilter(stop));
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_text))
                .setOngoing(true)
                .setContentIntent(broadcastIntent)
                .setSmallIcon(R.drawable.ic_track_changes_black_24dp);
        startForeground(1, builder.build());
    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            unregisterReceiver(stopReceiver);
            stopSelf();
            serviceRun = false;
            client.removeLocationUpdates(callback);
            callback.removeDB();
        }
    };

    @Override
    public void onDestroy() {
        try {
            unregisterReceiver(stopReceiver);
        } catch (Exception e) {

        }
        serviceRun = false;
        client.removeLocationUpdates(callback);
        callback.removeDB();
    }

    private void loginToFirebase() {
        // Authenticate with Firebase, and request location updates
        String email = getString(R.string.firebase_email);
        String password = getString(R.string.firebase_password);
        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>(){
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "firebase auth success");
                    requestLocationUpdates();
                } else {
                    Log.d(TAG, "firebase auth failed");
                }
            }
        });
    }

    private void requestLocationUpdates() {
        final String officerId = Helper.isEmptyString(SharedPreferences.getNrp(this)) ? "null"
                : SharedPreferences.getNrp(this);
        final String officerName = Helper.isEmptyString(SharedPreferences.getNama(this)) ? "null"
                : SharedPreferences.getNama(this);
        final String officerLevel = Helper.isEmptyString(SharedPreferences.getPangkat(this)) ? "null"
                : SharedPreferences.getPangkat(this);
        final String officerPhoto = Helper.isEmptyString(SharedPreferences.getFoto(this)) ? "null"
                : SharedPreferences.getFoto(this);
        final String officerSatwil = Helper.isEmptyString(SharedPreferences.getSatwil(this)) ? "null"
                : SharedPreferences.getSatwil(this);
        final String officerPhone = Helper.isEmptyString(SharedPreferences.getTlp(this)) ? "null"
                : SharedPreferences.getTlp(this);
        final String officerSatwilText = Helper.isEmptyString(SharedPreferences.getSatwilText(this)) ? "null"
                : SharedPreferences.getSatwilText(this);
        final String officerJabatan = Helper.isEmptyString(SharedPreferences.getJabatan(this)) ? "null"
                : SharedPreferences.getJabatan(this);
        final String officerFungsi = Helper.isEmptyString(SharedPreferences.getFungsi(this)) ? "null"
                : SharedPreferences.getFungsi(this);
        final String officerMarkerIcon = Helper.isEmptyString(SharedPreferences.getMarkerIcon(this)) ? "null"
                : SharedPreferences.getMarkerIcon(this);

        LocationRequest request = new LocationRequest();
        request.setInterval(10000);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        client = LocationServices.getFusedLocationProviderClient(this);

        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase

            OfficerLocation ol = new OfficerLocation();
            ol.setJabatan(officerJabatan);
            ol.setSatwilText(officerSatwilText);
            ol.setPhone(officerPhone);
            ol.setLevel(officerLevel);
            ol.setName(officerName);
            ol.setPhoto(officerPhoto);
            ol.setSatwil(officerSatwil);
            ol.setFungsi(officerFungsi);
            ol.setMarkerIcon(officerMarkerIcon);

            callback = new MyLocationCallback(officerId, ol);

            client.requestLocationUpdates(request, callback, null);
        }
    }

    private class MyLocationCallback extends LocationCallback {
        private OfficerLocation officerLocation;
        private String officerId;

        public MyLocationCallback(String officerId, OfficerLocation officerLocation) {
            this.officerId = officerId;
            this.officerLocation = officerLocation;
        }

        public void removeDB() {
            DatabaseReference locRef = FirebaseDatabase.getInstance()
                    .getReference(getString(R.string.firebase_path) + "/" + officerId);

            DatabaseReference fungRef = FirebaseDatabase.getInstance()
                    .getReference("fungsi/" + officerLocation.getFungsi() + "/" + officerId);

            locRef.removeValue();
            fungRef.removeValue();

            Log.d(TAG, "Remove db");
        }

        @Override
        public void onLocationResult(LocationResult locationResult) {
            String officerId = Helper.isEmptyString(SharedPreferences.getNrp(mContext)) ? "null"
                    : SharedPreferences.getNrp(mContext);

            Location location = locationResult.getLastLocation();

            if (location != null && !Helper.isEmptyString(officerId)) {
                officerLocation.setAccuracy(location.getAccuracy());
                officerLocation.setAltitude(location.getAltitude());
                officerLocation.setBearing(location.getBearing());
                officerLocation.setLatitude(location.getLatitude());
                officerLocation.setLongitude(location.getLongitude());
                officerLocation.setProvider(location.getProvider());
                officerLocation.setSpeed(location.getSpeed());
                officerLocation.setTime(location.getTime());

                Log.d(TAG, "location update " + officerId + ", " + officerLocation.getJabatan()
                        + ", " + officerLocation.getSatwilText() + " => " + location);

                Log.d(TAG, "service run " + serviceRun);

                DatabaseReference locRef = FirebaseDatabase.getInstance()
                    .getReference(getString(R.string.firebase_path) + "/" + officerId);

                DatabaseReference fungRef = FirebaseDatabase.getInstance()
                        .getReference("fungsi/" + officerLocation.getFungsi() + "/" + officerId);

                if (serviceRun) {
                    locRef.setValue(officerLocation);
                    fungRef.setValue(officerLocation);
                } else {
                    locRef.removeValue();
                    fungRef.removeValue();
                }
            } else {
                removeDB();
                onDestroy();
            }
        }
    }
}
